function AddToCart(){
	var siteurl 		= $('#url').val();
	var csrf_token 		= $('#csrf_token').val();
	var variant_type 	= $('#variant_type').val();
	var flag 			= 1;

	var result = variant_type.split(",");
	for(var i = 0;i < result.length;i++){
		var checkdata = $('#'+result[i]).val(); 
		if(checkdata==0){
			$('#Err_'+result[i]).html('Please select '+result[i]);
			flag++; 
		}
	}
	if(flag==1){
		$('#AddToCartForm').submit();
	}
}