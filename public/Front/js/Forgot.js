$(function() { 
   $("#ForgotForm").validate({
    rules: {   
      email: {
        required: true,             
        email: true  
      }
    },    
    messages: {
      email:{               
        required: "Please enter email address.",               
        email: "Please enter valid email address."            
      }
    },
    submitHandler: function(form) {
      var siteurl   = $('#url').val();
      var FormValue = $("#ForgotForm").serializeArray();
      
      $('#SubmitBtn').prop("disabled",true);
      $('#SubmitBtn').val('Loading..');
      $.post(siteurl+"/CheckForgetPassword", FormValue, function(data){
        var obj = $.parseJSON(data);
        if(obj['status']=='1'){
          $('#ForgotForm')[0].reset();
        } 
        $('#SubmitBtn').prop("disabled",false);
        $('#SubmitBtn').val('Submit');       
        $('#Return_msg').html(obj['msg']);
        $('#Return_msg').show();
      });
    }  
  });
  
});