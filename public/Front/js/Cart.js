CartSummary();
function RemoveCart(id){
	var siteurl 		= $('#url').val();
	var csrf_token 		= $('#csrf_token').val();
	var txt;
	var r = confirm("Are you sure!");
	if (r == true) {
	 	$.post(siteurl+"/RemoveCart", {action:'RemoveCart',id:id,_token:csrf_token}, function(data){
	 		
	 		if(data==0){
		    location.reload();
	 		}
	 		CartSummary();
	 		$('#'+id).html('');
			$('#TotalCartCount').html(data);
		});	
	}
}

function CartSummary(){
	var siteurl 		= $('#url').val();
	var csrf_token 		= $('#csrf_token').val();

	$.post(siteurl+"/CartSummary", {action:'CartSummary',page:'Cart',_token:csrf_token}, function(data){	
 		$('#CartSummary').html(data);
	});
}

function UpdateProQty(id){
	var siteurl 		= $('#url').val();
	var csrf_token 	= $('#csrf_token').val();
	var pro_qty 		= $('#pro_qty_'+id).val();

	$.post(siteurl+"/UpdateProQty", {action:'UpdateProQty',id:id,pro_qty:pro_qty,_token:csrf_token}, function(data){	
 		CartSummary();
	});
}

function CartEdit(id){
	$('#OpenCartModel').trigger('click');
	var siteurl 		= $('#url').val();
	var csrf_token 		= $('#csrf_token').val();
	$.post(siteurl+"/CartEdit", {action:'CartEdit',id:id,_token:csrf_token}, function(data){	
 		$('#CartEditModelDetails').html(data);
	});
}

function UpdateCartSubmitBtn(){
	$('#UpdateCart').submit();
}

function ApplyPromoCode(){
	var promo_code 	= $('#promo_code').val();
	var siteurl 		= $('#url').val();
	var csrf_token 	= $('#csrf_token').val();

	if(promo_code==''){
		$("#promo_code").focus();
		$("#PromoCodeSmg").html('Please enter promo code');
	}else{
		$.post(siteurl+"/ApplyPromoCode", {action:'ApplyPromoCode',promo_code:promo_code,_token:csrf_token}, function(data){
			if(data==1){
				CartSummary();
			}else{
	 			$('#PromoCodeSmg').html(data);
			}
		});
	}
}

function RemovePromoCode(){
	var siteurl 		= $('#url').val();
	var csrf_token 	= $('#csrf_token').val();
	var r = confirm("Are you sure!");
	if (r == true) {
		$.post(siteurl+"/RemovePromoCode", {action:'RemovePromoCode',_token:csrf_token}, function(data){
			CartSummary();
		});
	}
}