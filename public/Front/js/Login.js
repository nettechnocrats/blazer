$(function() { 
   $("#LoginForm").validate({
    rules: {   
      password: {
        required: true,
        minlength: 5
      },
      email: {
        required: true,             
        email: true  
      }
    },    
    messages: {
      password:{               
        required: "Please enter password.",               
        minlength: "Your password must be at least 5 characters long."            
      },
      email:{               
        required: "Please enter email address.",               
        email: "Please enter valid email address."            
      }
    },
    submitHandler: function(form) {
      var siteurl   = $('#url').val();
      var FormValue = $("#LoginForm").serializeArray();
      
      $('#LoginBtn').prop("disabled",true);
      $('#LoginBtn').val('Loading..');
      $.post(siteurl+"/CheckLogin", FormValue, function(data){
        var obj = $.parseJSON(data);
        if(obj['status']=='1'){
          $('#LoginForm')[0].reset();
          location.href = obj['RefererURL'];
        }
        $('#LoginBtn').prop("disabled",false);
        $('#LoginBtn').val('Login');       
        $('#Return_msg').html(obj['msg']);
        $('#Return_msg').show();
      });
    }  
  });
  
});