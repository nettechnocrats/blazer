

function ChangeUserStatus(id,status)
{
    var token           = $("#_token").val();
    var url             = $("#url").val();
    var DataStr         = {'_token':token,'id':id,'status':status};
    $("#StatusHref_"+id).html('Loading..');
    $.ajax({
        type: "POST",
        url:  url+"/ChangeUserStatus",
        data: DataStr,
        success: function(data)
        {
        	if(data=='1'){
        		$('#StatusHref_'+id).removeClass('btn-success');
        		$('#StatusHref_'+id).addClass('btn-danger');
        		$('#StatusHref_'+id).attr('onClick', 'ChangeUserStatus('+id+','+data+');');
        		$('#StatusHref_'+id).html('Deactive');
        	}else{
        		$('#StatusHref_'+id).removeClass('btn-danger');
        		$('#StatusHref_'+id).addClass('btn-success');
        		$('#StatusHref_'+id).attr('onClick', 'ChangeUserStatus('+id+','+data+');');
        		$('#StatusHref_'+id).html('Active');
        	}
        }
    });
}


