$(function() { 
  $("#ForgotForm").validate({
    rules: {   
      email:{          
        required: true,             
        email: true         
      }
    },    
    messages: {
      email:{               
        required: "Please enter email.",               
        email: "Please enter valid email."            
      }
    },
    submitHandler: function(form) {
      var password  = $('#password').val();
      var email     = $('#email').val();
      var referer_url= $('#referer_url').val();
      var siteurl   = $('#url').val();
      var _token    = $('#_token').val();
      
      $('#SubmitBtn').prop("disabled",true);
      $('#SubmitBtn').html('Loading..');
      $.post(siteurl+"/CheckForgot", {referer_url:referer_url,email:email,password:password,_token:_token}, function(data){
        var obj = $.parseJSON(data);
        $('#Error_msg').html(obj['msg']);
        $('#Error_msg').show();
        if(obj['status']=='1'){
          $('#ForgotForm')[0].reset();
          location.href = obj['RefererURL'];
        }else{
          $('#SubmitBtn').prop("disabled",false);
          $('#SubmitBtn').html('Reset Password');
        }
      });
    }  
  });
});