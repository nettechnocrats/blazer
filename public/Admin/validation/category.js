$(function() { 
  $("#saveform").validate({
  	ignore: [],
    rules: {   
      name: {
        required: true,
        normalizer: function(value) {
        	return $.trim(value);
      	}      
      },
      slug: {
        required: true,
        normalizer: function(value) {
        	return $.trim(value);
      	} 	     
      },
      seo_title: {
        required: true,
        normalizer: function(value) {
        	return $.trim(value);
      	}  	     
      },
      seo_description: {
        required: true,
        normalizer: function(value) {
        	return $.trim(value);
      	}   	     
      },
      sort: {
        required: true,
        minlength: 1
      },
      image:{
					required: true,
					extension: "png|jpg|JPEG"
	  },
	  description:{
             required: function() 
            {
             CKEDITOR.instances.description.updateElement();
            },
             minlength:10
      }
    },    
    messages: {
      name: "Please Enter Name",
      slug: "Please Enter Slug",
      seo_title: "Please Enter SEO Title",
      seo_description: "Please Enter SEO Description",
      sort: "Please Enter Sort",
      image:{
					required: 'Please Select Image',
					extension: "Invalid Image"
				},
	  description:{
	                 required:"Please Enter Category Description",
	                 minlength:"Please Enter 10 Characters"
	            }		
    },
    submitHandler: function(form) {
      $('#SubmitBtn').prop("disabled",true);
      $('#SubmitBtn').html('Loading..');
      form.submit();
    }  
  });
});


function DeleteCategory(id)
{
    var token           = $("#_token").val();
    var url             = $("#url").val();
    var DataStr         = {'_token':token,'id':id};
    $.ajax({
        type: "POST",
        url:  url+"/DeleteCategory",
        data: DataStr,
        success: function(data)
        {
            $("#Row_"+id).remove();
        }
    });
}

function Slug()
{
    var textValue = $('#name').val();
    textValue =textValue.toLowerCase().replace(/[\*\^\'\!]/g, '').split(' ').join('-');
    textValue=textValue.replace(/[_\W]+/g, "-");
    $('#slug').val(textValue);
}

$(function() { 
  $("#updateform").validate({
  	ignore: [],
    rules: {   
      name: {
        required: true,
        normalizer: function(value) {
        	return $.trim(value);
      	}      
      },
      slug: {
        required: true,
        normalizer: function(value) {
        	return $.trim(value);
      	} 	     
      },
      seo_title: {
        required: true,
        normalizer: function(value) {
        	return $.trim(value);
      	}  	     
      },
      seo_description: {
        required: true,
        normalizer: function(value) {
        	return $.trim(value);
      	}   	     
      },
      sort: {
        required: true,
        minlength: 1
      },
	  description:{
             required: function() 
            {
             CKEDITOR.instances.description.updateElement();
            },
             minlength:10
      }
    },    
    messages: {
      name: "Please Enter Name",
      slug: "Please Enter Slug",
      seo_title: "Please Enter SEO Title",
      seo_description: "Please Enter SEO Description",
      sort: "Please Enter Sort",
	  description:{
	                 required:"Please Enter Category Description",
	                 minlength:"Please Enter 10 Characters"
	            }		
    },
    submitHandler: function(form) {
      $('#UpdateBtn').prop("disabled",true);
      $('#UpdateBtn').html('Loading..');
      form.submit();
    }  
  });
});

function ChangeCategoryStatus(id,status)
{
    var token           = $("#_token").val();
    var url             = $("#url").val();
    var DataStr         = {'_token':token,'id':id,'status':status};
    $("#StatusHref_"+id).html('Loading..');
    $.ajax({
        type: "POST",
        url:  url+"/ChangeCategoryStatus",
        data: DataStr,
        success: function(data)
        {
        	if(data=='1'){
        		$('#StatusHref_'+id).removeClass('btn-success');
        		$('#StatusHref_'+id).addClass('btn-danger');
        		$('#StatusHref_'+id).attr('onClick', 'ChangeCategoryStatus('+id+','+data+');');
        		$('#StatusHref_'+id).html('Deactive');
        	}else{
        		$('#StatusHref_'+id).removeClass('btn-danger');
        		$('#StatusHref_'+id).addClass('btn-success');
        		$('#StatusHref_'+id).attr('onClick', 'ChangeCategoryStatus('+id+','+data+');');
        		$('#StatusHref_'+id).html('Active');
        	}
        }
    });
}

function ChangeCategoryShowOnHome(id,ShowOnHome)
{
    var token           = $("#_token").val();
    var url             = $("#url").val();
    var DataStr         = {'_token':token,'id':id,'ShowOnHome':ShowOnHome};
    $("#show_on_homeHref_"+id).html('Loading..');
    $.ajax({
        type: "POST",
        url:  url+"/ChangeCategoryShowOnHome",
        data: DataStr,
        success: function(data)
        {
          if(data=='1'){
            $('#show_on_homeHref_'+id).removeClass('btn-success');
            $('#show_on_homeHref_'+id).addClass('btn-danger');
            $('#show_on_homeHref_'+id).attr('onClick', 'ChangeCategoryShowOnHome('+id+','+data+');');
            $('#show_on_homeHref_'+id).html('<i class="fa fa-times" aria-hidden="true"></i>');
          }else{
            $('#show_on_homeHref_'+id).removeClass('btn-danger');
            $('#show_on_homeHref_'+id).addClass('btn-success');
            $('#show_on_homeHref_'+id).attr('onClick', 'ChangeCategoryShowOnHome('+id+','+data+');');
            $('#show_on_homeHref_'+id).html('<i class="fa fa-check-square" aria-hidden="true"></i>');
          }
        }
    });
}

