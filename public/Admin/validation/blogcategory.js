$(function() { 
  $("#saveform").validate({
    ignore: [],
    rules: {   
      name: {
        required: true,
        normalizer: function(value) {
          return $.trim(value);
        }      
      },
      sort: {
        required: true,
        minlength: 1
      },
      slug: {
        required: true,
        minlength: 1
      }
    },    
    messages: {
      name: "Please Enter Name",
      sort: "Please Enter Sort", 
      slug: "Please Enter Slug" 
    },
    submitHandler: function(form) {
      $('#SubmitBtn').prop("disabled",true);
      $('#SubmitBtn').html('Loading..');
      form.submit();
    }  
  });
});

function Slug()
{
    var textValue = $('#name').val();
    textValue =textValue.toLowerCase().replace(/[\*\^\'\!]/g, '').split(' ').join('-');
    textValue=textValue.replace(/[_\W]+/g, "-");
    $('#slug').val(textValue);
}

$(function() { 
  $("#updateform").validate({
  	ignore: [],
    rules: {   
      name: {
        required: true,
        normalizer: function(value) {
        	return $.trim(value);
      	}      
      },
      slug: {
        required: true,
        normalizer: function(value) {
        	return $.trim(value);
      	} 	     
      },
      sort: {
        required: true,
        minlength: 1
      }
    },    
    messages: {
      name: "Please Enter Name",
      slug: "Please Enter Slug",
      sort: "Please Enter Sort"	
    },
    submitHandler: function(form) {
      $('#UpdateBtn').prop("disabled",true);
      $('#UpdateBtn').html('Loading..');
      form.submit();
    }  
  });
});

function ChangeBlogCategoryStatus(id,status)
{
    var token           = $("#_token").val();
    var url             = $("#url").val();
    var DataStr         = {'_token':token,'id':id,'status':status};
    $("#StatusHref_"+id).html('Loading..');
    $.ajax({
        type: "POST",
        url:  url+"/ChangeBlogCategoryStatus",
        data: DataStr,
        success: function(data)
        {
          if(data=='1'){
            $('#StatusHref_'+id).removeClass('btn-success');
            $('#StatusHref_'+id).addClass('btn-danger');
            $('#StatusHref_'+id).attr('onClick', 'ChangeBlogCategoryStatus('+id+','+data+');');
            $('#StatusHref_'+id).html('Deactive');
          }else{
            $('#StatusHref_'+id).removeClass('btn-danger');
            $('#StatusHref_'+id).addClass('btn-success');
            $('#StatusHref_'+id).attr('onClick', 'ChangeBlogCategoryStatus('+id+','+data+');');
            $('#StatusHref_'+id).html('Active');
          }
        }
    });
}

