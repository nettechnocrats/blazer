$(function() { 
  $("#saveform").validate({
    ignore: [],
    rules: {   
      name: {
        required: true,
        normalizer: function(value) {
          return $.trim(value);
        }      
      },
      slug: {
        required: true,
        normalizer: function(value) {
          return $.trim(value);
        }        
      },
      category: {
        required: true,
        normalizer: function(value) {
          return $.trim(value);
        }        
      },
      seo_title: {
        required: true,
        normalizer: function(value) {
          return $.trim(value);
        }        
      },
      date: {
        required: true,
        normalizer: function(value) {
          return $.trim(value);
        }        
      },
      seo_description: {
        required: true,
        normalizer: function(value) {
          return $.trim(value);
        }          
      },
      author: {
        required: true,
        minlength: 1
      },
      image:{
          required: true,
          extension: "png|jpg|JPEG"
    },
    description:{
             required: function() 
            {
             CKEDITOR.instances.description.updateElement();
            },
             minlength:10
      }
    },    
    messages: {
      name: "Please Enter Name.",
      slug: "Please Enter Slug.",
      category: "Please Select Category.",
      date: "Please Select Publish Date.",
      seo_title: "Please Enter SEO Title",
      seo_description: "Please Enter SEO Description",
      author: "Please Enter Author Name",
      image:{
          required: 'Please Select Image',
          extension: "Invalid Image"
        },
    description:{
                   required:"Please Enter Blog Description",
                   minlength:"Please Enter 10 Characters"
              }   
    },
    submitHandler: function(form) {
      $('#SubmitBtn').prop("disabled",true);
      $('#SubmitBtn').html('Loading..');
      form.submit();
    }  
  });
});




function Slug()
{
    var textValue = $('#name').val();
    textValue =textValue.toLowerCase().replace(/[\*\^\'\!]/g, '').split(' ').join('-');
    textValue=textValue.replace(/[_\W]+/g, "-");
    $('#slug').val(textValue);
}

$(function() { 
  $("#updateform").validate({
  	ignore: [],
    rules: {   
      name: {
        required: true,
        normalizer: function(value) {
        	return $.trim(value);
      	}      
      },
      slug: {
        required: true,
        normalizer: function(value) {
        	return $.trim(value);
      	} 	     
      },
      seo_title: {
        required: true,
        normalizer: function(value) {
        	return $.trim(value);
      	}  	     
      },
      seo_description: {
        required: true,
        normalizer: function(value) {
        	return $.trim(value);
      	}   	     
      },
      sort: {
        required: true,
        minlength: 1
      },
	  description:{
             required: function() 
            {
             CKEDITOR.instances.description.updateElement();
            },
             minlength:10
      }
    },    
    messages: {
      name: "Please Enter Name",
      slug: "Please Enter Slug",
      seo_title: "Please Enter SEO Title",
      seo_description: "Please Enter SEO Description",
      sort: "Please Enter Sort",
	  description:{
	                 required:"Please Enter Variant Description",
	                 minlength:"Please Enter 10 Characters"
	            }		
    },
    submitHandler: function(form) {
      $('#UpdateBtn').prop("disabled",true);
      $('#UpdateBtn').html('Loading..');
      form.submit();
    }  
  });
});

function ChangeBlogStatus(id,status)
{
    var token           = $("#_token").val();
    var url             = $("#url").val();
    var DataStr         = {'_token':token,'id':id,'status':status};
    $("#StatusHref_"+id).html('Loading..');
    $.ajax({
        type: "POST",
        url:  url+"/ChangeBlogStatus",
        data: DataStr,
        success: function(data)
        {
        	if(data=='1'){
        		$('#StatusHref_'+id).removeClass('btn-success');
        		$('#StatusHref_'+id).addClass('btn-danger');
        		$('#StatusHref_'+id).attr('onClick', 'ChangeBlogStatus('+id+','+data+');');
        		$('#StatusHref_'+id).html('Deactive');
        	}else{
        		$('#StatusHref_'+id).removeClass('btn-danger');
        		$('#StatusHref_'+id).addClass('btn-success');
        		$('#StatusHref_'+id).attr('onClick', 'ChangeBlogStatus('+id+','+data+');');
        		$('#StatusHref_'+id).html('Active');
        	}
        }
    });
}


