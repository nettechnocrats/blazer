$(function() { 
  $("#saveform").validate({
    ignore: [],
    rules: {   
      name: {
        required: true,
        normalizer: function(value) {
          return $.trim(value);
        }      
      },
      slug: {
        required: true,
        normalizer: function(value) {
          return $.trim(value);
        }        
      },
      price: {
        required: true,
        normalizer: function(value) {
          return $.trim(value);
        }        
      },
      c_price: {
        required: true,
        normalizer: function(value) {
          return $.trim(value);
        }        
      },
      collection: {
        required: true,
        normalizer: function(value) {
          return $.trim(value);
        }        
      },
      Category: {
        required: true,
        normalizer: function(value) {
          return $.trim(value);
        }        
      },
      seo_title: {
        required: true,
        normalizer: function(value) {
          return $.trim(value);
        }        
      },
      seo_description: {
        required: true,
        normalizer: function(value) {
          return $.trim(value);
        }          
      },
      sort: {
        required: true,
        minlength: 1
      }
    },    
    messages: {
      name: "Please Enter Name",
      slug: "Please Enter Slug",
      price: "Please Enter Price",
      c_price: "Please Enter Comparision Price",
      collection: "Please Select Collection",
      Category: "Please Select Category",
      sort: "Please Enter Sort" 
    },
    submitHandler: function(form) {
      $('#SubmitBtn').prop("disabled",true);
      $('#SubmitBtn').html('Loading..');
      form.submit();
    }  
  });
});


function AddMoreSection()
{
    var url = $("#url").val();
    var image = "/public/Front/Category/noproduct.png";
    var id = Math.floor(1000 + Math.random() * 9000);
    var section = '<div class="row" id="'+id+'"><div class="col-md-4"><div class="form-group"><input type="file" name="image[]" class="form-control-sm form-control upld" alt="ImagePreview_'+id+'"><span id="imageErr"></span></div></div><div class="col-md-4"><div class="form-group"><a href="javascript:void(0);" onclick="RemoveSection('+id+');" class="btn btn-danger">Remove</a></div></div></div>';
     $("#ImagesDiv").append(section);    
}

function RemoveSection(ID)
{
    $("#"+ID).remove();
}

function DeleteCategory(id)
{
    var token           = $("#_token").val();
    var url             = $("#url").val();
    var DataStr         = {'_token':token,'id':id};
    $.ajax({
        type: "POST",
        url:  url+"/DeleteCategory",
        data: DataStr,
        success: function(data)
        {
            $("#Row_"+id).remove();
        }
    });
}

function Slug()
{
    var textValue = $('#name').val();
    textValue =textValue.toLowerCase().replace(/[\*\^\'\!]/g, '').split(' ').join('-');
    textValue=textValue.replace(/[_\W]+/g, "-");
    $('#slug').val(textValue);
}

$(function() { 
  $("#updateform").validate({
    ignore: [],
    rules: {   
      name: {
        required: true,
        normalizer: function(value) {
          return $.trim(value);
        }      
      },
      slug: {
        required: true,
        normalizer: function(value) {
          return $.trim(value);
        }        
      },
      seo_title: {
        required: true,
        normalizer: function(value) {
          return $.trim(value);
        }        
      },
      seo_description: {
        required: true,
        normalizer: function(value) {
          return $.trim(value);
        }          
      },
      sort: {
        required: true,
        minlength: 1
      },
    description:{
             required: function() 
            {
             CKEDITOR.instances.description.updateElement();
            },
             minlength:10
      }
    },    
    messages: {
      name: "Please Enter Name",
      slug: "Please Enter Slug",
      seo_title: "Please Enter SEO Title",
      seo_description: "Please Enter SEO Description",
      sort: "Please Enter Sort",
    description:{
                   required:"Please Enter Category Description",
                   minlength:"Please Enter 10 Characters"
              }   
    },
    submitHandler: function(form) {
      $('#UpdateBtn').prop("disabled",true);
      $('#UpdateBtn').html('Loading..');
      form.submit();
    }  
  });
});

function ChangeProductStatus(id,status)
{
    var token           = $("#_token").val();
    var url             = $("#url").val();
    var DataStr         = {'_token':token,'id':id,'status':status};
    $("#StatusHref_"+id).html('Loading..');
    $.ajax({
        type: "POST",
        url:  url+"/ChangeProductStatus",
        data: DataStr,
        success: function(data)
        {
          if(data=='1'){
            $('#StatusHref_'+id).removeClass('btn-success');
            $('#StatusHref_'+id).addClass('btn-danger');
            $('#StatusHref_'+id).attr('onClick', 'ChangeProductStatus('+id+','+data+');');
            $('#StatusHref_'+id).html('Deactive');
          }else{
            $('#StatusHref_'+id).removeClass('btn-danger');
            $('#StatusHref_'+id).addClass('btn-success');
            $('#StatusHref_'+id).attr('onClick', 'ChangeProductStatus('+id+','+data+');');
            $('#StatusHref_'+id).html('Active');
          }
        }
    });
}

function DeleteImage(id)
{
    var token           = $("#_token").val();
    var url             = $("#url").val();
    var DataStr         = {'_token':token,'id':id};
    $.ajax({
        type: "POST",
        url:  url+"/DeleteImage",
        data: DataStr,
        success: function(data)
        {
            $("#Row_"+id).remove();
        }
    });
}


