<?php
/*Admin Panel*/
Route::get('/Admin', ['as' => 'AdminLogin', 'uses' => 'Admin\LoginController@Login']);
Route::post('/Admin/AdminLoginValidate',['as'=>'AdminLoginValidate','uses'=>'Admin\LoginController@AdminLoginValidate']);
Route::get('/Admin/forgot', ['as' => 'AdminForgot', 'uses' => 'Admin\LoginController@Forgot']);
Route::post('/Admin/CheckForgot', ['as' => 'AdminCheckForgot', 'uses' => 'Admin\LoginController@CheckForgot']);

Route::group(['namespace' => 'Admin', 'prefix' => 'Admin', 'middleware'=>['IsAdminLogin']], function () {
	Route::get('/AdminLogout', ['as' => 'AdminLogout', 'uses' => 'LoginController@AdminLogout']);
	Route::get('/dashboard', ['as' => 'Dashboard', 'uses' => 'DashboardController@Dashboard']);
	/*-------------------------------------------------------------------------------------------
	Category Routes
	---------------------------------------------------------------------------------------------*/
	Route::get('/Category', ['as' => 'Category', 'uses' => 'CategoryController@Category']);
	Route::get('/AddCategory', ['as' => 'AddCategory', 'uses' => 'CategoryController@AddCategory']);
	Route::post('/SaveCategory', ['as' => 'SaveCategory', 'uses' => 'CategoryController@SaveCategory']);
	Route::get('/EditCategory/{id}', ['as' => 'EditCategory', 'uses' => 'CategoryController@EditCategory']);
	Route::post('/UpdateCategory', ['as' => 'UpdateCategory', 'uses' => 'CategoryController@UpdateCategory']);
	Route::post('/ChangeCategoryStatus', [ 'as' => 'ChangeCategoryStatus', 'uses' => 'CategoryController@ChangeCategoryStatus' ]);
	Route::post('/ChangeCategoryShowOnHome', [ 'as' => 'ChangeCategoryShowOnHome', 'uses' => 'CategoryController@ChangeCategoryShowOnHome' ]);

	/*----------------------------------------------------------------------------------------------
	Product Routes
	----------------------------------------------------------------------------------------------*/
	Route::get('/Product', ['as' => 'Product', 'uses' => 'ProductController@Product']);
	Route::get('/AddProduct', ['as' => 'AddProduct', 'uses' => 'ProductController@AddProduct']);
	Route::post('/SaveProduct', ['as' => 'SaveProduct', 'uses' => 'ProductController@SaveProduct']);
	Route::post('/ChangeProductStatus', [ 'as' => 'ChangeProductStatus', 'uses' => 'ProductController@ChangeProductStatus' ]);
	Route::get('/EditProduct/{id}', ['as' => 'EditProduct', 'uses' => 'ProductController@EditProduct']);
	Route::post('/UpdateProduct', ['as' => 'UpdateProduct', 'uses' => 'ProductController@UpdateProduct']);
	Route::post('/DeleteImage', [ 'as' => 'DeleteImage', 'uses' => 'ProductController@DeleteImage' ]);


	/*-------------------------------------------------------------------------------------------
	Variant Routes
	---------------------------------------------------------------------------------------------*/
	Route::get('/Variant', ['as' => 'Variant', 'uses' => 'VariantController@Variant']);
	Route::get('/AddVariant', ['as' => 'AddVariant', 'uses' => 'VariantController@AddVariant']);
	Route::post('/SaveVariant', ['as' => 'SaveVariant', 'uses' => 'VariantController@SaveVariant']);
	Route::get('/EditVariant/{id}', ['as' => 'EditVariant', 'uses' => 'VariantController@EditVariant']);
	Route::post('/DeleteVariant', [ 'as' => 'DeleteVariant', 'uses' => 'VariantController@DeleteVariant' ]);
	Route::post('/UpdateVariant', ['as' => 'UpdateVariant', 'uses' => 'VariantController@UpdateVariant']);
	Route::post('/ChangeVariantStatus', [ 'as' => 'ChangeVariantStatus', 'uses' => 'VariantController@ChangeVariantStatus' ]);

	/*-------------------------------------------------------------------------------------------
	User Routes
	---------------------------------------------------------------------------------------------*/
	Route::get('/User', ['as' => 'User', 'uses' => 'UserController@User']);
	Route::post('/ChangeUserStatus', [ 'as' => 'ChangeUserStatus', 'uses' => 'UserController@ChangeUserStatus' ]);
	Route::get('/ViewUser/{id}', ['as' => 'ViewUser', 'uses' => 'UserController@ViewUser']);
	
	
	/*-------------------------------------------------------------------------------------------
	Blog Routes
	---------------------------------------------------------------------------------------------*/
	Route::get('/Blog', ['as' => 'Blog', 'uses' => 'BlogController@Blog']);
	Route::get('/BlogCategory', ['as' => 'BlogCategory', 'uses' => 'BlogController@BlogCategory']);
	Route::get('/AddBlog', ['as' => 'AddBlog', 'uses' => 'BlogController@AddBlog']);
	Route::get('/AddBlogCategory', ['as' => 'AddBlogCategory', 'uses' => 'BlogController@AddBlogCategory']);
	Route::post('/SaveBlog', ['as' => 'SaveBlog', 'uses' => 'BlogController@SaveBlog']);
	Route::post('/SaveBlogCategory', ['as' => 'SaveBlogCategory', 'uses' => 'BlogController@SaveBlogCategory']);
	Route::get('/EditBlog/{id}', ['as' => 'EditBlog', 'uses' => 'BlogController@EditBlog']);
	Route::get('/EditBlogCategory/{id}', ['as' => 'EditBlogCategory', 'uses' => 'BlogController@EditBlogCategory']);
	Route::post('/UpdateBlog', ['as' => 'UpdateBlog', 'uses' => 'BlogController@UpdateBlog']);
	Route::post('/UpdateBlogCategory', ['as' => 'UpdateBlogCategory', 'uses' => 'BlogController@UpdateBlogCategory']);
	Route::post('/ChangeBlogStatus', [ 'as' => 'ChangeBlogStatus', 'uses' => 'BlogController@ChangeBlogStatus' ]);
	Route::post('/ChangeBlogCategoryStatus', [ 'as' => 'ChangeBlogCategoryStatus', 'uses' => 'BlogController@ChangeBlogCategoryStatus' ]);
	/*-------------------------------------------------------------------------------------------
	Content Management Routes
	---------------------------------------------------------------------------------------------*/
	Route::get('/Content', ['as' => 'Content', 'uses' => 'ContentController@Content']);
	Route::get('/AddContent', ['as' => 'AddContent', 'uses' => 'ContentController@AddContent']);
	Route::post('/SaveContent', ['as' => 'SaveContent', 'uses' => 'ContentController@SaveContent']);
	Route::get('/EditContent/{id}', ['as' => 'EditContent', 'uses' => 'ContentController@EditContent']);
	Route::post('/UpdateContent', ['as' => 'UpdateContent', 'uses' => 'ContentController@UpdateContent']);
	Route::post('/ChangeContentStatus', [ 'as' => 'ChangeContentStatus', 'uses' => 'ContentController@ChangeContentStatus' ]);
});

Route::get('/UserDashboard',['as'=>'UserDashboard','uses'=>'Front\LoginController@UserDashboard']);


/*Front Panel*/
Route::get('/',['as'=>'Home','uses'=>'Front\HomeController@Home']);
Route::get('/Category/{id}',['as'=>'Categories','uses'=>'Front\CategoryController@Category']);


Route::get('/register',['as'=>'Register','uses'=>'Front\LoginController@Register']);


Route::get('/login',['as'=>'Login','uses'=>'Front\LoginController@Login']);
Route::post('/CheckLogin',array('as'=>'CheckLogin','uses'=>'Front\LoginController@CheckLogin'));
Route::get('/Logout',['as'=>'Logout','uses'=>'Front\LoginController@Logout']);

Route::get('/forgot',['as'=>'Forgot','uses'=>'Front\LoginController@Forgot']);
Route::post('/CheckForgetPassword',['as'=>'CheckForgetPassword','uses'=>'Front\LoginController@CheckForgetPassword']);


Route::get('/ProductDetail/{id}', ['as' => 'ProductDetail', 'uses' => 'Front\ProductDetailController@ProductDetail']);


/*Cart*/
Route::get('/cart',array('as'=>'Cart','uses'=>'Front\CartController@CartList'));
Route::post('/AddToCart',array('as'=>'AddToCart','uses'=>'Front\CartController@AddToCart'));
Route::post('/CartSummary',array('as'=>'CartSummary','uses'=>'Front\CartController@CartSummary'));
Route::post('/RemoveCart',array('as'=>'RemoveCart','uses'=>'Front\CartController@RemoveCart'));
Route::post('/ApplyPromoCode',array('as'=>'ApplyPromoCode','uses'=>'Front\CartController@ApplyPromoCode'));
Route::post('/RemovePromoCode',array('as'=>'RemovePromoCode','uses'=>'Front\CartController@RemovePromoCode'));
Route::get('/checkout',array('as'=>'Checkout','uses'=>'Front\CartController@Checkout'));