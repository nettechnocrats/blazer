<?php
namespace App\Http\Controllers\Admin;
use Route;
use Auth, Hash;
use Session;
use Redirect;
use DB;
use Illuminate\Http\Request;
use Illuminate\Routing\ResponseFactory;
use App\Http\Controllers\Controller;
use App\Http\Models\Admin\Content;
use App\Http\Models\Admin\Pages;
use App\Helpers\Common;

class ContentController extends Controller 
{
	public function Content()
	{
		$Result['title']     = 'Content Management';
		$Result['Menu']      = 'Content';
		$Result['SubMenu'] 	 = '';
		$Result['Content'] 	 = Content::all();
		return View('Admin.Content.Content',$Result);
	}

	public function AddContent()
	{
		$Result['title'] 	   = 'Add Content';
		$Result['Menu']        = 'Content';
		$Result['SubMenu'] 	   = '';
		$Result['Pages']       = Pages::all();
		return View('Admin.Content.AddContent',$Result);
	}

	public function SaveContent(Request $request)
	{
		$Data   	               = $request->all();
		$ContentModel 			   = new Content;
		$ContentModel->pages_id    = $request->contentfor;
		$ContentModel->status      = $request->status;
		$ContentModel->content     = $request->content;
		$ContentModel->save();
		return redirect('Admin/Content')->with('success','Content Added Successfully');
	}

	public function EditContent($id)
	{
		$Result['title'] 		     = 'Edit Content';
		$Result['Menu'] 		     = 'Content';
		$Result['SubMenu'] 	         = '';
		$Result['id'] 	             = $id;
		$Result['Content']           = Content::find($id);
		$Result['Pages']             = Pages::all();
		return View('Admin.Content.EditContent',$Result);
	}

	public function UpdateContent(Request $request)
	{
	    $Data                      = $request->all();
		$id                        = $request->cid;
		$ContentModel 			   = new Content;
		$ContentModel              = Content::find($id);
		$ContentModel->pages_id    = $request->contentfor;
		$ContentModel->status      = $request->status;
		$ContentModel->content     = $request->content;
		$ContentModel->save();
		return redirect('Admin/Content')->with('success','Content Updated Successfully');
	}

	public function ChangeContentStatus(Request $request)
  	{
  		$Data   	            = $request->all();
		$id 		            = $Data['id'];
		$status	 	            = $Data['status'];
		$ContentModel 	        = new Content;
		$ContentModel           = Content::find($id);
		$ContentModel->status   = $request->status;
		$ContentModel->save();
		if($status==1){
			$Returnstatus = 0;
		}else{
			$Returnstatus = 1;
		}
		return $Returnstatus;
  	}
}