<?php
namespace App\Http\Controllers\Admin;
use Route;
use Auth, Hash;
use Session;
use Redirect;
use Illuminate\Http\Request;
use Illuminate\Routing\ResponseFactory;
use App\Http\Controllers\Controller;
use App\Helpers\Common;

class BannerController extends Controller
{
	public function __construct(Request $request)
	{		
		
	}
	public function List(Request $request)
	{
		$Result['title'] 		= 'Banner';
		$Result['Menu'] 		= 'Banner';
		$Result['SubMenu'] 	= '';
		return View('Admin.Banner.List',$Result);
	}
}