<?php
namespace App\Http\Controllers\Admin;
use Route;
use Auth, Hash;
use Session;
use Redirect;
use DB;
use Illuminate\Http\Request;
use Illuminate\Routing\ResponseFactory;
use App\Http\Controllers\Controller;
use App\Http\Models\Admin\Category;
use App\Helpers\Common;

class CategoryController extends Controller 
{
	public function __construct(Request $request)
	{		
		
	}
	public function Category()
	{
		$Result['title'] 		= 'Category';
		$Result['Menu'] 		= 'Category';
		$Result['SubMenu'] 	    = '';
		$Result['Category'] 	= Category::all();
		return View('Admin.Category.Category',$Result);
	}

	public function AddCategory()
	{
		$Result['title'] 		= 'Add Category';
		$Result['Menu'] 		= 'Category';
		$Result['SubMenu'] 	    = '';
		return View('Admin.Category.AddCategory',$Result);
	}

	public function SaveCategory(Request $request)
	{
		$filename = $request->image->getClientOriginalName();
		$request->image->move('public\Front\Category',$filename);
		$CategoryModel 			        = new Category;
		$CategoryModel->title            = $request->name;
		$CategoryModel->slug            = $request->slug;
		$CategoryModel->sort            = $request->sort;
		$CategoryModel->image           = $filename;
		$CategoryModel->seo_title       = $request->seo_title;
		$CategoryModel->seo_description = $request->seo_description;
		$CategoryModel->description     = $request->description;
		$CategoryModel->save();
		return redirect('Admin/Category')->with('success','Category Added Successfully');

	}

	public function DeleteCategory(Request $request)
	{
		$Data   	= $request->all();
		$id 		= $Data['id'];
		$CategoryModel = Category::find($id);
		$CategoryModel->delete();

	}	

	public function EditCategory($id)
	{
		$Result['title'] 		= 'Edit Category';
		$Result['Menu'] 		= 'Category';
		$Result['SubMenu'] 	    = '';
		$Result['id'] 	        = $id;
		$Result['Category']     = Category::find($id);
		return View('Admin.Category.EditCategory',$Result);

	}	

	public function UpdateCategory(Request $request)
	{
		if(!empty($request->image))
		{
		  $filename = $request->image->getClientOriginalName();	
		  $request->image->move('public\Front\Category',$filename);
		}else
		{
			$filename = $request->old_image;
		}
		$id                             = $request->id;
		$CategoryModel 			        = new Category;
		$CategoryModel                  = Category::find($id);
		$CategoryModel->title            = $request->name;
		$CategoryModel->slug            = $request->slug;
		$CategoryModel->sort            = $request->sort;
		$CategoryModel->image           = $filename;
		$CategoryModel->seo_title       = $request->seo_title;
		$CategoryModel->seo_description = $request->seo_description;
		$CategoryModel->description     = $request->description;
		$CategoryModel->save();
		return redirect('Admin/Category')->with('success','Category Updated Successfully');
	}

	public function ChangeCategoryStatus(Request $request)
  	{
  		$Data   	              = $request->all();
		$id 		              = $Data['id'];
		$status	 	              = $Data['status'];
		$CategoryModel 	          = new Category;
		$CategoryModel            = Category::find($id);
		$CategoryModel->status    = $request->status;
		$CategoryModel->save();
		if($status==1){
			$Returnstatus = 0;
		}else{
			$Returnstatus = 1;
		}
		return $Returnstatus;
  	}
  	public function ChangeCategoryShowOnHome(Request $request)
  	{
  		$Data   	              = $request->all();
		$id 		              = $Data['id'];
		$ShowOnHome	 	          = $Data['ShowOnHome'];
		$CategoryModel 	          = new Category;
		$CategoryModel            = Category::find($id);
		$CategoryModel->show_on_home    = $request->ShowOnHome;
		$CategoryModel->save();
		if($ShowOnHome==1){
			$ReturnShowOnHome = 0;
		}else{
			$ReturnShowOnHome = 1;
		}
		return $ReturnShowOnHome;
  	}
}