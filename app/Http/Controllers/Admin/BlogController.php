<?php
namespace App\Http\Controllers\Admin;
use Route;
use Auth, Hash;
use Session;
use Redirect;
use DB;
use Illuminate\Http\Request;
use Illuminate\Routing\ResponseFactory;
use App\Http\Controllers\Controller;
use App\Http\Models\Admin\Blog;
use App\Http\Models\Admin\BlogCategory;
use App\Helpers\Common;

class BlogController extends Controller 
{
	public function Blog()
	{
		$Result['title'] 		= 'Blog';
		$Result['Menu'] 		= 'Blog_List';
		$Result['SubMenu'] 	    = '';
		$Result['Blog'] 	    = Blog::all();
		// echo "<pre>";
		// print_r($Result['Blog']);
		// die;
		return View('Admin.Blog.Blog',$Result);
	}

	public function BlogCategory()
	{
		$Result['title'] 		= 'Blog Category';
		$Result['Menu'] 		= 'Blog_Category';
		$Result['SubMenu'] 	    = '';
		$Result['BlogCategory'] = BlogCategory::all();
		return View('Admin.Blog.BlogCategory',$Result);
	}

	public function AddBlog()
	{
		$Result['title'] 		= 'Add Blog';
		$Result['Menu'] 		= 'Blog_List';
		$Result['SubMenu'] 	    = '';
		$Result['BlogCategory'] = BlogCategory::all();
		return View('Admin.Blog.AddBlog',$Result);
	}

	public function AddBlogCategory()
		{
			$Result['title'] 		= 'Add Blog Category';
			$Result['Menu'] 		= 'Blog_Category';
			$Result['SubMenu'] 	    = '';
			return View('Admin.Blog.AddBlogCategory',$Result);
		}

	public function SaveBlog(Request $request)
	{
		$filename = $request->image->getClientOriginalName();
		$request->image->move('public\Front\Blog',$filename);
		$BlogModel 			        = new Blog;
		$BlogModel->category_id     = $request->category;
		$BlogModel->author          = $request->author;
		$BlogModel->title           = $request->name;
		$BlogModel->slug            = $request->slug;
		$BlogModel->description     = $request->description;
		$BlogModel->publish_date    = $request->date;
		$BlogModel->image           = $filename;
		$BlogModel->seo_title       = $request->seo_title;
		$BlogModel->seo_description = $request->seo_description;
		$BlogModel->status          = $request->status;
		$BlogModel->save();
		return redirect('Admin/Blog')->with('success','Blog Added Successfully');
	}

	public function SaveBlogCategory(Request $request)
	{
		$Data   	                    = $request->all();
		$BlogCategoryModel 			    = new BlogCategory;
		$BlogCategoryModel->category    = $request->name;
		$BlogCategoryModel->slug        = $request->slug;
		$BlogCategoryModel->status      = $request->status;
		$BlogCategoryModel->sort        = $request->sort;
		$BlogCategoryModel->save();
		return redirect('Admin/BlogCategory')->with('success','Blog Category Added Successfully');
	}

	public function EditBlog($id)
	{
		$Result['title'] 		        = 'Edit Blog';
		$Result['Menu'] 		        = 'Blog_List';
		$Result['SubMenu'] 	            = '';
		$Result['id'] 	                = $id;
		$Result['BlogCategory']         = BlogCategory::all();
		$Result['Blog']                 = Blog::find($id);
		return View('Admin.Blog.EditBlog',$Result);

	}

	public function EditBlogCategory($id)
	{
		$Result['title'] 		        = 'Edit Blog Category';
		$Result['Menu'] 		        = 'Blog_List';
		$Result['SubMenu'] 	            = '';
		$Result['id'] 	                = $id;
		$Result['BlogCategory']         = BlogCategory::find($id);
		return View('Admin.Blog.EditBlogCategory',$Result);

	}	

	public function UpdateBlog(Request $request)
	{
		if(!empty($request->image))
		{
		  $filename = $request->image->getClientOriginalName();	
		  $request->image->move('public\Front\Blog',$filename);
		}else
		{
			$filename = $request->old_image;
		}
		$id                         = $request->id;
		$BlogModel 			        = new Blog;
		$BlogModel                  = Blog::find($id);
		$BlogModel->category_id     = $request->category;
		$BlogModel->author          = $request->author;
		$BlogModel->title           = $request->name;
		$BlogModel->slug            = $request->slug;
		$BlogModel->description     = $request->description;
		$BlogModel->publish_date    = $request->date;
		$BlogModel->image           = $filename;
		$BlogModel->seo_title       = $request->seo_title;
		$BlogModel->seo_description = $request->seo_description;
		$BlogModel->status          = $request->status;
		$BlogModel->save();
		return redirect('Admin/Blog')->with('success','Blog Updated Successfully');
	}

	public function UpdateBlogCategory(Request $request)
	{
		$id                              = $request->id;
		$BlogCategoryModel 			     = new BlogCategory;
		$BlogCategoryModel               = BlogCategory::find($id);
		$BlogCategoryModel->category     = $request->name;
		$BlogCategoryModel->slug         = $request->slug;
		$BlogCategoryModel->sort         = $request->sort;
		$BlogCategoryModel->status       = $request->status;
		$BlogCategoryModel->save();
		return redirect('Admin/BlogCategory')->with('success','Blog Category Updated Successfully');
	}

	public function ChangeBlogStatus(Request $request)
  	{
  		$Data   	          = $request->all();
		$id 		          = $Data['id'];
		$status	 	          = $Data['status'];
		$BlogModel 	          = new Blog;
		$BlogModel            = Blog::find($id);
		$BlogModel->status    = $request->status;
		$BlogModel->save();
		if($status==1){
			$Returnstatus = 0;
		}else{
			$Returnstatus = 1;
		}
		return $Returnstatus;
  	}

  	public function ChangeBlogCategoryStatus(Request $request)
  	{
  		$Data   	                  = $request->all();
		$id 		                  = $Data['id'];
		$status	 	                  = $Data['status'];
		$BlogCategoryModel 	          = new BlogCategory;
		$BlogCategoryModel            = BlogCategory::find($id);
		$BlogCategoryModel->status    = $request->status;
		$BlogCategoryModel->save();
		if($status==1){
			$Returnstatus = 0;
		}else{
			$Returnstatus = 1;
		}
		return $Returnstatus;
  	}
}