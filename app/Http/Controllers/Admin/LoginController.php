<?php
namespace App\Http\Controllers\Admin;
use Route;
use Auth, Hash;
use Session;
use Redirect;
use DB;
use Illuminate\Http\Request;
use App\Http\Models\Admin\LoginModel;
use Illuminate\Routing\ResponseFactory;
use App\Http\Controllers\Controller;
use App\Helpers\Common;

class LoginController extends Controller 
{
	public function __construct(Request $request)
	{		
		$this->LoginModel = new LoginModel();
		
	}
	public function Login(Request $request)
	{
		$Result['title'] = 'Admin Login';
		return View('Admin.Login',$Result);
	}
	public function AdminLoginValidate(Request $request)
	{
		$Data = $request->all();
		$Email =$Data['email'];
		$Password =$Data['password'];

		$referer_url = route('Dashboard');
		if($Data['referer_url']){
			$referer_url = $Data['referer_url'];
		}

		$AdminLoginValidate = $this->LoginModel->AdminLoginValidate($Email,$Password);

		if(!empty($AdminLoginValidate))
		{
			Session::put('admin_id', $AdminLoginValidate->id);
			Session::put('admin_name', $AdminLoginValidate->name);
			Session::put('admin_image', asset('public/Admin/Profile/'.$AdminLoginValidate->image));
			Session::put('admin_login', 1);
			Session::save();

			$msg 		= 	Common::AlertErrorMsg('Success','Login Successfully.');
			$status = 1;
		}
		else
   	{
   		$msg 		= Common::AlertErrorMsg('Danger','Email Or Password Invalid.');
			$status = 0;
    	}

   	$arr['status'] 	= $status;
		$arr['msg'] 		= $msg;
		$arr['RefererURL'] 		= $referer_url;
		echo json_encode($arr);
		exit();
	}
	public function Forgot(){
		$Result['title'] = 'Forgot Password';
		return View('Admin.Forgot',$Result);
	}
	public function AdminLogout(Request $request)
	{
		Session::forget('admin_id');
		Session::forget('admin_name');
		Session::forget('admin_image');
		Session::forget('admin_login');

		$Msg = Common::AlertErrorMsg('Success','Log Out Successfully.');
		Session::flash('ErrorMsg', $Msg); 
		return redirect::route('AdminLogin');
	}
}