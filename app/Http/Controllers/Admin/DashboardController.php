<?php
namespace App\Http\Controllers\Admin;
use Route;
use Auth, Hash;
use Session;
use Redirect;
use DB;
use Illuminate\Http\Request;
use Illuminate\Routing\ResponseFactory;
use App\Http\Controllers\Controller;
use App\Helpers\Common;

class DashboardController extends Controller 
{
	public function __construct(Request $request)
	{		
		
	}
	public function Dashboard(Request $request)
	{
		$Result['title'] 		= 'Dashboard';
		$Result['Menu'] 		= 'Dashboard';
		$Result['SubMenu'] 	= '';
		return View('Admin.Dashboard',$Result);
	}
}