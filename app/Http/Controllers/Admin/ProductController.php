<?php
namespace App\Http\Controllers\Admin;
use Route;
use Auth, Hash;
use Session;
use Redirect;
use DB;
use Illuminate\Http\Request;
use Illuminate\Routing\ResponseFactory;
use App\Http\Controllers\Controller;
use App\Http\Models\Admin\Product;
use App\Http\Models\Admin\ProductImages;
use App\Http\Models\Admin\Collection;
use App\Http\Models\Admin\Variant;
use App\Http\Models\Admin\VarientAttributes;
use App\Http\Models\Admin\ProductAttribute;
use App\Http\Models\Admin\ProductVariant;
use App\Http\Models\Admin\Category;
use App\Helpers\Common;

class ProductController extends Controller 
{
	public function __construct(Request $request)
	{		
		
	}
	public function Product()
	{
		$Result['title'] 		= 'Product';
		$Result['Menu'] 		= 'Product';
		$Result['SubMenu'] 	    = '';
		$Result['Productc']     = \App\Http\Models\Admin\Product::with('category')->get();
		$Result['Product'] 	    = Product::all();
		return View('Admin.Product.Product',$Result);
	}

	public function AddProduct()
	{
		$Result['title'] 		       = 'Add Product';
		$Result['Menu'] 		       = 'Product';
		$Result['SubMenu'] 	           = '';
		$Result['Collection'] 	       = Collection::all();
		$Result['Category'] 	       = Category::all();
		$Result['Variant'] 	           = Variant::all();
		$Result['VariantAttributes']   = VarientAttributes::all();
		return View('Admin.Product.AddProduct',$Result);
	}

	public function SaveProduct(Request $request)
	{
		$Data   	                   = $request->all();
		$ProductModel 			       = new Product;
		$ProductModel->name            = $request->name;
		$ProductModel->slug            = $request->slug;
		$ProductModel->status          = $request->status;
		$ProductModel->sort            = $request->sort;
		$ProductModel->price           = $request->price;
		$ProductModel->compare_price   = $request->c_price;
		$ProductModel->collection_id   = $request->collection;
		$ProductModel->category_id     = $request->Category;
		$ProductModel->description     = $request->description;
		$ProductModel->offer		   = $request->offer;
		if($request->offer!='0')
		{
			$ProductModel->offer_p	   = $request->offer_p;			
		}
		$ProductModel->warranty 	   = $request->warranty;
		if($request->warranty!='')
		{
			$ProductModel->warranty_in = $request->warranty_in;	
		}
		$ProductModel->save();
		$product_id       	= $ProductModel->id;

		/* Insert Multiple Image */
		$ProductImageArr    = $request->image;
		$ProductImages 	    = new ProductImages;
		$Images             = array();
		if(!empty($ProductImageArr)){
			$Path           = 'public/Front/Product';
	        foreach($ProductImageArr as $file)
	        {
	            $name                       = $file->getClientOriginalName();
	            $file->move($Path,$name);
	            $ImageData['product_id']  = $product_id;
			    $ImageData['image']       = $name;
	            $Images[]                 = $ImageData;
	        }
		}
		
		ProductImages::insert($Images);
		/* Insert Multiple Image END */



//Insert Array Values--------------------------------------------------------------------------------
		if(!empty($Data['attributes'])){
            $VariantAttArr                     = array();
		    foreach ($Data['attributes'] as $attributes) {
			$VarintAttData['product_id']       = $product_id;
			$VarintAttData['attribute_id']     = $attributes;
			$VariantAttArr[]                   = $VarintAttData;
		}
		ProductAttribute::insert($VariantAttArr);
		}
//End----------------------------------------------------------------------------------------------

		if(!empty($Data['variant'])){
            $ProVariantArr                    = array();
		    foreach ($Data['variant'] as $variant) {
			$ProVarintData['product_id']      = $product_id;
			$ProVarintData['variant_id']      = $variant;
			$ProVariantArr[]                  = $ProVarintData;
		}
		ProductVariant::insert($ProVariantArr);
		}
		return redirect('Admin/Product')->with('success','Product Added Successfully');
	}

	public function DeleteCategory(Request $request)
	{
		$Data   	   = $request->all();
		$id 		   = $Data['id'];
		$CategoryModel = Category::find($id);
		$CategoryModel->delete();

	}	

	public function EditProduct($id)
	{
		$Result['title'] 		       = 'Edit Product';
		$Result['Menu'] 		       = 'Product';
		$Result['SubMenu'] 	           = '';
		$Result['id'] 	               = $id;
		$Result['Collection'] 	       = Collection::all();
		$Result['Category'] 	       = Category::all();
		$Result['Variant'] 	           = Variant::all();
		$Result['VariantAttributes']   = VarientAttributes::all();
		$Result['Product']             = Product::find($id);
		$Result['ProductImages']       = Product::find($id)->ProductImages;
		return View('Admin.Product.EditProduct',$Result);
	}

	public function DeleteImage(Request $request)
	{
		$Data   	               = $request->all();
		$id 		               = $Data['id'];
		$ProductImagesModel        = ProductImages::find($id);
		$ProductImagesModel->delete();
	}		

	public function UpdateCategory(Request $request)
	{
		if(!empty($request->image))
		{
		  $filename = $request->image->getClientOriginalName();	
		  $request->image->move('public\Front\Category',$filename);
		}else
		{
			$filename = $request->old_image;
		}
		$id                             = $request->id;
		$CategoryModel 			        = new Category;
		$CategoryModel                  = Category::find($id);
		$CategoryModel->name            = $request->name;
		$CategoryModel->slug            = $request->slug;
		$CategoryModel->sort            = $request->sort;
		$CategoryModel->image           = $filename;
		$CategoryModel->seo_title       = $request->seo_title;
		$CategoryModel->seo_description = $request->seo_description;
		$CategoryModel->description     = $request->description;
		$CategoryModel->save();
		return redirect('Admin/Category')->with('success','Category Updated Successfully');
	}

	public function ChangeProductStatus(Request $request)
  	{
  		$Data   	                 = $request->all();
		$id 		                 = $Data['id'];
		$status	 	              = $Data['status'];
		$ProductModel 	           = new Product;
		$ProductModel             = Product::find($id);
		$ProductModel->status     = $request->status;
		$ProductModel->save();
		if($status==1){
			$Returnstatus = 0;
		}else{
			$Returnstatus = 1;
		}
		return $Returnstatus;
  	}
  	public function ChangeCategoryShowOnHome(Request $request)
  	{
  		$Data   	              = $request->all();
		$id 		              = $Data['id'];
		$ShowOnHome	 	          = $Data['ShowOnHome'];
		$CategoryModel 	          = new Category;
		$CategoryModel            = Category::find($id);
		$CategoryModel->show_on_home    = $request->ShowOnHome;
		$CategoryModel->save();
		if($ShowOnHome==1){
			$ReturnShowOnHome = 0;
		}else{
			$ReturnShowOnHome = 1;
		}
		return $ReturnShowOnHome;
  	}
}