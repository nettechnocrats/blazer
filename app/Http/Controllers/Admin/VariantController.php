<?php
namespace App\Http\Controllers\Admin;
use Route;
use Auth, Hash;
use Session;
use Redirect;
use DB;
use Illuminate\Http\Request;
use Illuminate\Routing\ResponseFactory;
use App\Http\Controllers\Controller;
use App\Http\Models\Admin\Variant;
use App\Http\Models\Admin\VarientAttributes;
use App\Http\Models\Admin\ProductAttributes;
use App\Helpers\Common;

class VariantController extends Controller 
{
	public function Variant()
	{
		$Result['title'] 		  = 'Variant';
		$Result['Menu'] 		   = 'Variant';
		$Result['SubMenu'] 	 = '';
		$Result['Variant'] 	 = Variant::all();
		return View('Admin.Variant.Variant',$Result);
	}

	public function AddVariant()
	{
		$Result['title'] 		    = 'Add Variant';
		$Result['Menu'] 		     = 'Variant';
		$Result['SubMenu'] 	   = '';
		return View('Admin.Variant.AddVariant',$Result);
	}

	public function SaveVariant(Request $request)
	{
		$Data   	               = $request->all();
		$VariantModel 			   = new Variant;
		$VarientAttributesModel    = new VarientAttributes;
		$VariantModel->variant     = $request->name;
		$VariantModel->status      = $request->status;
		$VariantModel->sort        = $request->sort;
		$VariantModel->save();

		$variant_id       		      = $VariantModel->id;
		$VariantArr                   = array();
		foreach ($Data['attributes'] as $attributes) {
			$VarintData['variant_id'] = $variant_id;
			$VarintData['value']      = $attributes;
			$VariantArr[]             = $VarintData;
		}
		VarientAttributes::insert($VariantArr);
		return redirect('Admin/Variant')->with('success','Variant Added Successfully');
	}

	public function EditVariant($id)
	{
		$Result['title'] 		          = 'Edit Variant';
		$Result['Menu'] 		           = 'Variant';
		$Result['SubMenu'] 	         = '';
		$Result['id'] 	              = $id;
		$Result['Variant']           = Variant::find($id);
		$Result['VariantAttribute']  = Variant::find($id)->VarientAttributes/* VarientAttributes is Model Class name */;
		return View('Admin.Variant.EditVariant',$Result);

	}	

	public function DeleteVariant(Request $request)
	{
		$Data   	               = $request->all();
		$id 		               = $Data['id'];
		$attribute_id 		       = $Data['attribute_id'];
		$VarientAttributesModel    = VarientAttributes::find($id);
		$ProductAttributesModel    = ProductAttributes::find($attribute_id);
		$VarientAttributesModel->delete();
	}	

	public function UpdateVariant(Request $request)
	{
	$Data  = $request->all();

	
		$id                           = $request->id;
		$VariantModel 			      = new Variant;
		$VarientAttributesModel 	  = new VarientAttributes;
		$VariantModel                 = Variant::find($id);
		$VariantModel->variant        = $request->name;
		$VariantModel->status         = $request->status;
		$VariantModel->sort           = $request->sort;
		$VariantModel->save();
//Delete Atributes---------------------------------------------------------------------------
		if(!empty($Data['atributeid']))
		{
	    $atributeid 		          = $Data['atributeid'];
		$VarientAttributesModel::destroy($atributeid );
		}
//End Delete Atributes-----------------------------------------------------------------------		
		$variant_id       		      = $VariantModel->id;
		$VariantArr                   = array();
		foreach ($Data['attributes'] as $attributes) {
			$VarintData['variant_id'] = $variant_id;
			$VarintData['value']      = $attributes;
			$VariantArr[]             = $VarintData;
		}
		VarientAttributes::insert($VariantArr);
		return redirect('Admin/Variant')->with('success','Variant Updated Successfully');
	}

	public function ChangeVariantStatus(Request $request)
  	{
  		$Data   	             = $request->all();
		$id 		                  = $Data['id'];
		$status	 	              = $Data['status'];
		$VariantModel 	         = new Variant;
		$VariantModel           = Variant::find($id);
		$VariantModel->status   = $request->status;
		$VariantModel->save();
		if($status==1){
			$Returnstatus = 0;
		}else{
			$Returnstatus = 1;
		}
		return $Returnstatus;
  	}
}