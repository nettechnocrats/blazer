<?php
namespace App\Http\Controllers\Admin;
use Route;
use Auth, Hash;
use Session;
use Redirect;
use DB;
use Illuminate\Http\Request;
use Illuminate\Routing\ResponseFactory;
use App\Http\Controllers\Controller;
use App\Http\Models\Admin\User;
use App\Helpers\Common;

class UserController extends Controller 
{
	public function User()
	{
		$Result['title'] 		= 'User';
		$Result['Menu'] 		= 'User';
		$Result['SubMenu'] 	    = '';
		$Result['User'] 	    = User::all();
		return View('Admin.User.User',$Result);
	}

	public function ChangeUserStatus(Request $request)
  	{
  		$Data   	            = $request->all();
		$id 		            = $Data['id'];
		$status	 	            = $Data['status'];
		$UserModel 	            = new User;
		$UserModel              = User::find($id);
		$UserModel->status      = $request->status;
		$UserModel->save();
		if($status==1){
			$Returnstatus = 0;
		}else{
			$Returnstatus = 1;
		}
		return $Returnstatus;
  	}

  	public function ViewUser($id)
  	{
  		$Result['title'] 		= 'User Details';
		$Result['Menu'] 		= 'User';
		$Result['SubMenu'] 	    = '';
		$Result['id'] 	        = $id;
		$Result['User']         = User::find($id);
		return View('Admin.User.UserDetails',$Result);
  	}
}