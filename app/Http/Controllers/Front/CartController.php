<?php
namespace App\Http\Controllers\Front;
use Route;
use Auth, Hash;
use Validator;
use Session;
use Redirect;
use DB;
use Crypt;
use Anam\Phpcart\Cart;
use Illuminate\Http\Request;
use App\Http\Models\Front\CartModel;
use Illuminate\Routing\ResponseFactory;
use App\Http\Controllers\Controller;
use App\Helpers\Common;
class CartController extends Controller
{
	public function __construct(Request $request){
		$this->CartModel = new CartModel();
		$this->cart = new Cart();
	}
	public function CartList(){
		$CartList = $this->cart->getItems();
		$TotalCart = $this->cart->count();
		$Data['CartList'] = $CartList;
		$Data['TotalCart'] = $TotalCart;
		$Data['Menu'] = '';
		$Data['title'] 		 = 'Cart';
		$Data['Menu'] 		 = 'Home';
		return View('Front/Cart')->with($Data);
	}
	public function AddToCart(Request $request){
		$Data = $request->all();
		if($Data){
      $Cart['pro_id']   = $Data['pro_id'];
      $Cart['pro_url']  = $Data['pro_url'];
      $variant_type     = $Data['variant_type'];
      $Cart['id']       = $Cart['pro_id'];

      $variant_type_arr = explode(',', $variant_type);
      $attributes = array();
      foreach ($variant_type_arr as $variant) {
        $AttValue = $this->CartModel->GetAttributeData($Data[$variant]);
        $variant_arr['type'] = $variant;
        $variant_arr['value']   = $Data[$variant];
        $variant_arr['ValueName']= $AttValue->value;
        $attributes[] = $variant_arr;
      }
      $pro = $this->CartModel->GetSingalProduct($Cart['pro_id']);

      $img = $this->CartModel->GetFirstProductImage($Cart['pro_id']);

      $Cart['name'] = $pro->name;
      $Cart['cat_id'] = $pro->category_id;
      $Cart['price'] = $pro->price;
      $Cart['offer'] = $pro->offer;
      $Cart['compare_price'] = $pro->compare_price;
      $Cart['offer_p'] = $pro->offer_p;
      $Cart['pro_img'] = asset('public/Front/Product').'/'.$img->image;
      $Cart['quantity'] = $Data['quant'];
      $Cart['attributes'] = $attributes;

      $this->cart->add($Cart);
      return redirect()->route('Cart');
      exit();
    }else{
      return redirect('/');
      exit();
    }
	}
  public function CartSummary(Request $request){
    $Data   = $request->all();
    $CartCount = $this->cart->count();
    if($CartCount>0){
      $CartList = $this->cart->getItems();
      $subtotal = 0;
      $ExtraDiscount = 0;
      foreach ($CartList as $Cart) {
        $Discount = 0;
        $price = $Cart->price;
        if($Cart->offer==1){
          $Discount = ($Cart->compare_price - $Cart->price);
          $price = $Cart->compare_price;
        }
        $subtotal+= $price * $Cart->quantity;
        $ExtraDiscount+= $Discount * $Cart->quantity;
      }
      $tax_arr = $this->CartModel->ProductTax();
      $TotalAmt =  $this->cart->getTotal();
    ?>
        <table class="table">
          <tbody>
            <tr>
              <td><b>Sub-Total</b></td> 
              <td>$<?=number_format($subtotal,2)?></td>                   
            </tr>
            <tr>
              <td><b>Extra Discount</b></td>
              <td>-$<?=number_format($ExtraDiscount,2)?></td>
            </tr>
            <?php
            if(Session::get('DiscountPromoAmount') != ''){
              $TotalAmt = $TotalAmt - Session::get('DiscountPromoAmount');
            ?>
              <tr>
                <td><b>Promo Code (<?=Session::get('DiscountPromoCode')?>)</b></td>
                <td>-$<?= number_format(Session::get('DiscountPromoAmount'),2)?></td>
              </tr>
            <?php 
            }
            $TaxAmt = 0;
            if($tax_arr){
              foreach ($tax_arr as $tax) {
                $TaxPercentage = ($TotalAmt*$tax->percentage)/100;
                $TaxAmt+= $TaxPercentage;
                ?>
                <tr>
                  <td><b>Tax (<?=$tax->tax?>)</b></td>
                  <td>+$<?=number_format($TaxPercentage,2) ?></td>
                </tr>
              <?php
              }
            }
            ?>
            <tr>
              <td><b>Order Total</b></td>
              <td>$<?=number_format(($TotalAmt + $TaxAmt),2)?></td>
            </tr>
          </tbody>
        </table>
      <?php
      $CheckOutBtn = '<a href="'.route('Checkout').'">Proceed to Checkout</a>';
      if(Session::get('UserLogin')==0){
        $CheckOutBtn = '<a href="'.route('Login').'">Proceed to Checkout</a>';
      }
      if($Data['page']!='Checkout'){
      ?>
        <div class="cart-update1">
          <?=$CheckOutBtn?>
        </div>
        <div class="news-form promo_code">
          <p>Have a promo code?</p>
          <?php
          if(Session::get('DiscountPromoAmount') != ''){
          ?>
            <div class="input-group">
              <button class="btn btn-info btn-lg" onclick="RemovePromoCode()" type="button">Remove Promo Code</button>
            </div>
          <?php 
          }else{
          ?>
          <form action="" method="post" id="ApplyForm" name="ApplyForm">
            <div class="input-group">
              <input class="btn btn-lg" name="promo_code" id="promo_code" maxlength="15" type="text" placeholder="Promo Code">
              <button class="btn btn-info btn-lg" onclick="ApplyPromoCode()" type="button">APPLY</button>
              <span id="PromoCodeSmg" style="color:red"></span>
            </div>
          </form>
          <?php
          }
          ?>
        </div>
      <?php 
      }
    }
  }
  public function RemoveCart(Request $request){
    $Data   = $request->all();
    if($Data){
      $id = $Data['id'];
      $this->cart->remove($id);
      return $this->cart->count();
      exit();
    }else{
      return redirect('/');
      exit();
    }
  }
  public function ApplyPromoCode(Request $request){
    $Data   = $request->all();
    $promo_code = $Data['promo_code'];
    $promo = $this->CartModel->ApplyPromoCode($promo_code);
    if($promo){
      $msg = $this->ProductDiscount($promo);
    }else{
      $msg = 'Invalid promo code';
    }
    echo $msg;
    exit();
  }
  public function ProductDiscount($promo){
    $CartList = $this->cart->getItems();
    $status = 0;
    $msg = 1;
    $Discount = '';
    if($promo->coupon_for==1){ /*All Product*/
      $GetTotal = $this->cart->getTotal();
      if($GetTotal >= $promo->min_amount){
        $status = 1;
        if($promo->type==1){
          $fixed = $promo->fixed;
          $Discount = $fixed;
        }else{
          $percentage = $promo->percentage;
          $Discount = ($GetTotal * $percentage) / 100;
        }
      }else{
        $status = 0;
        $msg = 'Your minimum amount should be $'.$promo->min_amount.'.';
      }
    }else if($promo->coupon_for==2){ /*Selected Category*/
      $selected = explode(',', $promo->selected);
      $subtotal = 0;
      foreach ($CartList as $Cart) {
        if(in_array($Cart->cat_id, $selected)){
          $price = $Cart->price;
          $subtotal+= $price * $Cart->quantity;
        }
      }
      if($subtotal >= $promo->min_amount){
        $status = 1;
        if($promo->type==1){
          $fixed = $promo->fixed;
          $Discount = $fixed;
        }else{
          $percentage = $promo->percentage;
          $Discount = ($subtotal * $percentage) / 100;
        }
      }else{
        $status = 0;
        $msg = 'Not applicable.';
      }
    }else if($promo->coupon_for==3){ /*Selected Product*/
      $selected = explode(',', $promo->selected);
      $subtotal = 0;
      foreach ($CartList as $Cart) {
        if(in_array($Cart->pro_id, $selected)){
          $price = $Cart->price;
          $subtotal+= $price * $Cart->quantity;
        }
      }
      if($subtotal >= $promo->min_amount){
        $status = 1;
        if($promo->type==1){
          $fixed = $promo->fixed;
          $Discount = $fixed;
        }else{
          $percentage = $promo->percentage;
          $Discount = ($subtotal * $percentage) / 100;
        }
      }else{
        $status = 0;
        $msg = 'Not applicable.';
      }
    }
    if($status == 1){
      Session::put('DiscountPromoAmount', $Discount);   
      Session::put('DiscountPromoCode', $promo->coupon);
      Session::put('DiscountPromoCodeID', $promo->id);
      Session::save();    
    }
    return $msg;
  }
  public function RemovePromoCode(Request $request){
    Session::put('DiscountPromoAmount', '');
    Session::put('DiscountPromoCode', '');
    Session::put('DiscountPromoCodeID', '');
  }
  public function Checkout(){
    if($this->cart->count()==0){
      return redirect('/');
      exit();
    }
    if(Session::get('UserLogin')==0){
      return redirect('/');
      exit();
    }
    $user = $this->CartModel->GetUserDetails(Session::get('UserId'));
    $CartList = $this->cart->getItems();
    $TotalCart = $this->cart->count();
    $Data['CartList'] = $CartList;
    $Data['TotalCart'] = $TotalCart;
    $Data['user']     = $user;
    $Data['Menu'] = '';
    return View('Front/Checkout')->with($Data);
  }
}