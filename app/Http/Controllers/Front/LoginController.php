<?php
namespace App\Http\Controllers\Front;
use Route;
use Auth, Hash;
use Session;
use Redirect;
use Illuminate\Http\Request;
use App\Http\Models\Front\LoginModel;
use Illuminate\Routing\ResponseFactory;
use App\Http\Controllers\Controller;
use App\Helpers\Common;

class LoginController extends Controller
{
	public function __construct(Request $request)
	{		
		$this->LoginModel = new LoginModel();
	}
	public function Login()
	{
		if(Session::get('UserLogin')==1){
			return redirect()->route('Home');	
		}
		$Result['title'] 		= 'Login';
		$Result['Menu'] 		= '';
		return View('Front.Login',$Result);
	}
	public function CheckLogin(Request $request){
		$Data = $request->all();
		$Email 		= $Data['email'];
		$password = $Data['password'];

		$Check = $this->LoginModel->CheckLogin($Email,$password); 
		if($Check){
			if($Check->status==1){

				Session::put('UserId', $Check->id);
				Session::put('UserName', $Check->name);
				Session::put('UserEmail', $Check->email);
				Session::put('UserLogin', 1);
				Session::save();

				$arr['status'] 	= 1;
				$arr['msg'] 		= Common::AlertErrorMsg('Success','Login Successfully');
				$arr['RefererURL'] = route('Home');
			}else{
				$arr['status'] 	= 0;
				$arr['msg'] 		= Common::AlertErrorMsg('Danger','Your account has been deactivated');
			}
		}else{
			$arr['status'] 	= 0;
			$arr['msg'] 		= Common::AlertErrorMsg('Danger','Invalid email and password.');
		}
		echo json_encode($arr);
		exit();
	}
	public function Forgot()
	{
		if(Session::get('UserLogin')==1){
			return redirect()->route('Home');	
		}
		$Result['title'] 		= 'Forgot';
		$Result['Menu'] 		= '';
		return View('Front.Forgot',$Result);
	}
	public function CheckForgetPassword(Request $request){
		$Data = $request->all();
		$Email = $Data['email'];
		$Check = $this->LoginModel->CheckEmail($Email);
		if($Check == 0){
			$arr['status'] 	= 0;
			$arr['msg'] 		= Common::AlertErrorMsg('Danger','Invalid email.');
		}else{
			$arr['status'] 	= 1;
			$arr['msg'] 		= Common::AlertErrorMsg('Success','Forgot Successfully');
		}
		echo json_encode($arr);
		exit();
	}
	public function Logout(){
		Session::forget('UserId');
		Session::forget('UserName');
		Session::forget('UserEmail');
		Session::forget('UserLogin');
		return redirect()->route('Login');
	}
}