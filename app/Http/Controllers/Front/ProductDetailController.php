<?php
namespace App\Http\Controllers\Front;
use Route;
use DB;
use Auth, Hash;
use Session;
use Redirect;
use Illuminate\Http\Request;
use Illuminate\Routing\ResponseFactory;
use App\Http\Controllers\Controller;
use App\Http\Models\Admin\ProductImages;
use App\Http\Models\Admin\Product;
use App\Http\Models\Admin\Category;
use App\Helpers\Common;

class ProductDetailController extends Controller
{
	public function __construct(Request $request)
	{		
		$this->Product = new Product();
	}
	public function ProductDetail($id)
	{
		$Result['title'] 		 = 'Blazer USA';
		$Result['Menu'] 		 = 'Home';
		$Result['ProductImages'] =  ProductImages::find($id);
		$Result['Product']       =  Product::find($id);
		$ProductVariant         = $this->Product->ProductVariant($id);
		$ProductDetail = array();
		$ProColAttVal = DB::table('product_attribute as pro_att')
						     ->join('variants_attribute as Vatt','pro_att.attribute_id','=','Vatt.id')
						     ->where('pro_att.product_id',$id)
						     ->where('Vatt.variant_id',1)
						     ->select('Vatt.value')
						     ->get();
	     $ProSizeAttVal = DB::table('product_attribute as pro_att')
						     ->join('variants_attribute as Vatt','pro_att.attribute_id','=','Vatt.id')
						     ->where('pro_att.product_id',$id)
						     ->where('Vatt.variant_id',2)
						     ->select('Vatt.value')
						     ->get();
		$Result['pro_color']      = $ProColAttVal;
		$Result['ProductVariant'] = $ProductVariant;
		$Result['RelatedProduct']   = $this->Product->RelatedProduct($id);
		$Result['Category']         = Category::limit(12)->get();
		$Result['Size']           = $ProSizeAttVal; 
		
		return View('Front.ProductDetail',$Result);
	}
}