<?php
namespace App\Http\Controllers\Front;
use Route;
use DB;
use Auth, Hash;
use Session;
use Redirect;
use Illuminate\Http\Request;
use Illuminate\Routing\ResponseFactory;
use App\Http\Controllers\Controller;
use App\Http\Models\Admin\ProductImages;
use App\Http\Models\Admin\Product;
use App\Http\Models\Admin\Category;
use App\Helpers\Common;

class CategoryController extends Controller
{
	public function Category($id)
	{
		$Result['title'] 		 = 'Category';
		$Result['Menu'] 		 = 'Home';
		$Result['Category']      = Category::find($id);
		$ProductArr              = Product::where('category_id',$id)->get();
		$ProductList = array();
		foreach ($ProductArr as $row) {
			$ProImage    = ProductImages::where('product_id',$row->id)->first();
			$ProAttVal   = DB::table('product_attribute as pro_att')
						     ->join('variants_attribute as Vatt','pro_att.attribute_id','=','Vatt.id')
						     ->where('pro_att.product_id',$row->id)
						     ->where('Vatt.variant_id',1)
						     ->select('Vatt.value')
						     ->get();	     
			$Data['pro_name'] = $row->name;	
			$Data['pro_id'] = $row->id;	
			$Data['pro_slug'] = $row->slug;	
			$Data['pro_price'] = $row->price;	
			$Data['compare_price'] = $row->compare_price;	
			$Data['offer'] = $row->offer;	
			$Data['offer_p'] = $row->offer_p;	
			$Data['pro_image'] = asset('public/Front/Product/'.$ProImage->image);
			$Data['pro_color'] = $ProAttVal; 
			$ProductList[] = $Data;
		}
		$Result['ProductList'] 	 = $ProductList;
		$Result['Categoryl']         = Category::limit(12)->get();
		return View('Front.Category',$Result);
	}
}