<?php
namespace App\Http\Models\Front;
use Illuminate\Database\Eloquent\Model;
use DB;
class CartModel extends Model 
{
	
	public function GetSingalProduct($id){
		$HomeBanner = DB::table('products')->select('*')->where('status', 1)->where('id', $id)->first();
		return $HomeBanner;

	}
	public function GetFirstProductImage($product_id){
		$row = DB::table('products_images')->select('image')->where('product_id', $product_id)->first();
		return $row;
	}
	public function GetUserDetails($id){
		$row = DB::table('users')->where('id', $id)->first();
		return $row;
	}
	public function ProductTax(){
		$Tax = DB::table('tax')->select('tax','percentage')->where('status', 1)->get();
		return $Tax;
	}
	public function GetAttributeData($id){
		$row = DB::table('variants_attribute')->select('value')->where('id', $id)->first();
		return $row;
	}
	public function ApplyPromoCode($promo_code){
		$date = date('Y-m-d');
		$row = DB::table('coupon')
						->select('*')
						->where('coupon', $promo_code)
						->where('start_date','<=',$date)
						->where('end_date','>=',$date)
						->where('status',1)
						->first();
		return $row;
	}
}