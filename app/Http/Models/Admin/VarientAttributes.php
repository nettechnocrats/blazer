<?php
namespace App\Http\Models\Admin;
use Illuminate\Database\Eloquent\Model;
class VarientAttributes extends Model 
{
	protected $table = 'variants_attribute';
    public $primarykey = 'id';
    public $timestamps = false;

    public function Variant()
    {
    	return $this->belongsTo('App\Http\Models\Admin\Variant');
    }

}



