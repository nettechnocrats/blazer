<?php
namespace App\Http\Models\Admin;
use Illuminate\Database\Eloquent\Model;
class Blog extends Model 
{
	protected $table = 'blog';
    public $primarykey = 'id';
    public $timestamps = false;

    public function BlogCategory()
    {
        return $this->belongsTo('App\Http\Models\Admin\BlogCategory');
    }

}



