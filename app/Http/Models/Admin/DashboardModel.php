<?php
namespace App\Http\Models\Admin;
use Illuminate\Database\Eloquent\Model;
use DB;
class DashboardModel extends Model 
{
	public function AdminProfile($admin_id)
	{
		 return $AdminProfile = DB::table('admin')
                        ->select('*')
                        ->where('id',$admin_id)
                        ->first();
	}

	public function AdminProfileDetails($admin,$Details)
	{
		 $Add=DB::table('admin')->where('id',$admin)->update($Details);
         return true;
	}

	public function AdminSettingDetails($admin,$Details)
	{
        $AdminProfile = DB::table('admin')
                        ->select('*')
                        ->where('id',$admin)
                        ->first();
        if($AdminProfile->password==$Details['current_pass'])
        {
        	$Data['password'] =$Details['new_pass'];
        	$Add=DB::table('admin')->where('id',$admin)->update($Data);
        	return 1;
        }
        else
        {
        	return 2;
        }
	}

    public function TotalUsers()
    {
        return $TotalUsers = DB::table('users')
                        ->select('*')
                        ->count();
    }

     public function TotalProducts()
    {
        return $TotalProducts = DB::table('products')
                        ->select('*')
                        ->count();
    }

    public function AdminSocial()
    {
        return $TotalProducts = DB::table('social')
                        ->select('*')
                        ->get();
    }

    public function AdminSocialDetails($Data)
    {
        $ID = $Data['id'];
        $URL = $Data['url'];
        foreach ($ID as $key => $value) 
        {
           $Add['url'] = $URL[$key];
           $id = $ID[$key];
           DB::table('social')->where('id',$id)->update($Add);
        }
        return true;
    }
}



