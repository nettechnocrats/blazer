<?php
namespace App\Http\Models\Admin;
use Illuminate\Database\Eloquent\Model;
use DB;
class Product extends Model 
{
	protected $table = 'products';
    public $primarykey = 'id';
    public $timestamps = false;

    public function Collection()
    {
        return $this->belongsTo('App\Http\Models\Admin\Collection');
    }

    public function Category()
    {
        return $this->belongsTo('App\Http\Models\Admin\Category');
    }

    public function ProductImages()
    {
        return $this->hasMany('App\Http\Models\Admin\ProductImages');
    }

    public function ProductVariant($id){
        $ProductVariant_arr = DB::table('product_variant')
                        ->select('variants.id','variants.variant')
                        ->join('variants', 'variants.id','=','product_variant.variant_id','left')
                        ->where('product_variant.product_id', $id)
                        ->get();

        $ProductVariant = array();
        foreach ($ProductVariant_arr as $variant) {
            
            $ads['variant_id']      = $variant->id;
            $ads['variant_name']    = $variant->variant;
            $ads['AttributeList']   = $this->ProductAttribute($variant->id,$id);
            $ProductVariant[]       = $ads; 
        }
        return $ProductVariant;
    }
    public function ProductAttribute($variant_id,$id){
        $ProductAttribute = DB::table('product_attribute as pro_attr')
                                                ->select('vattri.id','vattri.value')
                                                ->join('variants_attribute as vattri','vattri.id','=','pro_attr.attribute_id')
                                                ->where('vattri.variant_id', $variant_id)
                                                ->where('pro_attr.product_id', $id)
                                                ->get();
        return $ProductAttribute;
    }



    public function RelatedProduct($id){
        $ProductList = DB::table('products')
                        ->select('products.id','products.slug','products.name','products.compare_price','products.offer','products.offer_p','products.price','category.slug as categoryslug')
                        ->join('category', 'category.id','=','products.category_id','left')
                        ->where('products.status', 1)
                        ->where('products.slug','!=', $id)
                        ->limit(5)
                        ->inRandomOrder()
                        ->get();
                        //->random(4);
        $ProductListArr = array();
        foreach ($ProductList as $pro) {
            $ads['id'] = $pro->id;
            $ads['slug'] = $pro->slug;
            $ads['name'] = $pro->name;
            $ads['compare_price'] = $pro->compare_price;
            $ads['offer'] = $pro->offer;
            $ads['offer_p'] = $pro->offer_p;
            $ads['price'] = $pro->price;
            $ads['categoryslug'] = $pro->categoryslug;
            $ads['image'] = $this->GetFirstProductImage($pro->id);
            $ProductListArr[] = $ads;   
        }
        return $ProductListArr;
    }

    public function GetFirstProductImage($id){
        $row = DB::table('products_images')->select('image')->where('product_id', $id)->first();
        return $row->image;
    }

}



