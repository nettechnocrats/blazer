<?php
namespace App\Http\Models\Admin;
use Illuminate\Database\Eloquent\Model;
class ProductImages extends Model 
{
	protected $table = 'products_images';
    public $primarykey = 'id';
    public $timestamps = false;

    public function Product()
    {
    	return $this->belongsTo('App\Http\Models\Admin\Product');
    }

}



