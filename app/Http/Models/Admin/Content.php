<?php
namespace App\Http\Models\Admin;
use Illuminate\Database\Eloquent\Model;
class Content extends Model 
{
	protected $table = 'content';
    public $primarykey = 'id';
    public $timestamps = false;

    public function Pages()
    {
        return $this->belongsTo('App\Http\Models\Admin\Pages');
    }

}



