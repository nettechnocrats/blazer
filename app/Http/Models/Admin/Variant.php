<?php
namespace App\Http\Models\Admin;
use Illuminate\Database\Eloquent\Model;
class Variant extends Model 
{
	protected $table = 'variants';
    public $primarykey = 'id';
    public $timestamps = false;

    public function VarientAttributes()
    {
    	return $this->hasMany('App\Http\Models\Admin\VarientAttributes');
    }

}



