<?php
namespace App\Http\Models\Admin;
use Illuminate\Database\Eloquent\Model;
class ProductVariant extends Model 
{
	protected $table = 'product_variant';
    public $primarykey = 'id';
    public $timestamps = false;

}



