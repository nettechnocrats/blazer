<?php
namespace App\Http\Models\Admin;
use Illuminate\Database\Eloquent\Model;
class BlogCategory extends Model 
{
	protected $table = 'blog_category';
    public $primarykey = 'id';
    public $timestamps = false;

}



