<?php
namespace App\Helpers;
use Session;
use Illuminate\Support\Facades\DB;
use Anam\Phpcart\Cart;
//use App\Http\Models\Front\HomeModel;

class Common {
	
	public function __construct()
	{		
		//$this->HomeModel = new HomeModel();
		$this->cart = new Cart();
	}
	public static function LimitTextWords($content=false,$limit=false,$stripTags=false,$ellipsis=false) 
	{
    if ($content && $limit) {
      $content = ($stripTags ? strip_tags($content) : $content);
      $content = explode(' ', $content, $limit+1);
      array_pop($content);
      if ($ellipsis) {
        array_push($content, '');
      }
      $content = implode(' ', $content);
    }
    return $content;
	}
	public static function AlertErrorMsg($ErrorType,$Msg) {
		if($ErrorType == 'Success'){
			$AlertErrorMsg = '<div class="alert alert-success background-success">
				    							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
														<i class="icofont icofont-close-line-circled text-white"></i>
													</button>
				    							'.$Msg.'
                  			</div>';
		}elseif($ErrorType == 'Danger'){
			$AlertErrorMsg = '<div class="alert alert-danger background-danger">
				    							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
														<i class="icofont icofont-close-line-circled text-white"></i>
													</button>
													'.$Msg.'
                  			</div>';
		}else{
			$AlertErrorMsg = '';
		}
		return $AlertErrorMsg;
	}
	public static function Links($numofrecords, $count, $page)
	{
		$per_page = $numofrecords;
		$previous_btn = true;
		$next_btn = true;
		$first_btn = true;
		$last_btn = true;
		$start = $page * $per_page;
		$cur_page = $page;
		$msg = "";

		$no_of_paginations = ceil($count / $per_page);

		if($count>0)
		{
			if ($cur_page >= 7) {
				$start_loop = $cur_page - 3;
				if ($no_of_paginations > $cur_page + 3)
				$end_loop = $cur_page + 3;
				else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
	    			$start_loop = $no_of_paginations - 6;
	    			$end_loop = $no_of_paginations;
				} else {
	    			$end_loop = $no_of_paginations;
				}
			} else {
				$start_loop = 1;
				if ($no_of_paginations > 7)
	    			$end_loop = 7;
				else
	    			$end_loop = $no_of_paginations;
			}
			$msg .= "<div class='product-pagination'><ul class='pagination'>";
			/*if ($first_btn && $cur_page > 1) {
					$msg .= "<li p='1' class='active' onclick='Pagination(1)'>First</li>";
			} else if ($first_btn) {
					$msg .= "<li p='1' class='inactive'>First</li>";
			}*/
			if ($previous_btn && $cur_page > 1) {
				$pre = $cur_page - 1;
				$msg .= "<li p='$pre' class='page-item'><a onclick='Pagination($pre)' class='page-link' href='javascript:void(0)'>Previous</a></li>";
			} else if ($previous_btn) {
				$msg .= "<li class='page-item inactive'><a class='page-link' href='javascript:void(0)'>Previous</a></li>";
			}
			for ($i = $start_loop; $i <= $end_loop; $i++) {
				if ($cur_page == $i)
	    			$msg .= "<li p='$i' class='page-item active'><a class='page-link' href='javascript:void(0)'>{$i}</a></li>";
				else
	    			$msg .= "<li p='$i' class='page-item'><a onclick='Pagination($i)' class='page-link' href='javascript:void(0)'>{$i}</a></li>";
				}
			    if ($next_btn && $cur_page < $no_of_paginations) {
			        $nex = $cur_page + 1;
			        $msg .= "<li p='$nex' class='page-item'><a onclick='Pagination($nex)' class='page-link' href='javascript:void(0)'>Next</a></li>";
			    } else if ($next_btn) {
			        $msg .= "<li class='inactive'><a class='page-link' href='javascript:void(0)'>Next</a></li>";
			    }
				/*if ($last_btn && $cur_page < $no_of_paginations) {
					$msg .= "<li p='$no_of_paginations' class='' onclick='Pagination($no_of_paginations)'>Last</li>";
				} else if ($last_btn) {
					$msg .= "<li p='$no_of_paginations' class='inactive'>Last</li>";
				}*/
				/*$total_string = "<span class='total' a='$no_of_paginations'>(page<b>" . $cur_page . "</b> of <b>$no_of_paginations</b> )</span>";
				$msg = $msg . "</ul>" . $goto . $total_string . "</div>";*/
				$msg = $msg . "</ul></div>";
				return $msg;
		}
		else
		{
			return '<div class="col-md-12 text-center" style="color:red"><strong>No Record Found</strong></div>';
		}
	}
	public function HeaderCategoryList(){
		$Result = DB::table('category')->select('id','title')->where('status',1)->orderBy('sort','ASC')->get();
		$Msg = '';
		foreach ($Result as $row) {
			$Msg.= '<option value="'.$row->id.'">'.$row->title.'</option>';
		}
		return $Msg;
	}
	public function HeaderPopUpCart(){
		$Result = '<ul class="cart_box">
			          <li>
			            <h4>Big Billy Zoot Suit<span class="pr">$175.00</span></h4>
			            <div class="cont_wrap">
			              <ul>
			                <li>SKU : 34980845</li>
			                <li>Color : Blue</li>
			                <li>Type: w/ Vest</li>
			                <li>Size: 46R</li>
			                <li>QTY: 1</li>
			              </ul>
			            </div>
			            <div class="img_wrap"><img src="'.asset('public/Front/img/cart_box1.jpg').'" alt=""></div>
			          </li>
			          <li>
			            <h4>Big Billy Zoot Suit<span class="pr">$175.00</span></h4>
			            <div class="cont_wrap">
			              <ul>
			                <li>SKU : 34980845</li>
			                <li>Color : Blue</li>
			                <li>Type: w/ Vest</li>
			                <li>Size: 46R</li>
			                <li>QTY: 1</li>
			              </ul>
			            </div>
			            <div class="img_wrap"><img src="'.asset('public/Front/img/cart_box1.jpg').'" alt=""></div>
			          </li>
			        </ul>
			        <div class="subtotal">
			          <span>Subtotal: (2 items)</span>
			          <span>$350.00</span>
			        </div>
			        <div class="free_ship">YOU QUALIFY FOR FREE SHIPPING!</div>
				        <div class="cart_btns">
				          <a class="btn1" href="'.route('Cart').'">View Full Cart</a>
				          <a class="btn1 red" href="'.route('Checkout').'">CHECKOUT</a>
				        </div>
				        <div class="complete_look">
				          <p>Complete Your Look</p>
				          <ul>
				            <li><a href="#"><img src="'.asset('public/Front/img/product1.jpg').'" alt=""/></a></li>
				            <li><a href="#"><img src="'.asset('public/Front/img/product2.jpg').'" alt=""/></a></li>
				            <li><a href="#"><img src="'.asset('public/Front/img/product3.jpg').'" alt=""/></a></li>
				            <li><a href="#"><img src="'.asset('public/Front/img/product4.jpg').'" alt=""/></a></li>
				          </ul>
				        </div>
			        ';
		return $Result;	        
	}
	public function TotalCartCount(){
		return $this->cart->count();
	}
}