<?php
namespace App\Helpers;
use Session;
use Illuminate\Support\Facades\DB;

class AdminCommon {
	
	public function __construct()
	{		
	
	}
	public static function AlertErrorMsg($ErrorType,$Msg) {
		if($ErrorType == 'Success'){
			$AlertErrorMsg = '<div class="alert alert-success alert-dismissible" role="alert">
				    							<button type="button" class="close" data-dismiss="alert">×</button>
				    							<div class="alert-icon"><i class="icon-check"></i></div>
				    							<div class="alert-message">
				      							<span>'.$Msg.'</span>
				    							</div>
                  			</div>';
		}elseif($ErrorType == 'Danger'){
			$AlertErrorMsg = '<div class="alert alert-danger alert-dismissible" role="alert">
				    							<button type="button" class="close" data-dismiss="alert">×</button>
				    							<div class="alert-icon"><i class="icon-close"></i></div>
				    							<div class="alert-message">
				      							<span>'.$Msg.'</span>
				    							</div>
                  			</div>';

		}elseif($ErrorType == 'Info'){
			$AlertErrorMsg = '<div class="alert alert-info alert-dismissible" role="alert">
				    							<button type="button" class="close" data-dismiss="alert">×</button>
				    							<div class="alert-icon"><i class="icon-info"></i></div>
				    							<div class="alert-message">
				      							<span>'.$Msg.'</span>
				    							</div>
                  			</div>';
		}elseif($ErrorType == 'Warning'){
			$AlertErrorMsg = '<div class="alert alert-warning alert-dismissible" role="alert">
				    							<button type="button" class="close" data-dismiss="alert">×</button>
				    							<div class="alert-icon"><i class="icon-exclamation"></i></div>
				    							<div class="alert-message">
				      							<span>'.$Msg.'</span>
				    							</div>
                  			</div>';
		}else{
			$AlertErrorMsg = '';
		}
		return $AlertErrorMsg;
	}

	public static function Pagination($numofrecords, $count, $page)
	{
		$per_page = $numofrecords;
		$previous_btn = true;
		$next_btn = true;
		$first_btn = true;
		$last_btn = true;
		$start = $page * $per_page;
		$cur_page = $page;
		$msg = "";
		$no_of_paginations = ceil($count / $per_page);
		if($count>0)
		{
			if ($cur_page >= 7) {
				$start_loop = $cur_page - 3;
				if ($no_of_paginations > $cur_page + 3)
				$end_loop = $cur_page + 3;
				else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
	   			$start_loop = $no_of_paginations - 6;
	   			$end_loop = $no_of_paginations;
				} else {
	   			$end_loop = $no_of_paginations;
				}
			} else {
				$start_loop = 1;
				if ($no_of_paginations > 7)
	   			$end_loop = 7;
				else
	   			$end_loop = $no_of_paginations;
			}
			$msg .= "<div class='product-pagination'><ul class='pagination'>";
			/*if ($first_btn && $cur_page > 1) {
					$msg .= "<li p='1' class='active' onclick='Pagination(1)'>First</li>";
			} else if ($first_btn) {
					$msg .= "<li p='1' class='inactive'>First</li>";
			}*/
			if ($previous_btn && $cur_page > 1) {
				$pre = $cur_page - 1;
				$msg .= "<li p='$pre' class='page-item'><a onclick='Pagination($pre)' class='page-link' href='javascript:void(0)'>Previous</a></li>";
			} else if ($previous_btn) {
				$msg .= "<li class='page-item inactive'><a class='page-link' href='javascript:void(0)'>Previous</a></li>";
			}
			for ($i = $start_loop; $i <= $end_loop; $i++) {
				if ($cur_page == $i)
	    		$msg .= "<li p='$i' class='page-item active'><a class='page-link' href='javascript:void(0)'>{$i}</a></li>";
				else
	    		$msg .= "<li p='$i' class='page-item'><a onclick='Pagination($i)' class='page-link' href='javascript:void(0)'>{$i}</a></li>";
			}
		  if ($next_btn && $cur_page < $no_of_paginations) {
		    $nex = $cur_page + 1;
		    $msg .= "<li p='$nex' class='page-item'><a onclick='Pagination($nex)' class='page-link' href='javascript:void(0)'>Next</a></li>";
		  } else if ($next_btn) {
		    $msg .= "<li class='inactive'><a class='page-link' href='javascript:void(0)'>Next</a></li>";
		  }
			/*if ($last_btn && $cur_page < $no_of_paginations) {
				$msg .= "<li p='$no_of_paginations' class='' onclick='Pagination($no_of_paginations)'>Last</li>";
			} else if ($last_btn) {
				$msg .= "<li p='$no_of_paginations' class='inactive'>Last</li>";
			}*/
			/*$total_string = "<span class='total' a='$no_of_paginations'>(page<b>" . $cur_page . "</b> of <b>$no_of_paginations</b> )</span>";
			$msg = $msg . "</ul>" . $goto . $total_string . "</div>";*/
			$msg = $msg . "</ul></div>";
			return $msg;
		}
		else
		{
			return '<div class="col-md-12 text-center" style="color:red"><strong>No Record Found</strong></div>';
		}
	}

	function SendMail($to_email, $from_email, $Cc, $Bcc, $subject, $message) { 

    $New_Line = "\n";

    $headers = "MIME-Version: 1.0" .$New_Line;
    $headers .= "Content-type: text/html; charset=iso-8859-1" .$New_Line;
    $headers .= "Content-Transfer-Encode: 7bit " .$New_Line;
    $headers .= "From: $from_email ".$New_Line;

    if(!empty($Cc)) {
        $headers .= "Cc: $Cc" .$New_Line;
    }
    if(!empty($Bcc)) {
        $headers .= "Bcc: $Bcc " .$New_Line;
    }

    $headers .= "X-Mailer: PHP " .$New_Line; // mailer
    $headers .= "Return-Path: < $to_email > " .$New_Line;  // Return path for errors        
    $mail_sent = mail($to_email, $subject, $message, $headers);
    return $mail_sent;
	}

}