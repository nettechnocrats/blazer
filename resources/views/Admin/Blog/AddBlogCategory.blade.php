@extends('Admin.Default')

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('public/Admin')}}/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="{{asset('public/Admin')}}/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
@stop

@section('BreadCrumb')
<div class="page-header card">
  <div class="row align-items-end">
    <div class="col-lg-8">
      <div class="page-header-title">
        <div class="d-inline">
          <h5>Add Variant</h5>
        </div>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="page-header-breadcrumb">
        <ul class=" breadcrumb breadcrumb-title">
          <li class="breadcrumb-item">
            <a href="{{route('Dashboard')}}"><i class="feather icon-home"></i></a>
          </li>
          <li class="breadcrumb-item"><a href="{{route('BlogCategory')}}">Blog Category List</a>
          </li>
          <li class="breadcrumb-item"><a>Add Blog Category</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
@stop
@section('content')
<div class="col-sm-8">
  <div class="card">
    <div class="card-header">
      <h5>Add Blog Category</h5>
    </div>
    <div class="card-block">
      <input type="hidden" id="url" value='{{url("Admin/")}}'>
      <form id="saveform" action="{{route('SaveBlogCategory')}}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group row">
          <div class="col-sm-6">
            <label>Name</label>
            <input type="text" class="form-control" name="name" id="name" onkeyup="Slug();">
          </div>
          <div class="col-sm-6">
            <label>Slug</label>
            <input type="text" class="form-control" name="slug" id="slug">
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-6">
                    <div class="form-group">
                      <strong for="country" class=" form-control-label">Status</strong>
                      <select  name="status" id="status"
                      class="form-control">
                        <option value="1">Active</option>
                        <option value="0">De-active</option>
                      </select>
                    </div>
                  </div>
          <div class="col-sm-6">
            <label>Sort</label>
            <input type="number" class="form-control" name="sort">
          </div>
        </div>

        <input type="submit" class="btn btn-success" value="Save" id="SubmitBtn">
      </form>
    </div>
  </div>
</div>
@stop

@section('javascript')
<script src="{{ asset('public/Admin/validation/blogcategory.js')}}"></script>

@stop