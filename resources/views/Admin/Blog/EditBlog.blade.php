@extends('Admin.Default')

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('public/Admin')}}/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="{{asset('public/Admin')}}/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
@stop

@section('BreadCrumb')
<div class="page-header card">
  <div class="row align-items-end">
    <div class="col-lg-8">
      <div class="page-header-title">
        <div class="d-inline">
          <h5>Edit Blog</h5>
        </div>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="page-header-breadcrumb">
        <ul class=" breadcrumb breadcrumb-title">
          <li class="breadcrumb-item">
            <a href="{{route('Dashboard')}}"><i class="feather icon-home"></i></a>
          </li>
          <li class="breadcrumb-item"><a href="{{route('Blog')}}">Blog List</a>
          </li>
          <li class="breadcrumb-item"><a>Edit Blog</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
@stop
@section('content')
<div class="col-sm-12">
  <div class="card">
    <div class="card-header">
      <h5>Edit Blog</h5>
    </div>
    <div class="card-block">
      <input type="hidden" id="url" value='{{url("Admin/")}}'>
      <form id="updateform" action="{{route('UpdateBlog')}}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group row">
          <div class="col-md-3">
                    <div class="form-group">
                      <strong for="country" class=" form-control-label">Category</strong>
                      <input type="hidden" value="{{$Blog->id}}" name="id">
                      <select  name="category" id="category"
                      class="form-control">
                        <option value="">Select Blog Category</option>
                        @foreach($BlogCategory as $category)
                          <option value="{{$category->id}}" @if($category->id==$Blog->category_id) selected @endif>{{$category->category}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
          <div class="col-sm-3">
            <label>Name</label>
            <input type="text" class="form-control" name="name" id="name" onkeyup="Slug();" value="{{$Blog->title}}">
          </div>
          <div class="col-sm-3">
            <label>Slug</label>
            <input type="text" class="form-control" name="slug" id="slug" value="{{$Blog->slug}}">
          </div>
          <div class="col-md-3">
                    <div class="form-group">
                      <strong for="country" class=" form-control-label">Status</strong>
                      <select  name="status" id="status"
                      class="form-control">
                        <option value="1"  @if($Blog->status==1) selected @endif>Active</option>
                        <option value="0"  @if($Blog->status==0) selected @endif>De-active</option>
                      </select>
                    </div>
                  </div>
          <div class="col-sm-3">
            <label>Author</label>
            <input type="text" class="form-control" name="author" id="author" value="{{$Blog->author}}">
          </div>
          <div class="col-sm-3">
            <label>Publish Date</label>
            <input type="date" class="form-control" name="date" value="{{$Blog->publish_date}}">
          </div>
          <div class="col-sm-3">
            <label>Upload File</label>
            <input type="file" class="form-control" name="image">
          </div>
          <div class="col-sm-3">
            <label>Image</label>
            <img src="{{asset('public\Front\Blog')}}/{{$Blog->image}}" width="80%" >
            <input type="hidden" value="{{$Blog->image}}" name="old_image">
          </div>
          <div class="col-sm-6">
            <label>SEO Title</label>
            <textarea rows="5" cols="5" class="form-control" name="seo_title" placeholder="SEO Title">{{$Blog->seo_title}}</textarea>
          </div>
          <div class="col-sm-6">
            <label>SEO Description</label>
            <textarea rows="5" cols="5" class="form-control" name="seo_description" placeholder="SEO Description">{{$Blog->seo_description}}</textarea>
          </div>
          <div class="col-sm-12">
            <label>Description</label>
            <textarea rows="5" cols="5" class="form-control" id="description" name="description" placeholder="Category Description">{{$Blog->description}}</textarea>
          </div>
        </div>
        <input type="submit" class="btn btn-success" value="Update" id="UpdateBtn">
      </form>
    </div>
  </div>
</div>
@stop

@section('javascript')

<script src="{{asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
<script>
  CKEDITOR.replace( 'description' );
</script>
<script src="{{ asset('public/Admin/validation/blogcategory.js')}}"></script>

@stopz