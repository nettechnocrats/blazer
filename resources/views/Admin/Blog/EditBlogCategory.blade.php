@extends('Admin.Default')

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('public/Admin')}}/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="{{asset('public/Admin')}}/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
@stop

@section('BreadCrumb')
<div class="page-header card">
  <div class="row align-items-end">
    <div class="col-lg-8">
      <div class="page-header-title">
        <div class="d-inline">
          <h5>Edit Blog Category</h5>
        </div>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="page-header-breadcrumb">
        <ul class=" breadcrumb breadcrumb-title">
          <li class="breadcrumb-item">
            <a href="{{route('Dashboard')}}"><i class="feather icon-home"></i></a>
          </li>
          <li class="breadcrumb-item"><a href="{{route('BlogCategory')}}">Blog Category List</a>
          </li>
          <li class="breadcrumb-item"><a>Edit Blog Category</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
@stop
@section('content')
<div class="col-sm-12">
  <div class="card">
    <div class="card-header">
      <h5>Edit Blog Category</h5>
    </div>
    <div class="card-block">
      <input type="hidden" id="url" value='{{url("Admin/")}}'>
      <form id="updateform" action="{{route('UpdateBlogCategory')}}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group row">
          <div class="col-sm-4">
            <label>Name</label>
            <input type="hidden" value="{{$BlogCategory->id}}" name="id" onkeyup="Slug();">
            <input type="text" class="form-control" name="name" id="name" value="{{$BlogCategory->category}}">
          </div>
          <div class="col-sm-6">
            <label>Slug</label>
            <input type="text" class="form-control" name="slug" id="slug" value="{{$BlogCategory->slug}}">
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-2">
                    <div class="form-group">
                      <strong for="country" class=" form-control-label">Status</strong>
                      <select  name="status" id="status"
                      class="form-control">
                        <option value="1"  @if($BlogCategory->status==1) selected @endif>Active</option>
                        <option value="0"  @if($BlogCategory->status==0) selected @endif>De-active</option>
                      </select>
                    </div>
                  </div>
          <div class="col-sm-2">
            <label>Sort</label>
            <input type="number" class="form-control" name="sort" value="{{$BlogCategory->sort}}">
          </div>
        </div>
        <input type="submit" class="btn btn-success" value="Update" id="UpdateBtn">
      </form>
    </div>
  </div>
</div>
@stop

@section('javascript')
<script src="{{ asset('public/Admin/validation/blogcategory.js')}}"></script>

@stop