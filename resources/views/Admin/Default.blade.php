<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
  <title>{{$title}}</title>
  
  <link rel="shortcut icon" type="image/x-icon" href="{{asset('public/Front/logo.png')}}">
  
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Quicksand:500,700" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{asset('public/Admin')}}/bower_components/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{asset('public/Admin')}}/assets/pages/waves/css/waves.min.css" type="text/css" media="all">
  <link rel="stylesheet" type="text/css" href="{{asset('public/Admin')}}/assets/icon/feather/css/feather.css">
  <link rel="stylesheet" type="text/css" href="{{asset('public/Admin')}}/assets/css/font-awesome-n.min.css">
  <link rel="stylesheet" type="text/css" href="{{asset('public/Admin')}}/assets/css/style.css">
  <link rel="stylesheet" type="text/css" href="{{asset('public/Admin')}}/assets/css/widget.css">
  <link rel="stylesheet" type="text/css" href="{{asset('public/Admin')}}/assets/css/local.css">

  @yield('css')
</head>

<body>
  <input type="hidden" id="url" value='{{route("AdminLogin")}}'>
  <input type="hidden" id="csrf_token" value='{{ csrf_token() }}'>
  <div class="loader-bg">
    <div class="loader-bar"></div>
  </div>

  <div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">
      <nav class="navbar header-navbar pcoded-header">
        <div class="navbar-wrapper">
          <div class="navbar-logo">
            <a href="{{route('Dashboard')}}">
              <img class="img-fluid" src="{{asset('public/Front/logo.png')}}" alt="Logo" />
            </a>
            <a class="mobile-menu" id="mobile-collapse" href="#!">
              <i class="feather icon-menu icon-toggle-right"></i>
            </a>
            <a class="mobile-options waves-effect waves-light">
              <i class="feather icon-more-horizontal"></i>
            </a>
          </div>
          <div class="navbar-container container-fluid">
            <ul class="nav-right">
              <li class="user-profile header-notification">
                <div class="dropdown-primary dropdown">
                  <div class="dropdown-toggle" data-toggle="dropdown">
                    <img src="{{Session::get('admin_image')}}" class="img-radius" alt="User-Profile-Image">
                    <span>{{Session::get('admin_name')}}</span>
                    <i class="feather icon-chevron-down"></i>
                  </div>
                  <ul class="show-notification profile-notification dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                    <li>
                      <a href="#!">
                        <i class="feather icon-settings"></i> Settings
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i class="feather icon-user"></i> Profile
                      </a>
                    </li>
                    <li>
                      <a href="{{route('AdminLogout')}}">
                        <i class="feather icon-log-out"></i> Logout
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
          <nav class="pcoded-navbar">
            <div class="nav-list">
              <div class="pcoded-inner-navbar main-menu">
                <ul class="pcoded-item pcoded-left-item">
                  <li class=" @if($Menu=='Dashboard')active @endif">
                    <a href="{{route('Dashboard')}}" class="waves-effect waves-dark">
                      <span class="pcoded-micon">
                        <i class="feather icon-home"></i>
                      </span>
                      <span class="pcoded-mtext">Dashboard</span>
                    </a>
                  </li>
                  <li class="@if($Menu=='Category')active @endif">
                    <a href="{{route('Category')}}" class="waves-effect waves-dark">
                      <span class="pcoded-micon">
                        <i class="fa fa-microchip"></i>
                      </span>
                      <span class="pcoded-mtext">Categorys</span>
                    </a>
                  </li>
                  <li class="@if($Menu=='Variant')active @endif">
                    <a href="{{route('Variant')}}" class="waves-effect waves-dark">
                      <span class="pcoded-micon">
                        <i class="fas fa-book"></i>
                      </span>
                      <span class="pcoded-mtext">Variants</span>
                    </a>
                  </li>
                  <li class="@if($Menu=='Product')active @endif">
                    <a href="{{route('Product')}}" class="waves-effect waves-dark">
                      <span class="pcoded-micon">
                       <i class="fas fa-archive"></i>
                      </span>
                      <span class="pcoded-mtext">Products</span>
                    </a>
                  </li>
                  <li class="@if($Menu=='User')active @endif">
                    <a href="{{route('User')}}" class="waves-effect waves-dark">
                      <span class="pcoded-micon">
                        <i class="fas fa-users"></i>
                      </span>
                      <span class="pcoded-mtext">Users</span>
                    </a>
                  </li>
                   <li class="@if($Menu=='Order')active @endif">
                    <a href="" class="waves-effect waves-dark">
                      <span class="pcoded-micon">
                        <i class="fas fa-users"></i>
                      </span>
                      <span class="pcoded-mtext">Orders</span>
                    </a>
                  </li>
                  <ul class="pcoded-item pcoded-left-item">
                    <li class="pcoded-hasmenu">
                      <a href="javascript:void(0)" class="waves-effect waves-dark">
                      <span class="pcoded-micon">
                      <i class="feather icon-box"></i>
                      </span>
                      <span class="pcoded-mtext">Blog</span>
                      </a>
                    <ul class="pcoded-submenu">
                      <li class="@if($Menu=='Blog_Category')active @endif">
                      <a href="{{route('BlogCategory')}}" class="waves-effect waves-dark">
                      <span class="pcoded-mtext">Blog Category</span>
                      </a>
                      </li>
                      <li class="@if($Menu=='Blog_List')active @endif">
                      <a href="{{route('Blog')}}" class="waves-effect waves-dark">
                      <span class="pcoded-mtext">Blog List</span>
                      </a>
                      </li>
                    </ul>
                </ul>
                <li class="@if($Menu=='Content')active @endif">
                    <a href="{{route('Content')}}" class="waves-effect waves-dark">
                      <span class="pcoded-micon">
                        <i class="fas fa-book"></i>
                      </span>
                      <span class="pcoded-mtext">Content Management</span>
                    </a>
                  </li>
              </div>
            </div>
          </nav>
          <div class="pcoded-content">
            @yield('BreadCrumb')
            <div class="pcoded-inner-content">
              <div class="main-body">
                <div class="page-wrapper">
                  <div class="page-body">
                    <div class="row">          
                      @yield('content')
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div id="styleSelector"></div>
        </div>
      </div>
    </div>
  </div>
  
  
  <script src="{{asset('public/Admin')}}/bower_components/jquery/js/jquery.min.js"></script>
  <script src="{{asset('public/Admin')}}/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
  <script src="{{asset('public/Admin')}}/bower_components/popper.js/js/popper.min.js"></script>
  <script src="{{asset('public/Admin')}}/bower_components/bootstrap/js/bootstrap.min.js"></script>
  <script src="{{asset('public/Admin')}}/assets/pages/waves/js/waves.min.js" ></script>
  <script src="{{asset('public/Admin')}}/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
  
  <script src="{{asset('public/Admin')}}/assets/js/markerclusterer.js" ></script>
  <script src="{{asset('public/Admin')}}/assets/js/pcoded.min.js" ></script>
  <script src="{{asset('public/Admin')}}/assets/js/vertical/vertical-layout.min.js" ></script>
  <script src="{{asset('public/Admin')}}/assets/js/script.min.js"></script>

  <script src="{{ asset('public/Admin')}}/assets/js/jquery.validate.min.js"></script>
  <script src="{{ asset('public/Admin')}}/assets/js/additional-methods.min.js"></script> 
  
  @yield('javascript')

</body>
</html>