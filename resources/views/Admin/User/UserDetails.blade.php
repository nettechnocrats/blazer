@extends('Admin.Default')

@section('css')
@stop

@section('BreadCrumb')
<div class="page-header card">
  <div class="row align-items-end">
    <div class="col-lg-8">
      <div class="page-header-title">
        <div class="d-inline">
          <h5>User Details</h5>
        </div>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="page-header-breadcrumb">
        <ul class=" breadcrumb breadcrumb-title">
          <li class="breadcrumb-item">
            <a href="{{route('Dashboard')}}"><i class="feather icon-home"></i></a>
          </li>
          <li class="breadcrumb-item">
            <a href="{{route('User')}}">User List</a>
          </li>
          <li class="breadcrumb-item"><a>{{$title}}</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="col-lg-12">
      @if(\Session::has('success'))
      <div class="alert alert-success background-success">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i class="icofont icofont-close-line-circled text-white"></i>
      </button>
      <p>{{\Session::get('success')}}</p>
    </div>
    @endif
    </div>

@stop
@section('content')
<div class="col-sm-12">
  <div class="card">
    <div class="card-header">
      <h5>User Details</h5>
    </div>
    <div class="card-block">
      <div class="dt-responsive table-responsive">
        <div class="col-lg-4">
            <table class="table">
              <tbody>
                <tr>
                <th>Name:</th>
                <td>{{$User->name}}</td>
              </tr>
              <tr>
                <th>Email:</th>
                <td>{{$User->email}}</td>
              </tr>
              <tr>
                <th>Mobile:</th>
                <td>{{$User->mobile}}</td>
              </tr>
              <tr>
                <th>Active:</th>
                <td>
                @if($User->status==1)
                <span style="color:green;font-style:bold;">YES</span>
                @else
                <span style="color:red;font-style:bold;">NO</span>
                @endif
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@stop

@section('javascript')

<script src="{{ asset('public/Admin/validation/user.js')}}"></script>

@stop