@extends('Admin.Default')

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('public/Admin')}}/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="{{asset('public/Admin')}}/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
@stop

@section('BreadCrumb')
<div class="page-header card">
  <div class="row align-items-end">
    <div class="col-lg-8">
      <div class="page-header-title">
        <div class="d-inline">
          <h5>User</h5>
        </div>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="page-header-breadcrumb">
        <ul class=" breadcrumb breadcrumb-title">
          <li class="breadcrumb-item">
            <a href="{{route('Dashboard')}}"><i class="feather icon-home"></i></a>
          </li>
          <li class="breadcrumb-item"><a>{{$title}}</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="col-lg-12">
      @if(\Session::has('success'))
      <div class="alert alert-success background-success">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i class="icofont icofont-close-line-circled text-white"></i>
      </button>
      <p>{{\Session::get('success')}}</p>
    </div>
    @endif
    </div>

@stop
@section('content')
<div class="col-sm-12">
  <div class="card">
    <div class="card-header">
      <h5>User List</h5>
    </div>
    <div class="card-block">
      <div class="dt-responsive table-responsive">
        <table id="footer-search" class="table table-striped table-bordered nowrap">
          <thead>
            <tr>
              <th>Name</th>
              <th>Email</th>
              <th>Mobile</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($User as $u)
            <tr id="Row_<?php echo $u->id; ?>">
              <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
              <td>{{$u->name}}</td>
              <td>{{$u->email}}</td>
              <td>{{$u->mobile}}</td>
              <td id="StatusDiv_<?php echo $u->id; ?>" style="text-align:center;">
            <?php 
            if($u->status=='0')
            {
              ?>
                <a href="javascript:void(0);" id="StatusHref_{{$u->id}}" class="btn btn-danger"
                  onclick="ChangeUserStatus('<?php echo $u->id; ?>',1);">
                  Deactive
                </a>                
              <?php
            }
            else if($u->status=='1')
            {
              ?>
                <a href="javascript:void(0);" id="StatusHref_{{$u->id}}" class="btn btn-success"
                   onclick="ChangeUserStatus('<?php echo $u->id; ?>',0);">
                  Active
                </a>
              <?php
            }             
            ?>
          </td>
              <td>
                <a href="{{url('Admin/ViewUser')}}/{{$u->id}}" class="btn btn-success btn-xs" title="Edit"><i class="fa fa-eye" aria-hidden="true" ></i>View Details</a>
              </td>
            </tr>
            @endforeach
          </tbody>
          <tfoot>
            <tr>
              <th>Name</th>
              <th>Email</th>
              <th>Mobile</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>
@stop

@section('javascript')
<script src="{{asset('public/Admin')}}/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="{{asset('public/Admin')}}/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{asset('public/Admin')}}/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{asset('public/Admin')}}/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>

<script type="text/javascript">
$('#footer-search tfoot th').each(function(){
  var title=$(this).text();$(this).html('<input type="text" class="form-control" placeholder="Search '+title+'" />');
});
var table=$('#footer-search').DataTable();
table.columns().every(function(){
  var that=this;
  $('input',this.footer()).on('keyup change',function(){
    if(that.search()!==this.value){
      that.search(this.value).draw();
    }
  });
});
</script>
<script src="{{ asset('public/Admin/validation/user.js')}}"></script>

@stop