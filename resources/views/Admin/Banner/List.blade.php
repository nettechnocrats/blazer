@extends('Admin.Default')

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('public/Admin')}}/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="{{asset('public/Admin')}}/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
@stop

@section('BreadCrumb')
<div class="page-header card">
  <div class="row align-items-end">
    <div class="col-lg-8">
      <div class="page-header-title">
        <div class="d-inline">
          <h5>Category</h5>
        </div>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="page-header-breadcrumb">
        <ul class=" breadcrumb breadcrumb-title">
          <li class="breadcrumb-item">
            <a href="{{route('Dashboard')}}"><i class="feather icon-home"></i></a>
          </li>
          <li class="breadcrumb-item"><a>{{$title}}</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
@stop
@section('content')
<div class="col-sm-12">
  <div class="card">
    <div class="card-header">
      <h5>Category List</h5>
    </div>
    <div class="card-block">
      <div class="dt-responsive table-responsive">
        <table id="footer-search" class="table table-striped table-bordered nowrap">
          <thead>
            <tr>
              <th>Name</th>
              <th>Description</th>
              <th>Image</th>
              <th>Sort</th>
              <th>Status</th>
              <th>Show on Home Page</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Michael Bruce</td>
              <td>Javascript Developer</td>
              <td>Singapore</td>
              <td>29</td>
              <td>2011/06/27</td>
              <td>$183,000</td>
              <td>$183,000</td>
            </tr>
            <tr>
              <td>Michael Bruce</td>
              <td>Javascript Developer</td>
              <td>Singapore</td>
              <td>29</td>
              <td>2011/06/27</td>
              <td>$183,000</td>
              <td>$183,000</td>
            </tr>
            <tr>
              <td>Donna Snider</td>
              <td>Customer Support</td>
              <td>New York</td>
              <td>27</td>
              <td>2011/01/25</td>
              <td>$112,000</td>
              <td>$112,000</td>
            </tr>
          </tbody>
          <tfoot>
            <tr>
              <th>Name</th>
              <th>Position</th>
              <th>Office</th>
              <th>Age</th>
              <th>Start date</th>
              <th>Salary</th>
              <td></td>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>
@stop

@section('javascript')
<script src="{{asset('public/Admin')}}/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>

<script src="{{asset('public/Admin')}}/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{asset('public/Admin')}}/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{asset('public/Admin')}}/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>

<script type="text/javascript">
$('#footer-search tfoot th').each(function(){
  var title = $(this).text();
  $(this).html('<input type="text" class="form-control" placeholder="Search '+title+'" />');
});
var table=$('#footer-search').DataTable();
table.columns().every(function(){
  var that=this;
  $('input',this.footer()).on('keyup change',function(){
    if(that.search()!==this.value){
      that.search(this.value).draw();
    }
  });
});
</script>

@stop