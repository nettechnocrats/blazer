@extends('Admin.Default')

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('public/Admin')}}/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="{{asset('public/Admin')}}/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
@stop

@section('BreadCrumb')
<div class="page-header card">
  <div class="row align-items-end">
    <div class="col-lg-8">
      <div class="page-header-title">
        <div class="d-inline">
          <h5>Add Product</h5>
        </div>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="page-header-breadcrumb">
        <ul class=" breadcrumb breadcrumb-title">
          <li class="breadcrumb-item">
            <a href="{{route('Dashboard')}}"><i class="feather icon-home"></i></a>
          </li>
          <li class="breadcrumb-item"><a href="{{route('Product')}}">Product List</a>
          </li>
          <li class="breadcrumb-item"><a>Add Product</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
@stop
@section('content')
<div class="col-sm-12">
  <div class="content mt-3">
    <div class="animated fadeIn"> 
      <input type="hidden" id="url" value='{{url("Admin/")}}'>
      <form id="saveform" action="{{route('SaveProduct')}}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row">
          <div class="col-lg-4">
            <div class="card">
              <div class="card-header">
                <i class="fa fa-plus"></i> 
                <strong>Product General Info</strong>
              </div>
              <div class="card-body card-block"> 
                <div class="form-group">
                  <strong for="country" class=" form-control-label">Name</strong>
                  <input  type="text" id="name" name="name" 
                  placeholder="Name" class="form-control" onkeyup="Slug();">
                </div>
                <div class="form-group">
                  <strong for="country" class=" form-control-label">Slug</strong>
                  <input type="text" id="slug" name="slug" 
                  placeholder="Slug" class="form-control">
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <strong for="country" class=" form-control-label">Status</strong>
                      <select  name="status" id="status"
                      class="form-control">
                        <option value="1">Active</option>
                        <option value="0">De-active</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">                    
                    <div class="form-group">
                    <strong for="country" class=" form-control-label">Sort</strong>
                     <input type="number" name="sort" id="sort" class="form-control">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="card">
              <div class="card-header">
                <i class="fa fa-plus"></i> 
                <strong>Product Price Info</strong>
              </div>
              <div class="card-body card-block">
              <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"> 
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <strong for="country" class=" form-control-label">Price</strong>
                      <input  type="text" id="price" name="price" 
                      placeholder="Price" class="form-control prc">
                      <span id="priceErr"></span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <strong for="country" class=" form-control-label">Compare Price</strong>
                      <input type="text" id="c_price" name="c_price" 
                      placeholder="Compare Price" class="form-control prc">
                      <span id="c_priceErr"></span>
                    </div>
                  </div>
                </div>
                <strong for="country" class=" form-control-label">Warranty</strong>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <select  name="warranty" id="warranty" 
                      class="form-control">
                        <option value="">Select</option>
                        @for($w=1;$w<=12;$w++)
                        <option value="{{$w}}">{{$w}}</option>
                        @endfor
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <select name="warranty_in" id="warranty_in" 
                      class="form-control">
                        <option value="">Select</option>                                          
                        <option value="M">Month(s)</option>
                        <option value="Y">Year(s)</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <strong for="country" class=" form-control-label">Any Offer?</strong>
                      <select name="offer" id="offer" 
                      class="form-control">                                         
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <strong for="country" class=" form-control-label">Offer %</strong>
                      <input  type="text" id="offer_p" name="offer_p" 
                      placeholder="Offer %" class="form-control prc">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="card">
              <div class="card-header">
              <i class="fa fa-plus"></i> <strong>Product Category Info</strong>
              </div>
              <div class="card-body card-block">
                <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"> 
                <div class="form-group">
                  <strong for="country" class=" form-control-label">Collection</strong>
                  <select name="collection" id="collection" 
                  class="form-control">
                    <option value="">Select Collection</option>
                    @foreach($Collection as $Col)
                    <option value="{{$Col->id}}">{{$Col->colname}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group"> 
                  <strong for="country" class=" form-control-label">Category</strong>
                  <select name="Category" id="Category" 
                  class="form-control">
                    <option value="">Select Category</option>
                    @foreach($Category as $C)
                    <option value="{{$C->id}}">{{$C->title}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">             
          <div class="col-lg-6">
            <div class="card">
              <div class="card-header">
                <i class="fa fa-plus"></i> 
                <strong>Product Variant</strong>
              </div>
              <div class="card-body card-block product-variant" style="overflow: scroll;height: 165px">
              @foreach($Variant as $v)
                <label class="container1"><strong>{{$v->variant}}</strong>
                  <input type="checkbox" name="variant[]" value="{{$v->id}}" id="variant_{{$v->id}}">          
                @foreach($VariantAttributes as $va)
                  @if($v->id==$va->variant_id)
                    <label class="container1 cont">{{$va->value}}
                      <input type="checkbox" name="attributes[]" value="{{$va->id}}" id="attribute_{{$va->id}}">
                    </label>
                    <br/>
                  @endif
                @endforeach                 
                </label>
                <br/>
              @endforeach
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="card">
              <div class="card-header">
                <i class="fa fa-plus"></i> 
                <strong>Product Images</strong>
              </div>
              <div class="card-body card-block">
                <h1>Images</h1>
                <div id="ImagesDiv">
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <input type="file" name="image[]" id="image" 
                        class="form-control-sm form-control upld" alt="ImagePrev" >
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <center>
                          <img src="{{ asset('public/Front/Category/noproduct.png') }}" height="50" width="50" id="ImagePrev">
                        </center>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <a href="javascript:void(0);" onclick="AddMoreSection();" class="btn btn-success">Add More</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-header">
                <h4>Product Other Info.</h4>
              </div>
              <div class="card-body">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="home-tab" 
                  data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><strong>Product Description</strong></a>
                  </li>
                </ul>
                <div class="tab-content pl-3 p-1" id="myTabContent">
                  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class="form-group">
                      <textarea name="description" id="product_description" 
                      class="form-control-sm form-control" rows="5"></textarea>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="card">
          <div class="card-footer">
            <input type="submit" class="btn btn-success" value="Save" id="SubmitBtn">
            </button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<input type="hidden" name="url" id="url" value='{{URL::to("/")}}'>
@stop

@section('javascript')

<script src="{{asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
<script>
  CKEDITOR.replace( 'description' );
</script>
<script src="{{ asset('public/Admin/validation/product.js')}}"></script>

@stop