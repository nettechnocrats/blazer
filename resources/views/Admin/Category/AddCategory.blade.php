@extends('Admin.Default')

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('public/Admin')}}/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="{{asset('public/Admin')}}/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
@stop

@section('BreadCrumb')
<div class="page-header card">
  <div class="row align-items-end">
    <div class="col-lg-8">
      <div class="page-header-title">
        <div class="d-inline">
          <h5>Add category</h5>
        </div>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="page-header-breadcrumb">
        <ul class=" breadcrumb breadcrumb-title">
          <li class="breadcrumb-item">
            <a href="{{route('Dashboard')}}"><i class="feather icon-home"></i></a>
          </li>
          <li class="breadcrumb-item"><a href="{{route('Category')}}">Category List</a>
          </li>
          <li class="breadcrumb-item"><a>Add Category</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
@stop
@section('content')
<div class="col-sm-12">
  <div class="card">
    <div class="card-header">
      <h5>Add category</h5>
    </div>
    <div class="card-block">
      <input type="hidden" id="url" value='{{url("Admin/")}}'>
      <form id="saveform" action="{{route('SaveCategory')}}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group row">
          <div class="col-sm-4">
            <label>Name</label>
            <input type="text" class="form-control" name="name" id="name" onkeyup="Slug();">
          </div>
          <div class="col-sm-3">
            <label>Slug</label>
            <input type="text" class="form-control" name="slug" id="slug">
          </div>
          <div class="col-sm-2">
            <label>Sort</label>
            <input type="number" class="form-control" name="sort">
          </div>
          <div class="col-sm-3">
            <label>Upload File</label>
            <input type="file" class="form-control" name="image">
          </div>
          <div class="col-sm-6">
            <label>SEO Title</label>
            <textarea rows="5" cols="5" class="form-control" name="seo_title" placeholder="SEO Title"></textarea>
          </div>
          <div class="col-sm-6">
            <label>SEO Description</label>
            <textarea rows="5" cols="5" class="form-control" name="seo_description" placeholder="SEO Description"></textarea>
          </div>
          <div class="col-sm-12">
            <label>Description</label>
            <textarea rows="5" cols="5" class="form-control" id="description" name="description" placeholder="Category Description"></textarea>
          </div>
        </div>
        <input type="submit" class="btn btn-success" value="Save" id="SubmitBtn">
      </form>
    </div>
  </div>
</div>
@stop

@section('javascript')

<script src="{{asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
<script>
  CKEDITOR.replace( 'description' );
</script>
<script src="{{ asset('public/Admin/validation/category.js')}}"></script>

@stop