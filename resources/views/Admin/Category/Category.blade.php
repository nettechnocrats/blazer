@extends('Admin.Default')

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('public/Admin')}}/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="{{asset('public/Admin')}}/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
@stop

@section('BreadCrumb')
<div class="page-header card">
  <div class="row align-items-end">
    <div class="col-lg-8">
      <div class="page-header-title">
        <div class="d-inline">
          <h5>Category</h5>
        </div>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="page-header-breadcrumb">
        <ul class=" breadcrumb breadcrumb-title">
          <li class="breadcrumb-item">
            <a href="{{route('Dashboard')}}"><i class="feather icon-home"></i></a>
          </li>
          <li class="breadcrumb-item"><a>{{$title}}</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="col-lg-12">
      @if(\Session::has('success'))
      <div class="alert alert-success background-success">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i class="icofont icofont-close-line-circled text-white"></i>
      </button>
      <p>{{\Session::get('success')}}</p>
    </div>
    @endif
    </div>

@stop
@section('content')
<div class="col-sm-12">
  <div class="card">
    <div class="card-header">
      <h5>Category List</h5>
      <a href="{{route('AddCategory')}}" class="btn btn-primary" style="float: right;" title="Add category"><i class="fa fa-plus" aria-hidden="true"></i>Add Category</a>
    </div>
    <div class="card-block">
      <div class="dt-responsive table-responsive">
        <table id="footer-search" class="table table-striped table-bordered nowrap">
          <thead>
            <tr>
              <th>Name</th>
              <th>Description</th>
              <th>Image</th>
              <th>Sort</th>
              <th>Status</th>
              <th>Show on Home Page</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($Category as $c)
            <tr id="Row_<?php echo $c->id; ?>">
              <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
              <td>{{$c->title}}</td>
              <td>{!! \Illuminate\Support\Str::words($c->description, 4,'....')  !!}</td>
              <td><img class="img-fluid" src="{{asset('public\Front\Category')}}/{{$c->image}}" height="70px" width="80%" alt="Logo" /></td>
              <td>{{$c->sort}}</td>
              <td id="StatusDiv_<?php echo $c->id; ?>" style="text-align:center;">
            <?php 
            if($c->status=='0')
            {
              ?>
                <a href="javascript:void(0);" id="StatusHref_{{$c->id}}" class="btn btn-danger"
                  onclick="ChangeCategoryStatus('<?php echo $c->id; ?>',1);">
                  Deactive
                </a>                
              <?php
            }
            else if($c->status=='1')
            {
              ?>
                <a href="javascript:void(0);" id="StatusHref_{{$c->id}}" class="btn btn-success"
                   onclick="ChangeCategoryStatus('<?php echo $c->id; ?>',0);">
                  Active
                </a>
              <?php
            }             
            ?>
          </td>
              <td style="text-align:center;" >
                <?php 
            if($c->show_on_home=='0')
            {
              ?>
                <a href="javascript:void(0);" id="show_on_homeHref_{{$c->id}}" class="btn btn-danger"
                  onclick="ChangeCategoryShowOnHome('<?php echo $c->id; ?>',1);">
                  <i class="fa fa-times" aria-hidden="true"></i>
                </a>                
              <?php
            }
            else if($c->show_on_home=='1')
            {
              ?>
                <a href="javascript:void(0);" id="show_on_homeHref_{{$c->id}}" class="btn btn-success"
                   onclick="ChangeCategoryShowOnHome('<?php echo $c->id; ?>',0);">
                  <i class="fa fa-check-square" aria-hidden="true"></i>
                </a>
              <?php
            }             
            ?>
              </td>
              <td>
                <a href="{{url('Admin/EditCategory')}}/{{$c->id}}" class="btn btn-success btn-xs" title="Edit"><i class="fa fa-edit" aria-hidden="true" ></i></a>
                    &nbsp;&nbsp;
                <a href="javascript:void(0);" onclick="DeleteCategory('<?php echo $c->id; ?>',0);" class="btn btn-danger btn-xs" title="Delete"><i class="fa fa-trash" aria-hidden="true" ></i></a>
              </td>
            </tr>
            @endforeach
          </tbody>
          <tfoot>
            <tr>
              <th>Name</th>
              <th>Description</th>
              <th>Image</th>
              <th>Sort</th>
              <th>Status</th>
              <th>Show on Home Page</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>
@stop

@section('javascript')
<script src="{{asset('public/Admin')}}/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="{{asset('public/Admin')}}/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{asset('public/Admin')}}/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{asset('public/Admin')}}/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>

<script type="text/javascript">
$('#footer-search tfoot th').each(function(){
  var title=$(this).text();$(this).html('<input type="text" class="form-control" placeholder="Search '+title+'" />');
});
var table=$('#footer-search').DataTable();
table.columns().every(function(){
  var that=this;
  $('input',this.footer()).on('keyup change',function(){
    if(that.search()!==this.value){
      that.search(this.value).draw();
    }
  });
});
</script>
<script src="{{ asset('public/Admin/validation/category.js')}}"></script>

@stop