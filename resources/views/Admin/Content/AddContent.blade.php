@extends('Admin.Default')

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('public/Admin')}}/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="{{asset('public/Admin')}}/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
@stop

@section('BreadCrumb')
<div class="page-header card">
  <div class="row align-items-end">
    <div class="col-lg-8">
      <div class="page-header-title">
        <div class="d-inline">
          <h5>Add Variant</h5>
        </div>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="page-header-breadcrumb">
        <ul class=" breadcrumb breadcrumb-title">
          <li class="breadcrumb-item">
            <a href="{{route('Dashboard')}}"><i class="feather icon-home"></i></a>
          </li>
          <li class="breadcrumb-item"><a href="{{route('Variant')}}">Variant List</a>
          </li>
          <li class="breadcrumb-item"><a>Add Variant</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
@stop
@section('content')
<div class="col-sm-12">
  <div class="card">
    <div class="card-header">
      <h5>Add Variant</h5>
    </div>
    <div class="card-block">
      <input type="hidden" id="url" value='{{url("Admin/")}}'>
      <form id="saveform" action="{{route('SaveContent')}}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group row">
          <div class="col-md-4">
            <div class="form-group">
              <strong for="country" class=" form-control-label">Content For</strong>
              <select  name="contentfor" id="contentfor"
              class="form-control">
                @foreach($Pages as $p)
                <option value="{{$p->id}}">{{$p->page}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <strong for="country" class=" form-control-label">Status</strong>
               <select  name="status" id="status"
                      class="form-control">
                        <option value="1">Active</option>
                        <option value="0">De-active</option>
                      </select>
               
              </select>
            </div>
          </div>
          <div class="col-sm-12">
            <label>Content</label>
            <textarea rows="5" cols="5" class="form-control" id="content" name="content" placeholder="Page Content"></textarea>
          </div>
        </div>

        <input type="submit" class="btn btn-success" value="Save" id="SubmitBtn">
      </form>
    </div>
  </div>
</div>
@stop

@section('javascript')

<script src="{{asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
<script>
  CKEDITOR.replace( 'content' );
</script>
<script src="{{ asset('public/Admin/validation/variant.js')}}"></script>

@stop