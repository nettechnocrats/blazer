@extends('Front.Default')

@section('content')
<section class="breadcrumb">
  <div class="container">
    <ul>
      <li><a href="{{route('Home')}}">Home</a><i class="fa fa-angle-right" aria-hidden="true"></i></li>
      <li><a href="#">{{$Product->Category['title']}}</a><i class="fa fa-angle-right" aria-hidden="true"></i></li>
      <li>{{$Product->name}}</li>
    </ul>
  </div>
</section>

<section class="product_detail">
  <form action="<?=url('AddToCart')?>" method="post" name="AddToCartForm" id="AddToCartForm" class="rating-form">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="hidden" name="pro_id" value="<?=$Product->id?>">
          <input type="hidden" name="pro_url" value="<?=url()->full()?>">
  <div class="container">
    <h2 class="heading1">SHOP MORE ZOOT SUITS</h2>
    <div class="pro_det">
      <div class="left_side">
        <ul class="pro_sl">
          <li><img src="{{asset('public/Front/Product')}}/{{$ProductImages->image}}" alt=""/></li>
          <li><img src="img/pro_sl1.jpg" alt=""/></li>
          <li><img src="img/pro_sl1.jpg" alt=""/></li>
        </ul>
        <ul class="pro_sl_cl">
          @foreach($pro_color as $color)
          <li><a style="background:{{$color->value}};" href="#"></a></li>
          @endforeach
          
        </ul>
      </div>
      <div class="right_side">
        <div class="head">
          <div class="head_left">
            <h2 class="heading1">{{$Product->name}}</h2>
            <h4>${{$Product->price}}</h4>
          </div>
          <div class="head_right">
              <fieldset class="form-group">
              <legend class="form-legend">Rating:</legend>
                <div class="form-item">
                  <input id="rating-5" name="rating" type="radio" value="5" />
                  <label for="rating-5" data-value="5">
                    <span class="rating-star">
                      <i class="fa fa-star grey"></i>
                      <i class="fa fa-star gold"></i>
                    </span>
                  </label>
                  <input id="rating-4" name="rating" type="radio" value="4" checked />
                  <label for="rating-4" data-value="4">
                    <span class="rating-star">
                      <i class="fa fa-star grey"></i>
                      <i class="fa fa-star gold"></i>
                    </span>
                  </label>
                  <input id="rating-3" name="rating" type="radio" value="3" />
                  <label for="rating-3" data-value="3">
                    <span class="rating-star">
                      <i class="fa fa-star grey"></i>
                      <i class="fa fa-star gold"></i>
                    </span>
                  </label>
                  <input id="rating-2" name="rating" type="radio" value="2" />
                  <label for="rating-2" data-value="2">
                    <span class="rating-star">
                      <i class="fa fa-star grey"></i>
                      <i class="fa fa-star gold"></i>
                    </span>
                  </label>
                  <input id="rating-1" name="rating" type="radio" value="1" />
                  <label for="rating-1" data-value="1">
                    <span class="rating-star">
                      <i class="fa fa-star grey"></i>
                      <i class="fa fa-star gold"></i>
                    </span>
                  </label>
                </div>
              </fieldset>
            <div class="social_footer">
              <a href="#"><img src="{{asset('public/Front')}}/img/social_footer1.png" alt=""></a>
              <a href="#"><img src="{{asset('public/Front')}}/img/social_footer2.png" alt=""></a>
              <a href="#"><img src="{{asset('public/Front')}}/img/social_footer3.png" alt=""></a>
              <a href="#"><img src="{{asset('public/Front')}}/img/social_footer4.png" alt=""></a>
            </div>
          </div>
        </div>
        <div class="pro_dea_fil">
          <div class="row">
                <?php
                $variant = array();
                foreach ($ProductVariant as $Variant) {
                  $variant[] = $Variant['variant_name'];
                ?>
                <div class="col-md-4">
                  <p>Select <?=$Variant['variant_name']?></p>
                  <select id="<?=$Variant['variant_name']?>" onchange="HideMsg('Err_<?=$Variant['variant_name']?>');" name="<?=$Variant['variant_name']?>" class="form-control">
                    <option value="0" selected>Select <?=$Variant['variant_name']?></option>
                    <?php
                    foreach ($Variant['AttributeList'] as $Attribute) {
                      echo '<option value="'.$Attribute->id.'">'.$Attribute->value.'</option>';
                    }
                    ?>
                  </select>
                  <span id="Err_<?=$Variant['variant_name']?>" style="color:red"></span>
                </div>
                <?php
                }
                ?>
              </div>
              <input type="hidden" id="variant_type" name="variant_type" value="<?php echo implode(',', $variant);?>">
              <br><br>
          <div class="row">
            <div class="col-lg-4 col-md-6 qty">
              <div class="form-group">
                <label class="comm_label">Quantity</label>
                <select class="inputfield sm" name="quant">
                  <option>1</option>
                  <option>2</option>
                  <option>3</option>
                  <option>4</option>
                </select>
              </div>
            </div>
            <div class="col-md-12"><a class="btn1 big" href="javascript:void(0)" onclick="AddToCart()">Add to Cart</a></div>
          </div>
        </div>
        <div class="pro_det_dec">
          <p>{{$Product->description}}</p>
          <ul>
            <li>1.<strong>Gender: </strong> {{$Product->gender}}</li>
            <li>2.<strong>Age Group: </strong> {{$Product->age_group}}</li>
            <li>3. Lorem ipsum dolor sit amet consectetur</li>
            <li>4. Lorem ipsum dolor sit amet consectetur</li>
            <li>5. Lorem ipsum dolor sit amet consectetur</li>
            <li>6. Lorem ipsum dolor sit amet consectetur</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</form>
</section>

<section class="cat_product">
  <div class="container">
    <h2 class="heading1 text-center">Similar Styles</h2>
    <ul class="product_box">
      @foreach($RelatedProduct as $rp)
      <li>
        <a href="{{url('ProductDetail')}}/{{$rp['id']}}"></a>
        <div class="img_wrap"><img src="{{asset('public/Front/Product')}}/{{$rp['image']}}" alt=""></div>
        <div class="cont_wrap">
          <h5>{{$rp['name']}}</h5>
          <div class="color_price">
            <div class="pro_color">
              <span style="background:#131313;"></span>
              <span style="background:#6c6c6c;"></span>
              <p>2 Colors</p>
            </div>
            <div class="pro_price">${{$rp['price']}}</div>
          </div>
        </div>
      </li>
      @endforeach
    </ul>
  </div>
</section>

<section class="comm_Sec">
  <div class="container">
    <h2 class="heading1 text-center">Shopping Categories<span class="sub_h1">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</span></h2>
    <ul class="category_box">
      @foreach($Category as $c)
      <li style="background-image:url({{asset('public/Front/Category')}}/{{$c->image}}">
        <div class="cont_wrap">
          <h5>{{$c->title}}</h5>
          <div class="cat_box_link"><a href="{{url('Category')}}/{{$c->id}}">Click Here</a></div>
        </div>
      </li>
      @endforeach
    </ul>
  </div>
</section>
@stop
@section('javascript')
<script src="{{asset('public/Front/js/ProductDetail.js')}}"></script> 
@stop