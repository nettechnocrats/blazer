@extends('Front.Default')

@section('content')
<section class="breadcrumb">
  <div class="container">
    <ul>
      <li><a href="{{route('Home')}}">Home</a><i class="fa fa-angle-right" aria-hidden="true"></i></li>
      <li>Login</li>
    </ul>
  </div>
</section>

<section>
  <div class="register-section">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-xs-12 col-sm-6">
          <div class="register-left">
            <div class="register-head">
              <img src="{{asset('public/Front/img/register-logo.png')}}">
              <p>Paulo detraxit in eos, id mel facilisi scribentur. Ea alii delicata philosophia eos.</p>
            </div>
            <div class="register-list">
              <ul>
                <li>
                  <div class="row">
                    <div class="col-md-12-col-sm-10 col-xs-10">
                      <div class="register-text">
                        <h3>Lorem Ipsum</h3>
                        <p>Paulo detraxit in eos, id mel facilisi scribentur. Ea alii delicata philosophia eos.</p>
                      </div>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="row">
                    <div class="col-md-12-col-sm-10 col-xs-10">
                      <div class="register-text">
                        <h3>Lorem Ipsum</h3>
                        <p>Paulo detraxit in eos, id mel facilisi scribentur. Ea alii delicata philosophia eos.</p>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="register-right" style="margin-top: 70px;">
            <div class="register-form">
              <div class="form-head">
                <h3>Login here</h3>
              </div>
              <div class="register-tab">
                <div class="tab-pane">
                  <span id="Return_msg"></span>
                  <form role="form" id="LoginForm" name="LoginForm" action="javascript:void(0)" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                      <label for="email"></label>
                      <input type="email" class="form-control" id="email" name="email" placeholder="Email Address">
                      <span class="icon fa fa-envelope fa-lg"></span>
                    </div>
                    <div class="form-group">
                      <label for="sub"></label>
                      <input type="password" class="form-control" id="password" name="password" placeholder="Enter Password">
                      <span class="icon fa fa-key"></span>
                    </div>
                    <div class="checkbox">
                      <input id="login-remember" type="checkbox" name="remember" value="1">
                      <label>Remember me</label>
                    </div>
                    <p> <a href="{{route('Forgot')}}">Forget Password</a></p>
                    <div class="col-sm-12 text-center mt-5">
                      <input class="btn1 black" type="submit" name="LoginBtn" id="LoginBtn" value="Login">
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@stop

@section('javascript')
<script src="{{asset('public/Front/js/Login.js')}}"></script> 
@stop