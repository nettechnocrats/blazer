@extends('Front.Default')

@section('content')
<section class="banner">
  <ul class="bn_slider">
    <li style="background-image:url({{asset('public/Front')}}/img/bn1.jpg);">
      <div class="container">
        <div class="b_info">
          <div class="b_info_cont">
            <h2>Seasonal Cateogry 1</h2>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem...</p>
            <a class="btn1" href="#">Shop Now</a>
          </div>
        </div>
      </div>
    </li>
  </ul>
</section>
<section class="category_home">
  <div class="cont_wrap">
    <div class="cat_cont">
      <h2 class="heading1">{{$Category[1]->title}}</h2>
      <h3>Lorem Ipsum</h3>
      <p>{!! \Illuminate\Support\Str::words($Category[1]->description, 17,'....')  !!}</p>
      <a class="btn1 black" href="{{url('Category')}}/{{$Category[1]->id}}">Shop Now</a>
    </div>
  </div>
  <div class="img_wrap" style="background-image:url({{asset('public/Front/Category')}}/{{$Category[1]->image}}"></div>
</section>
<section class="category_home opposite">
  <div class="img_wrap" style="background-image:url({{asset('public/Front/Category')}}/{{$Category[2]->image}}"></div>
  <div class="cont_wrap">
    <div class="cat_cont">
      <h2 class="heading1 white">{{$Category[2]->title}}</h2>
      <h3>Lorem Ipsum</h3>
      <p>{!! \Illuminate\Support\Str::words($Category[2]->description, 17,'....')  !!}</p>
      <a class="btn1 white" href="{{url('Category')}}/{{$Category[2]->id}}">Shop Now</a>
    </div>
  </div>
</section>
<section class="comm_Sec fer_pro">
  <div class="container">
    <h2 class="heading1 text-center">Featured Products<span class="sub_h1">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</span></h2>
    <ul class="product_box">
      @foreach($ProductList as $p)
      <li>
        <a href="{{url('ProductDetail')}}/{{$p['pro_id']}}"></a>
        <div class="img_wrap"><img src="{{$p['pro_image']}}" alt=""/></div>
        <div class="cont_wrap">
          <h5>{{$p['pro_name']}}</h5>
          <div class="color_price">
            <div class="pro_color">
              @foreach($p['pro_color'] as $color)
                <span style="background:{{$color->value}};"></span>
              @endforeach 
              <p>{{count($p['pro_color'])}} Colors</p>
            </div>
            <div class="pro_price">${{$p['pro_price']}}</div>
          </div>
        </div>
      </li>
      @endforeach
    </ul>
  </div>
</section>
<section class="comm_Sec">
  <div class="container">
    <h2 class="heading1 text-center">Shopping Categories<span class="sub_h1">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</span></h2>
    <ul class="category_box">
      @foreach($Category as $c)
      <li style="background-image:url({{asset('public/Front/Category')}}/{{$c->image}}">
        <div class="cont_wrap">
          <h5>{{$c->title}}</h5>
          <div class="cat_box_link"><a href="{{url('Category')}}/{{$c->id}}">Click Here</a></div>
        </div>
      </li>
      @endforeach
    </ul>
  </div>
</section>
@stop