<?php $__env->startSection('css'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/Admin')); ?>/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/Admin')); ?>/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('BreadCrumb'); ?>
<div class="page-header card">
  <div class="row align-items-end">
    <div class="col-lg-8">
      <div class="page-header-title">
        <div class="d-inline">
          <h5>Edit Category</h5>
        </div>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="page-header-breadcrumb">
        <ul class=" breadcrumb breadcrumb-title">
          <li class="breadcrumb-item">
            <a href="<?php echo e(route('Dashboard')); ?>"><i class="feather icon-home"></i></a>
          </li>
          <li class="breadcrumb-item"><a href="<?php echo e(route('Category')); ?>">Category List</a>
          </li>
          <li class="breadcrumb-item"><a>Edit Category</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="col-sm-12">
  <div class="card">
    <div class="card-header">
      <h5>Edit category</h5>
    </div>
    <div class="card-block">
      <input type="hidden" id="url" value='<?php echo e(url("Admin/")); ?>'>
      <form id="updateform" action="<?php echo e(route('UpdateCategory')); ?>" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
        <div class="form-group row">
          <div class="col-sm-3">
            <label>Name</label>
            <input type="hidden" value="<?php echo e($Category->id); ?>" name="id">
            <input type="text" class="form-control" name="name" id="name" onkeyup="Slug();" value="<?php echo e($Category->title); ?>">
          </div>
          <div class="col-sm-3">
            <label>Slug</label>
            <input type="text" class="form-control" name="slug" id="slug" value="<?php echo e($Category->slug); ?>">
          </div>
          <div class="col-sm-1">
            <label>Sort</label>
            <input type="number" class="form-control" name="sort" value="<?php echo e($Category->sort); ?>">
          </div>
          <div class="col-sm-3">
            <label>Upload File</label>
            <input type="file" class="form-control" name="image">
          </div>
          <div class="col-sm-2">
            <label>Image</label>
            <img src="<?php echo e(asset('public\Front\Category')); ?>/<?php echo e($Category->image); ?>" width="80%" >
            <input type="hidden" value="<?php echo e($Category->image); ?>" name="old_image">
          </div>
          <div class="col-sm-6">
            <label>SEO Title</label>
            <textarea rows="5" cols="5" class="form-control" name="seo_title" placeholder="SEO Title"><?php echo e($Category->seo_title); ?></textarea>
          </div>
          <div class="col-sm-6">
            <label>SEO Description</label>
            <textarea rows="5" cols="5" class="form-control" name="seo_description" placeholder="SEO Description"><?php echo e($Category->seo_description); ?></textarea>
          </div>
          <div class="col-sm-12">
            <label>Description</label>
            <textarea rows="5" cols="5" class="form-control" id="description" name="description" placeholder="Category Description"><?php echo e($Category->description); ?></textarea>
          </div>
        </div>
        <input type="submit" class="btn btn-success" value="Update" id="UpdateBtn">
      </form>
    </div>
  </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>

<script src="<?php echo e(asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js')); ?>"></script>
<script>
  CKEDITOR.replace( 'description' );
</script>
<script src="<?php echo e(asset('public/Admin/validation/category.js')); ?>"></script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/nettech2010/public_html/Project/BlazerUSA/resources/views/Admin/Category/EditCategory.blade.php ENDPATH**/ ?>