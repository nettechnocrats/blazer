<header id="header" class="header"  style="background-color: #f5b32f;">
  <div class="header-menu">
    <div class="col-sm-7">
      <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
      <div class="header-left">
    </div>
  </div>
  <div class="col-sm-5">
    <div class="user-area dropdown float-right">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <?php
        $Image = Session::get('admin_image'); 
        if($Image!=""){
        ?>
          <img class="user-avatar rounded-circle" src="<?php echo e(asset('public/Admin/Profile').'/'.$Image); ?>" alt="User Avatar">
        <?php
        }else{
        ?>
        <img class="user-avatar rounded-circle" src="<?php echo e(asset('public/Admin')); ?>/images/admin.jpg" alt="User Avatar">
        <?php
        }
        ?>
      </a>
      <div class="user-menu dropdown-menu">
        <a class="nav-link" href="<?php echo e(route('AdminProfile')); ?>"><i class="fa fa- user"></i>My Profile</a>
        <a class="nav-link" href="<?php echo e(route('AdminSetting')); ?>"><i class="fa fa -cog"></i>Settings</a>
        <a class="nav-link" href="<?php echo e(route('AdminLogout')); ?>"><i class="fa fa-power -off"></i>Logout</a>
      </div>
    </div>
  </div>
</header><?php /**PATH C:\xampp\htdocs\Blazer_USA\resources\views/Admin/Layouts/Header.blade.php ENDPATH**/ ?>