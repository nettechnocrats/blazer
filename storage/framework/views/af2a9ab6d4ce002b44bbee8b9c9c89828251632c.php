<!DOCTYPE html>
<html lang="en">
  <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
  <head>
    <title><?php echo e($title); ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo e(asset('public/Front/favicon.png')); ?>">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Quicksand:500,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/Admin')); ?>/bower_components/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo e(asset('public/Admin')); ?>/assets/pages/waves/css/waves.min.css" type="text/css" media="all">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/Admin')); ?>/assets/icon/feather/css/feather.css">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/Admin')); ?>/assets/icon/themify-icons/themify-icons.css">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/Admin')); ?>/assets/icon/icofont/css/icofont.css">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/Admin')); ?>/assets/icon/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/Admin')); ?>/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/Admin')); ?>/assets/css/pages.css">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/Admin')); ?>/assets/css/local.css">
  </head>
  <body themebg-pattern="theme1">

    <input type="hidden" id="url" value='<?php echo e(url("Admin/")); ?>'>
    <input type="hidden" id="_token" value="<?php echo e(csrf_token()); ?>">
    <input type="hidden" id="referer_url" value="<?=@$_SERVER['HTTP_REFERER']?>">

    <section class="login-block">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-12">
            <form class="md-float-material" id="LoginForm" name="LoginForm">
              <input type="hidden" name="referer_url" value="<?=@$_SERVER['HTTP_REFERER']?>">
              <div class="text-center">
                <img src="<?php echo e(asset('public/Front/logo.png')); ?>" alt="logo.png">
              </div>
              <div class="auth-box card">
                <div class="card-block">
                  <div class="row m-b-20">
                    <div class="col-md-12">
                      <h3 class="text-center txt-primary">Admin Login</h3>
                    </div>
                  </div>
                  <span id="Login_msg">
                    <?php if(Session::has('ErrorMsg')): ?>
                      <?php echo Session::get('ErrorMsg'); ?>

                    <?php endif; ?>
                  </span>
                  <div class="form-group form-primary">
                    <label class="float-label">Username</label>
                    <input type="text" name="email" id="email" class="form-control" required="">
                  </div>
                  <div class="form-group form-primary">
                    <label class="float-label">Password</label>
                    <input type="password" name="password" id="password" class="form-control" required="">
                  </div>
                  <div class="row m-t-25 text-left">
                    <div class="col-12">
                      <div class="forgot-phone text-right float-right">
                        <a href="<?php echo e(route('AdminForgot')); ?>" class="text-right f-w-600"> Forgot Password?</a>
                      </div>
                    </div>
                  </div>
                  <div class="row m-t-30">
                    <div class="col-md-12">
                      <button type="submit" id="LoginBtn" class="btn btn-primary btn-block text-center m-b-20">LOGIN</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      </div>
    </section>
    <script src="<?php echo e(asset('public/Admin')); ?>/bower_components/jquery/js/jquery.min.js"></script>
    <script src="<?php echo e(asset('public/Admin')); ?>/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
    <script src="<?php echo e(asset('public/Admin')); ?>/bower_components/popper.js/js/popper.min.js"></script>
    <script src="<?php echo e(asset('public/Admin')); ?>/bower_components/bootstrap/js/bootstrap.min.js"></script>
    
    <script src="<?php echo e(asset('public/Admin')); ?>/assets/pages/waves/js/waves.min.js"></script>
    <script src="<?php echo e(asset('public/Admin')); ?>/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
    
    <script src="<?php echo e(asset('public/Admin')); ?>/bower_components/modernizr/js/modernizr.js"></script>
    <script src="<?php echo e(asset('public/Admin')); ?>/bower_components/modernizr/js/css-scrollbars.js"></script>
    <script src="<?php echo e(asset('public/Admin')); ?>/assets/js/common-pages.js"></script>
    <script src="<?php echo e(asset('public/Admin')); ?>/assets/js/rocket-loader.min.js"></script>

    <script src="<?php echo e(asset('public/Admin')); ?>/assets/js/jquery.validate.min.js"></script>
    <script src="<?php echo e(asset('public/Admin')); ?>/assets/js/additional-methods.min.js"></script> 

    <script src="<?php echo e(asset('public/Admin/validation/login.js')); ?>"></script>
  </body>
</html><?php /**PATH C:\xampp\htdocs\Blazer\resources\views/Admin/Login.blade.php ENDPATH**/ ?>