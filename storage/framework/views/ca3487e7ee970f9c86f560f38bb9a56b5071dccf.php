<!doctype html>

<html class="no-js" lang="">

    <?php echo $__env->make('Admin/Layouts/Head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <body>



        <?php echo $__env->make('Admin/Layouts/Menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        

        <div id="right-panel" class="right-panel">



            <?php echo $__env->make('Admin/Layouts/Header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>



            <div class="breadcrumbs">

                <div class="col-sm-4">

                    <div class="page-header float-left">

                        <div class="page-title">

                            <h1>Dashboard</h1>

                        </div>

                    </div>

                </div>

                <div class="col-sm-8">

                    <div class="page-header float-right">

                        <div class="page-title">

                            <ol class="breadcrumb text-right">

                                <li class="active">Dashboard</li>

                            </ol>

                        </div>

                    </div>

                </div>

            </div>



            <div class="content mt-3">

               

                <div class="col-xl-9">

                    <div class="card">

                        <div class="card-body">

                            <div class="row">

                                <div class="col-sm-4">

                                    <h4 class="card-title mb-0">Traffic</h4>

                                    <div class="small text-muted">October 2017</div>

                                </div>

                                <!--/.col-->

                                <div class="col-sm-8 hidden-sm-down">

                                    <!-- <button type="button" class="btn btn-warning float-right bg-flat-color-1">

                                        <i class="fa fa-cloud-download"></i>

                                    </button> -->

                                    <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">

                                        <div class="btn-group mr-3" data-toggle="buttons" aria-label="First group">

                                            <label class="btn btn-warning active" >

                                                <input type="radio" name="options" id="option1"> Day

                                            </label>

                                            <label class="btn btn-warning ">

                                                <input type="radio" name="options" id="option2" checked=""> Month

                                            </label>

                                            <label class="btn btn-warning">

                                                <input type="radio" name="options" id="option3"> Year

                                            </label>

                                        </div>

                                    </div>

                                </div><!--/.col-->





                            </div><!--/.row-->

                            <div class="chart-wrapper mt-4" >

                                <canvas id="trafficChart" style="height:200px;" height="200"></canvas>

                            </div>



                        </div>

                    </div>

                </div>



               





                





                <div class="col-xl-3 col-lg-6">

                    <div class="card">

                        <div class="card-body">

                            <div class="stat-widget-one">

                                <div class="stat-icon dib"><i class="fa fa-users text-warning border-warning"></i></div>

                                <div class="stat-content dib">

                                    <div class="stat-text">Users</div>

                                    <div class="stat-digit"><?php echo e($TotalUsers); ?></div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>



                <div class="col-xl-3 col-lg-6">

                    <div class="card">

                        <div class="card-body">

                            <div class="stat-widget-one">

                                <div class="stat-icon dib"><i class="ti-layout-grid2 text-warning border-warning"></i></div>

                                <div class="stat-content dib">

                                    <div class="stat-text">Total Products</div>

                                    <div class="stat-digit"><?php echo e($TotalProducts); ?></div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>



                <div class="col-xl-3 col-lg-6">

                    <div class="card">

                        <div class="card-body">

                            <div class="stat-widget-one">

                                <div class="stat-icon dib"><i class="ti-money text-warning border-warning"></i></div>

                                <div class="stat-content dib">

                                    <div class="stat-text">Total Orders</div>

                                    <div class="stat-digit">1,012</div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>



                



                <div class="col-xl-3 col-lg-6">

                    <div class="card">

                        <div class="card-body">

                            <div class="stat-widget-one">

                                <div class="stat-icon dib"><i class="ti-money text-warning border-warning"></i></div>

                                <div class="stat-content dib">

                                    <div class="stat-text">Today's Orders</div>

                                    <div class="stat-digit">120</div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>







            </div> <!-- .content -->

        </div>



        <?php echo $__env->make('Admin/Layouts/Footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> 
        
        <script src="<?php echo e(asset('public/Admin')); ?>/assets/js/lib/chart-js/Chart.bundle.js"></script>
        <script src="<?php echo e(asset('public/Admin')); ?>/assets/js/dashboard.js"></script>
        <script src="<?php echo e(asset('public/Admin')); ?>/assets/js/widgets.js"></script>
        <script src="<?php echo e(asset('public/Admin')); ?>/assets/js/lib/vector-map/jquery.vmap.js"></script>
        <script src="<?php echo e(asset('public/Admin')); ?>/assets/js/lib/vector-map/jquery.vmap.min.js"></script>
        <script src="<?php echo e(asset('public/Admin')); ?>/assets/js/lib/vector-map/jquery.vmap.sampledata.js"></script>
        <script src="<?php echo e(asset('public/Admin')); ?>/assets/js/lib/vector-map/country/jquery.vmap.world.js"></script>
        <script>
            ( function ( $ ) {
                "use strict";
                jQuery( '#vmap' ).vectorMap( {
                    map: 'world_en',
                    backgroundColor: null,
                    color: '#ffffff',
                    hoverOpacity: 0.7,
                    selectedColor: '#1de9b6',
                    enableZoom: true,
                    showTooltip: true,
                    values: sample_data,
                    scaleColors: [ '#1de9b6', '#03a9f5' ],
                    normalizeFunction: 'polynomial'
                } );
            } )( jQuery );
        </script>
    </body>

</html>

<?php /**PATH C:\xampp\htdocs\Blazer_USA\resources\views/Admin/Pages/Dashboard.blade.php ENDPATH**/ ?>