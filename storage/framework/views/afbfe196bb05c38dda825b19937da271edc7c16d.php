<?php $__env->startSection('content'); ?>
  <section class="banner">
    <ul class="banner_slider">
      <?php $__currentLoopData = $HomeBanner; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $banner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
        <li><img src="<?php echo e(asset('public/Front/Banner')); ?>/<?php echo e($banner->image); ?>" alt="<?php echo e($banner->image); ?>" /></li>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
  </section>
  <section class="blazer_category">
    <ul>
      <?php $__currentLoopData = $HomeCategory; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
        <li>
          <img src="<?php echo e(asset('public/Front/Category')); ?>/<?php echo e($cat->banner); ?>" alt="<?php echo e($cat->banner_text); ?>" />
          <a href="<?php echo e(route('CategoryWiseProduct',['CatSlug'=>$cat->slug])); ?>"><?php echo e($cat->category); ?></a>
        </li>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
    <div class="shop_our">SHOP OUR CLEARANCE SECTION AND SAVE BIG ON ALL BLAZERS. OVER 80% OFF!</div>
  </section>
  <section class="new_products">
    <div class="container">
      <div class="new_pro_h">
        <h2>New Products</h2>
        <p><span>BlazersUSA is committed to bringing you the largest selection of Men’s Blazers at the lowest prices. We are committed to adding new products on a daily basis. Shop today and save today.</span></p>
      </div>
      <div class="row new_pro_cont">
        <?php $__currentLoopData = $NewProduct; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $NewPro): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
          <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="cont_wrap">
              <a href="<?php echo e(route('ProductDetails',['ProSlug'=>$NewPro['slug']])); ?>"></a>
              <div class="img_wrap">
                <img src="<?php echo e(asset('public/Front/Product')); ?>/<?php echo e($NewPro['image']); ?>" alt="<?php echo e($NewPro['name']); ?>" />
              </div>
              <h4><?php echo e($NewPro['name']); ?></h4>
              <div class="blz_price">
                <?php if($NewPro['offer']==1): ?>
                <span class="was">was $<?php echo e($NewPro['compare_price']); ?></span>
                <?php endif; ?>
                <span class="now">Now $<?php echo e($NewPro['price']); ?></span>
              </div>
            </div>
          </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </div>
    </div>
  </section>

<section class="what_perfact">
  <div class="container">
    <div class="cont_wrap">
      <h3>What is the perfect fit Blazer?</h3>
      <p>BlazersUSA has a blazer for any occasion. The perfect fit blazer should be carfully selected based on your measurements. The best way to select the right blazer for your body is to...</p>
      <a class="btn cus_btn1" href="#">LEARN MORE</a>
      <span class="what_perfact_logo">
        <img src="<?php echo e(asset('public/Front')); ?>/img/what_perfact_logo.png" alt="" />
      </span>
    </div>
  </div>
</section>

<section class="seller">
  <div class="container">
    <div class="row">
      <div class="col-lg-4 col-md-6">
        <h4>Best Sellers</h4>
        <ul class="seller_list">
          <?php $__currentLoopData = $BestProduct; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $BPro): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <li>
              <div class="img_wrap">
                <img src="<?php echo e(asset('public/Front/Product')); ?>/<?php echo e($BPro['image']); ?>" alt="<?php echo e($BPro['name']); ?>" />
              </div>
              <div class="cont_wrap">
                <h5><a href="<?php echo e(route('ProductDetails',['ProSlug'=>$BPro['slug']])); ?>"> <?php echo e($BPro['name']); ?></a></h5>
                <span>Only $<?php echo e($BPro['price']); ?></span>
              </div>
            </li>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
      </div>
      <div class="col-lg-4 col-md-6">
        <h4>Featured Products</h4>
        <ul class="seller_list">
          <?php $__currentLoopData = $FeaturedProduct; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $FPro): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <li>
              <div class="img_wrap">
                <img src="<?php echo e(asset('public/Front/Product')); ?>/<?php echo e($FPro['image']); ?>" alt="<?php echo e($FPro['name']); ?>" />
              </div>
              <div class="cont_wrap">
                <h5><a href="<?php echo e(route('ProductDetails',['ProSlug'=>$FPro['slug']])); ?>"> <?php echo e($FPro['name']); ?></a></h5>
                <span>Only $<?php echo e($FPro['price']); ?></span>
              </div>
            </li>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
        </ul>
      </div>
      <div class="col-lg-4 col-md-6">
        <h4>Happy Customers</h4>
        <div class="happy_client">
          <div class="img_wrap">
            <img src="<?php echo e(asset('public/Front')); ?>/img/client.jpg" alt="" />
          </div>
          <p class="client_comm">“ I have been shopping from BlazerUSA for over 8 years now and they have the best customer service!”</p>
          <p class="client_thanks">Thank you BlazerUSA.com!</p>
          <p class="client_mail">AM@gmail.com</p>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="latest_blog">
  <div class="container">
    <h2>Latest from our blog</h2>
    <span>
      Do you want to present posts in the best way to highlight interesting moments of your blog? <br>Focus on the latest news!
    </span>
    <ul class="blog_slide">
      <li>
        <img src="<?php echo e(asset('public/Front')); ?>/img/blog1.jpg" alt="" />
        <div class="cont_wrap">
          <h4>Linen Blazers</h4>
          <p>Linen is the coolest blazer of all. Linen Blazers are perfect for the summer or Just for going out on the weekend ...</p>
          <a href="#">READ MORE</a>
        </div>
      </li>
      <li>
        <img src="<?php echo e(asset('public/Front')); ?>/img/blog1.jpg" alt="" />
        <div class="cont_wrap">
          <h4>Linen Blazers</h4>
          <p>Linen is the coolest blazer of all. Linen Blazers are perfect for the summer or Just for going out on the weekend ...</p>
          <a href="#">READ MORE</a>
        </div>
      </li>
      <li>
        <img src="<?php echo e(asset('public/Front')); ?>/img/blog1.jpg" alt="" />
        <div class="cont_wrap">
          <h4>Linen Blazers</h4>
          <p>Linen is the coolest blazer of all. Linen Blazers are perfect for the summer or Just for going out on the weekend ...</p>
          <a href="#">READ MORE</a>
        </div>
      </li>
      <li>
        <img src="<?php echo e(asset('public/Front')); ?>/img/blog1.jpg" alt="" />
        <div class="cont_wrap">
          <h4>Linen Blazers</h4>
          <p>Linen is the coolest blazer of all. Linen Blazers are perfect for the summer or Just for going out on the weekend ...</p>
          <a href="#">READ MORE</a>
        </div>
      </li>
      <li>
        <img src="<?php echo e(asset('public/Front')); ?>/img/blog1.jpg" alt="" />
        <div class="cont_wrap">
          <h4>Linen Blazers</h4>
          <p>Linen is the coolest blazer of all. Linen Blazers are perfect for the summer or Just for going out on the weekend ...</p>
          <a href="#">READ MORE</a>
        </div>
      </li>
      <li>
        <img src="<?php echo e(asset('public/Front')); ?>/img/blog1.jpg" alt="" />
        <div class="cont_wrap">
          <h4>Linen Blazers</h4>
          <p>Linen is the coolest blazer of all. Linen Blazers are perfect for the summer or Just for going out on the weekend ...</p>
          <a href="#">READ MORE</a>
        </div>
      </li>
    </ul>
  </div>
</section>

<section class="blazer_category follow_insta">
  <h4>Follow Us On Instagram #MensBlazers</h4>
  <ul>
    <li><img src="<?php echo e(asset('public/Front')); ?>/img/follow_insta1.jpg" alt="" /></li>
    <li><img src="<?php echo e(asset('public/Front')); ?>/img/follow_insta1.jpg" alt="" /></li>
    <li><img src="<?php echo e(asset('public/Front')); ?>/img/follow_insta1.jpg" alt="" /></li>
    <li><img src="<?php echo e(asset('public/Front')); ?>/img/follow_insta1.jpg" alt="" /></li>
    <li><img src="<?php echo e(asset('public/Front')); ?>/img/follow_insta1.jpg" alt="" /></li>
  </ul>
</section>

<section class="brand_logo">
  <div class="container">
    <ul>
      <li><img src="<?php echo e(asset('public/Front')); ?>/img/brand_logo1.jpg" alt="" /></li>
      <li><img src="<?php echo e(asset('public/Front')); ?>/img/brand_logo2.jpg" alt="" /></li>
      <li><img src="<?php echo e(asset('public/Front')); ?>/img/brand_logo3.jpg" alt="" /></li>
      <li><img src="<?php echo e(asset('public/Front')); ?>/img/brand_logo4.jpg" alt="" /></li>
      <li><img src="<?php echo e(asset('public/Front')); ?>/img/brand_logo5.jpg" alt="" /></li>
      <li><img src="<?php echo e(asset('public/Front')); ?>/img/brand_logo6.jpg" alt="" /></li>
    </ul>
  </div>
</section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script>
$('.banner_slider').slick({
  infinite: true,
  slidesToShow: 5,
  slidesToScroll: 1,
});

$('.blog_slide').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 3,
  arrows:false,
  dots:true,
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Front.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\Blazer\resources\views/Front/home.blade.php ENDPATH**/ ?>