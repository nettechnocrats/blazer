<?php $__env->startSection('content'); ?>

<section>
  <div class="inner-banner">
    <img src="<?php echo e(asset('public/Front/img/inner-banner.png')); ?>">
    <div class="container">
      <div class="row">
        <div class="banner-text">
          <h3>Blog</h3>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
        </div>
      </div>
    </div>
  </div>
</section>
<section>
  <input type="hidden" id="cat_id" value="<?php echo e($cat_id); ?>">  
  <div class="blog-section">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-sm-8 col-xs-12 blog-left" id="BlogList"></div>
        <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="blog-right">
            
          <div class="search-sec">
            <h3>Category</h3>
            <ul>
              <?php $__currentLoopData = $BlogCategory; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li>
                  <a href="<?php echo e(route('CategoryWiseBlog',['CatSlug'=>$cat['Slug']])); ?>">
                    <?php echo e($cat['Category']); ?><span> (<?php echo e($cat['TotalBlog']); ?>)</span>
                  </a>
                </li>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
          </div>
          <div class="search-sec post">
            <h3>Recent</h3>
            <ul>
              <?php $__currentLoopData = $RecentBlogList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <li>
                <a href="<?php echo e(route('BlogDetails',['CatSlug'=>$cat->CatSlug,'BlogUrl'=>$cat->slug])); ?>">
                  <span class="post-img">
                    <img src="<?php echo e(asset('public/Front/Blog/'.$cat->image)); ?>" width="120">
                  </span>
                  <p><?=(new App\Helpers\Common)->LimitTextWords(html_entity_decode($cat->description),7, true, true)?></p>Read more... 
                </a>
              </li>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script src="<?php echo e(asset('public/Front/js/BlogList.js')); ?>"></script> 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Front.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\Blazer_USA\resources\views/Front/Blog.blade.php ENDPATH**/ ?>