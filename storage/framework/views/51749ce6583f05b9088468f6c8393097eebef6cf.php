<?php $__env->startSection('content'); ?>

<section>
  <div class="inner-banner">
    <img src="<?php echo e(asset('public/Front/img/inner-banner.png')); ?>">
    <div class="container">
      <div class="row">
        <div class="banner-text">
          <h3>Product Listing</h3>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="new_products">
  <div class="container">
    <div class="row new_pro_cont">
      <div class="col-md-3 col-sm-3 col-xs-12 col-lg-3">
        <div class="panel-group" id="accordion">
          <div class="accordion">
            <?php $__currentLoopData = $CategoryDetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <h4 class="accordion-toggle"><?php echo e($cat['CatName']); ?></h4>
            <div class="accordion-content">
              <ul class="list-unstyled">
                <?php $__currentLoopData = $cat['SubCategoryList']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subcat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <li><a class="menu-item" href="<?php echo e(route('CategoryWiseProduct',['CatSlug'=>$subcat['CatSlug']])); ?>"><?php echo e($subcat['CatName']); ?></a></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </ul>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </div>
        </div>
      </div>
      <div class="col-md-9 col-sm-9 col-xs-12 col-lg-9">
        <div class="row" id="ProductList"></div>
      </div>
    </div>
  </div>
  <input type="hidden" id="CatSlug" value="<?=$CatSlug?>">
</section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script src="<?php echo e(asset('public/Front/js/ProductList.js')); ?>"></script> 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Front.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\Blazer_USA\resources\views/Front/ProductList.blade.php ENDPATH**/ ?>