<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title><?php echo e($title); ?></title>
  <link rel="shortcut icon" type="image/x-icon" href="<?php echo e(asset('public/Front/favicon.png')); ?>">
  
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo e(asset('public/Front/css/bootstrap.min.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(asset('public/Front/css/slick.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(asset('public/Front/css/custom.css')); ?>">
  <?php echo $__env->yieldContent('css'); ?>
</head>
<body>
<?php
use App\Helpers\Common;

$Common = new Common();
?>
<input type="hidden" id="url" value='<?php echo e(route("Home")); ?>'>
<input type="hidden" id="csrf_token" value='<?php echo e(csrf_token()); ?>'>
<section class="top_head">
  <div class="container">
    <div class="left_side">
      <span><i class="fa fa-phone" aria-hidden="true"></i> Call to Support: (+1)866-540-3229</span>
      <p>FREE Shipping on orders over $99</p>
    </div>
    <div class="right_side">
      <ul>
        <?php if(Session::get('UserLogin')==1): ?>
          <li><a href="<?php echo e(route('UserDashboard')); ?>">Dashboard</a></li>
          <li><a href="<?php echo e(route('Logout')); ?>">Logout</a></li>
        <?php else: ?>
          <li>
            <a href="<?php echo e(route('Login')); ?>"><i class="fa fa-user" aria-hidden="true"></i> Login</a>
            / <a href="<?php echo e(route('Login')); ?>">Register</a>
          </li>
        <?php endif; ?>
        <li><a href="#"><i class="fa fa-heart" aria-hidden="true"></i> Wishlist (0)</a></li>
      </ul>
    </div>
  </div>
</section>
<header class="header">
  <div class="container">
    <div class="logo">
      <a href="<?php echo e(route('Home')); ?>">
        <img src="<?php echo e(asset('public/Front/img/logo.png')); ?>" alt="Blazer USA"/>
      </a>
    </div>
    <div class="head_search">
      <select id="SearchCat" name="SearchCat">
        <option value="All">All categories</option>
        <?=$Common->HeaderCategoryList()?>
      </select>
      <div class="head_search_box">
        <a href="#"><img src="<?php echo e(asset('public/Front/img/search.png')); ?>" alt="Search"/></a>
        <input type="text" placeholder="Search for products..."/>
      </div>
    </div>
    <div class="cart">
      <a href="javascript:void(0)">
        <span><?=$Common->TotalCartCount()?></span>
        <img src="<?php echo e(asset('public/Front/img/cart.png')); ?>" alt=""/>
      </a>
      <div class="cart_pop" id="CartPopUp">
        <?=$Common->HeaderPopUpCart()?>
      </div>
    </div>
  </div>
</header>

<section class="navigation">
  <div class="container">
    <nav class="navbar navbar-expand-lg navbar-light">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
          <li class="nav-item active"><a class="nav-link" href="#">Suits</a></li>
          <li class="nav-item"><a class="nav-link" href="#">Tuxedos</a></li>
          <li class="nav-item"><a class="nav-link" href="#">Shoes</a></li>
          <li class="nav-item"><a class="nav-link" href="#">Shirts</a></li>
          <li class="nav-item"><a class="nav-link" href="#">Outerwear</a></li>
          <li class="nav-item"><a class="nav-link" href="#">Accessories</a></li>
          <li class="nav-item"><a class="nav-link blazer_nav" href="#">Blazers*</a></li>
          <li class="nav-item"><a class="nav-link" href="#">Slacks</a></li>
          <li class="nav-item"><a class="nav-link" href="#">Womens</a></li>
        </ul>
      </div>
    </nav>
  </div>
</section>

<?php echo $__env->yieldContent('content'); ?>

<section class="comm_Sec mission">
  <div class="container">
    <h2 class="heading1 text-center">Mission Statement</h2>
    <p class="mb-0">If you are not 100% satisfied with the quality, fit, or fabric of your suit you can return it for a full refund! The quality can be proven by your tailor! People may recognize Brioni, Armani, Zegna, Boss, and Canali... at MensUSA.com you get the same quality but you are paying only for the products, not "the name". We feel compelled and responsible to give you the best customer service. If you don't want to pay those silly prices at the malls, just let MensUSA.com take care of you and we promise you will become a regular customer! We strive to provide the best quality, value and service possible. We want you as a customer for life and we promise to never sell you a polyester suit as an Italian wool suit, like many other online suit stores! We guarantee your satisfaction. We have an unconditional return policy on all unworn, and unused new items purchased. Returns must come with their original packing and labels. Once the item is received, we will refund the full purchase price minus the 0% Restocking Fee. No shipping fees are refunded. You must make a request to return your item within 15 days after you received your item, otherwise we shall deem that you are satisfied with your purchase and the sale will become final. If somehow we mistakenly misrepresented the item we will reimburse the full amount paid plus return shipping cost.</p>
  </div>
</section>

<footer class="footer">
  <div class="container">
    <div class="social_footer">
      <a href="#"><img src="<?php echo e(asset('public/Front')); ?>/img/social_footer1.png" alt=""/></a>
      <a href="#"><img src="<?php echo e(asset('public/Front')); ?>/img/social_footer2.png" alt=""/></a>
      <a href="#"><img src="<?php echo e(asset('public/Front')); ?>/img/social_footer3.png" alt=""/></a>
      <a href="#"><img src="<?php echo e(asset('public/Front')); ?>/img/social_footer4.png" alt=""/></a>
    </div>
    <ul class="foot_nav">
      <li><a href="#">Contact Us</a><span>|</span></li>
      <li><a href="#">About Us</a><span>|</span></li>
      <li><a href="#">Help/FAQ</a><span>|</span></li>
      <li><a href="#">Measurment Info</a><span>|</span></li>
      <li><a href="#">100% Guarantee</a><span>|</span></li>
      <li><a href="#">Terms and Conditions</a><span>|</span></li>
      <li><a href="#">Online Privacy</a><span>|</span></li>
      <li><a href="#">Shipping Policy</a><span>|</span></li>
      <li><a href="#">Blog</a></li>
    </ul>
  </div>
</footer>

<script src="<?php echo e(asset('public/Front/js/jquery-3.4.1.min.js')); ?>"></script>
<script src="<?php echo e(asset('public/Front/js/custom.js')); ?>"></script>
<script src="<?php echo e(asset('public/Front/js/bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('public/Front/js/slick.min.js')); ?>"></script>

<script src="<?php echo e(asset('public/Front/js/jquery.validate.min.js')); ?>"></script>
<script src="<?php echo e(asset('public/Front/js/additional-methods.min.js')); ?>"></script>  

<?php echo $__env->yieldContent('javascript'); ?>

</body>
</html><?php /**PATH C:\xampp\htdocs\Blazer\resources\views/Front/default.blade.php ENDPATH**/ ?>