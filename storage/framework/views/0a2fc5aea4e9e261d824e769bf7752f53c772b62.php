<?php $__env->startSection('content'); ?>

<section>
  <div class="inner-banner">
    <img src="<?php echo e(asset('public/Front/img/inner-banner.png')); ?>">
    <div class="container">
      <div class="row">
        <div class="banner-text">
          <h3>Personal Information</h3>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="dashboard">
  <div class="container">
    <div class="dash_menu">
      <?php echo $__env->make('Front.User.LeftMenu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <div class="dash_cont">
      <h2>Personal Information</h2>
      <span id="Return_msg"></span>
      <form action="" method="post" name="PersonalInformationForm" id="PersonalInformationForm">
        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
        <div class="row">
          <div class="form-group col-md-6">
            <label>Name</label>
            <input type="text" class="form-control inputfiled" id="name" name="name" value="<?php echo e($User->name); ?>" placeholder="Name">
          </div>
          <div class="form-group col-md-6">
            <label>Email</label>
            <input type="email" class="form-control inputfiled" disabled="" placeholder="Email" value="<?php echo e($User->email); ?>">
          </div>
          <div class="form-group col-md-6">
            <label>Phone No</label>
            <input type="text" class="form-control inputfiled" id="mobile" name="mobile" placeholder="Phone No" value="<?php echo e($User->mobile); ?>">
          </div>
        </div>
        <input type="submit" class="btn cus_btn1 sm red" id="SubmitBtn" value="Save">
      </form>
    </div>
  </div>
</section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script src="<?php echo e(asset('public/Front/js/PersonalInformation.js')); ?>"></script> 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Front.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\Blazer_USA\resources\views/Front/User/PersonalInformation.blade.php ENDPATH**/ ?>