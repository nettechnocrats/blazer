<?php $__env->startSection('content'); ?>
<section class="breadcrumb">
  <div class="container">
    <ul>
      <li><a href="<?php echo e(route('Home')); ?>">Home</a><i class="fa fa-angle-right" aria-hidden="true"></i></li>
      <li>Forgot Password</li>
    </ul>
  </div>
</section>

<section>
  <div class="register-section">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-xs-12 col-sm-6">
          <div class="register-left">
            <div class="register-head">
              <img src="<?php echo e(asset('public/Front/img/register-logo.png')); ?>">
              <p>Paulo detraxit in eos, id mel facilisi scribentur. Ea alii delicata philosophia eos.</p>
            </div>
            <div class="register-list">
              <ul>
                <li>
                  <div class="row">
                    <div class="col-md-12-col-sm-10 col-xs-10">
                      <div class="register-text">
                        <h3>Lorem Ipsum</h3>
                        <p>Paulo detraxit in eos, id mel facilisi scribentur. Ea alii delicata philosophia eos.</p>
                      </div>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="row">
                    <div class="col-md-12-col-sm-10 col-xs-10">
                      <div class="register-text">
                        <h3>Lorem Ipsum</h3>
                        <p>Paulo detraxit in eos, id mel facilisi scribentur. Ea alii delicata philosophia eos.</p>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="register-right" style="margin-top: 70px;">
            <div class="register-form">
              <div class="form-head">
                <h3>Forgot Password</h3>
              </div>
              <div class="register-tab">
                <div class="tab-pane">
                  <span id="Return_msg"></span>
                  <form role="form" id="ForgotForm" name="ForgotForm" action="javascript:void(0)" method="post">
                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                    <div class="form-group">
                      <label for="email"></label>
                      <input type="email" class="form-control" id="email" name="email" placeholder="Email Address">
                      <span class="icon fa fa-envelope fa-lg"></span>
                    </div>
                    <p> <a href="<?php echo e(route('Login')); ?>">Login Again</a></p>
                    <div class="col-sm-12 text-center mt-5">
                      <input class="btn1 black" type="submit" name="SubmitBtn" id="SubmitBtn" value="Submit">
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script src="<?php echo e(asset('public/Front/js/Forgot.js')); ?>"></script> 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Front.Default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\Blazer\resources\views/Front/Forgot.blade.php ENDPATH**/ ?>