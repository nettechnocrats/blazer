<?php $__env->startSection('content'); ?>
<section class="breadcrumb">
  <div class="container">
    <ul>
      <li><a href="<?php echo e(route('Home')); ?>">Home</a><i class="fa fa-angle-right" aria-hidden="true"></i></li>
      <li>Login</li>
    </ul>
    <a href="https://api.whatsapp.com/send?phone=9871659388&text=I'm%20interested%20in%20your%20car%20for%20sale">Test</a>
  </div>
</section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script type="text/javascript">
  public void onClickWhatsApp(View view) {

    PackageManager pm=getPackageManager();
    try {

        Intent waIntent = new Intent(Intent.ACTION_SEND);
        waIntent.setType("text/plain");
        String text = "YOUR TEXT HERE";

        PackageInfo info=pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
        //Check if package exists or not. If not then code 
        //in catch block will be called
        waIntent.setPackage("com.whatsapp");

        waIntent.putExtra(Intent.EXTRA_TEXT, text);
        startActivity(Intent.createChooser(waIntent, "Share with"));

   } catch (NameNotFoundException e) {
        Toast.makeText(this, "WhatsApp not Installed", Toast.LENGTH_SHORT).show();
   }  

}    

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Front.Default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/nettech2010/public_html/Project/BlazerUSA/resources/views/Front/Login.blade.php ENDPATH**/ ?>