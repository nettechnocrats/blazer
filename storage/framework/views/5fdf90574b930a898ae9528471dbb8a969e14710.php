<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
  <title><?php echo e($title); ?></title>
  
  <link rel="shortcut icon" type="image/x-icon" href="<?php echo e(asset('public/Front/favicon.png')); ?>">
  
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Quicksand:500,700" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/Admin')); ?>/bower_components/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo e(asset('public/Admin')); ?>/assets/pages/waves/css/waves.min.css" type="text/css" media="all">
  <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/Admin')); ?>/assets/icon/feather/css/feather.css">
  <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/Admin')); ?>/assets/css/font-awesome-n.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/Admin')); ?>/assets/css/style.css">
  <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/Admin')); ?>/assets/css/widget.css">
  <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/Admin')); ?>/assets/css/local.css">
  <?php echo $__env->yieldContent('css'); ?>
</head>

<body>
  <input type="hidden" id="url" value='<?php echo e(route("AdminLogin")); ?>'>
  <input type="hidden" id="csrf_token" value='<?php echo e(csrf_token()); ?>'>
  <div class="loader-bg">
    <div class="loader-bar"></div>
  </div>

  <div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">
      <nav class="navbar header-navbar pcoded-header">
        <div class="navbar-wrapper">
          <div class="navbar-logo">
            <a href="<?php echo e(route('Dashboard')); ?>">
              <img class="img-fluid" src="<?php echo e(asset('public/Front/logo.png')); ?>" alt="Logo" />
            </a>
            <a class="mobile-menu" id="mobile-collapse" href="#!">
              <i class="feather icon-menu icon-toggle-right"></i>
            </a>
            <a class="mobile-options waves-effect waves-light">
              <i class="feather icon-more-horizontal"></i>
            </a>
          </div>
          <div class="navbar-container container-fluid">
            <ul class="nav-right">
              <li class="user-profile header-notification">
                <div class="dropdown-primary dropdown">
                  <div class="dropdown-toggle" data-toggle="dropdown">
                    <img src="<?php echo e(Session::get('admin_image')); ?>" class="img-radius" alt="User-Profile-Image">
                    <span><?php echo e(Session::get('admin_name')); ?></span>
                    <i class="feather icon-chevron-down"></i>
                  </div>
                  <ul class="show-notification profile-notification dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                    <li>
                      <a href="#!">
                        <i class="feather icon-settings"></i> Settings
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i class="feather icon-user"></i> Profile
                      </a>
                    </li>
                    <li>
                      <a href="email-inbox.html">
                        <i class="feather icon-mail"></i> My Messages
                      </a>
                    </li>
                    <li>
                      <a href="auth-lock-screen.html">
                        <i class="feather icon-lock"></i> Lock Screen
                      </a>
                    </li>
                    <li>
                      <a href="<?php echo e(route('AdminLogout')); ?>">
                        <i class="feather icon-log-out"></i> Logout
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
          <nav class="pcoded-navbar">
            <div class="nav-list">
              <div class="pcoded-inner-navbar main-menu">
                <ul class="pcoded-item pcoded-left-item">
                  <li class=" <?php if($Menu=='Dashboard'): ?> active <?php endif; ?>">
                    <a href="<?php echo e(route('Dashboard')); ?>" class="waves-effect waves-dark">
                      <span class="pcoded-micon">
                        <i class="feather icon-home"></i>
                      </span>
                      <span class="pcoded-mtext">Dashboard</span>
                    </a>
                  </li>
                  <li class=" <?php if($Menu=='Banner'): ?> active <?php endif; ?>">
                    <a href="<?php echo e(route('Banner')); ?>" class="waves-effect waves-dark">
                      <span class="pcoded-micon">
                        <i class="feather icon-sliders"></i>
                      </span>
                      <span class="pcoded-mtext">Banner</span>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
          <div class="pcoded-content">
            <div class="pcoded-inner-content">
              <div class="main-body">
                <div class="page-wrapper">
                  <div class="page-body">
                    <div class="row">
                      
                      <?php echo $__env->yieldContent('content'); ?>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div id="styleSelector"></div>
        </div>
      </div>
    </div>
  </div>
  
  
  <script src="<?php echo e(asset('public/Admin')); ?>/bower_components/jquery/js/jquery.min.js"></script>
  <script src="<?php echo e(asset('public/Admin')); ?>/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
  <script src="<?php echo e(asset('public/Admin')); ?>/bower_components/popper.js/js/popper.min.js"></script>
  <script src="<?php echo e(asset('public/Admin')); ?>/bower_components/bootstrap/js/bootstrap.min.js"></script>
  <script src="<?php echo e(asset('public/Admin')); ?>/assets/pages/waves/js/waves.min.js" ></script>
  <script src="<?php echo e(asset('public/Admin')); ?>/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
  
  <script src="<?php echo e(asset('public/Admin')); ?>/assets/js/markerclusterer.js" ></script>
  <script src="<?php echo e(asset('public/Admin')); ?>/assets/js/pcoded.min.js" ></script>
  <script src="<?php echo e(asset('public/Admin')); ?>/assets/js/vertical/vertical-layout.min.js" ></script>
  <script src="<?php echo e(asset('public/Admin')); ?>/assets/js/script.min.js"></script>

  <script src="<?php echo e(asset('public/Admin')); ?>/assets/js/jquery.validate.min.js"></script>
    <script src="<?php echo e(asset('public/Admin')); ?>/assets/js/additional-methods.min.js"></script>  

  <?php echo $__env->yieldContent('javascript'); ?>

</body>
</html><?php /**PATH C:\xampp\htdocs\Blazer\resources\views/Admin/Default.blade.php ENDPATH**/ ?>