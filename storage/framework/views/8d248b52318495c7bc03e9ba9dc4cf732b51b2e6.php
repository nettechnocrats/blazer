<?php $__env->startSection('content'); ?>

<section>
  <div class="inner-banner">
    <img src="<?php echo e(asset('public/Front/img/inner-banner.png')); ?>">
    <div class="container">
      <div class="row">
        <div class="banner-text">
          <h3>Dashboard</h3>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="dashboard">
  <div class="container">
    <div class="dash_menu">
      <?php echo $__env->make('Front.User.LeftMenu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <div class="dash_cont">
      <h2>Personal Information</h2>
      <form>
        <div class="row">
          <div class="form-group col-md-6">
            <label>First Name</label>
            <input type="email" class="form-control inputfiled" placeholder="First Name">
          </div>
          <div class="form-group col-md-6">
            <label>Last Name</label>
            <input type="email" class="form-control inputfiled" placeholder="Last Name">
          </div>
          <div class="form-group col-md-6">
            <label>Email</label>
            <input type="email" class="form-control inputfiled" placeholder="Email">
          </div>
          <div class="form-group col-md-6">
            <label>Phone No</label>
            <input type="email" class="form-control inputfiled" placeholder="Phone No">
          </div>
        </div>
        <a class="btn cus_btn1 sm red" href="#">Save</a>
      </form>
    </div>
  </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Front.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\Blazer_USA\resources\views/Front/User/Dashboard.blade.php ENDPATH**/ ?>