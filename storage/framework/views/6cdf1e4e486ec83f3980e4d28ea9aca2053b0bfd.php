<?php $__env->startSection('content'); ?>
<section class="banner">
  <ul class="bn_slider">
    <li style="background-image:url(<?php echo e(asset('public/Front')); ?>/img/bn1.jpg);">
      <div class="container">
        <div class="b_info">
          <div class="b_info_cont">
            <h2>Seasonal Cateogry 1</h2>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem...</p>
            <a class="btn1" href="#">Shop Now</a>
          </div>
        </div>
      </div>
    </li>
  </ul>
</section>
<section class="category_home">
  <div class="cont_wrap">
    <div class="cat_cont">
      <h2 class="heading1"><?php echo e($Category[1]->title); ?></h2>
      <h3>Lorem Ipsum</h3>
      <p><?php echo \Illuminate\Support\Str::words($Category[1]->description, 17,'....'); ?></p>
      <a class="btn1 black" href="<?php echo e(url('Category')); ?>/<?php echo e($Category[1]->id); ?>">Shop Now</a>
    </div>
  </div>
  <div class="img_wrap" style="background-image:url(<?php echo e(asset('public/Front/Category')); ?>/<?php echo e($Category[1]->image); ?>"></div>
</section>
<section class="category_home opposite">
  <div class="img_wrap" style="background-image:url(<?php echo e(asset('public/Front/Category')); ?>/<?php echo e($Category[2]->image); ?>"></div>
  <div class="cont_wrap">
    <div class="cat_cont">
      <h2 class="heading1 white"><?php echo e($Category[2]->title); ?></h2>
      <h3>Lorem Ipsum</h3>
      <p><?php echo \Illuminate\Support\Str::words($Category[2]->description, 17,'....'); ?></p>
      <a class="btn1 white" href="<?php echo e(url('Category')); ?>/<?php echo e($Category[2]->id); ?>">Shop Now</a>
    </div>
  </div>
</section>
<section class="comm_Sec fer_pro">
  <div class="container">
    <h2 class="heading1 text-center">Featured Products<span class="sub_h1">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</span></h2>
    <ul class="product_box">
      <?php $__currentLoopData = $ProductList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <li>
        <a href="<?php echo e(url('ProductDetail')); ?>/<?php echo e($p['pro_id']); ?>"></a>
        <div class="img_wrap"><img src="<?php echo e($p['pro_image']); ?>" alt=""/></div>
        <div class="cont_wrap">
          <h5><?php echo e($p['pro_name']); ?></h5>
          <div class="color_price">
            <div class="pro_color">
              <?php $__currentLoopData = $p['pro_color']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $color): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <span style="background:<?php echo e($color->value); ?>;"></span>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
              <p><?php echo e(count($p['pro_color'])); ?> Colors</p>
            </div>
            <div class="pro_price">$<?php echo e($p['pro_price']); ?></div>
          </div>
        </div>
      </li>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
  </div>
</section>
<section class="comm_Sec">
  <div class="container">
    <h2 class="heading1 text-center">Shopping Categories<span class="sub_h1">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</span></h2>
    <ul class="category_box">
      <?php $__currentLoopData = $Category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <li style="background-image:url(<?php echo e(asset('public/Front/Category')); ?>/<?php echo e($c->image); ?>">
        <div class="cont_wrap">
          <h5><?php echo e($c->title); ?></h5>
          <div class="cat_box_link"><a href="<?php echo e(url('Category')); ?>/<?php echo e($c->id); ?>">Click Here</a></div>
        </div>
      </li>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
  </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Front.Default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\Blazer\resources\views/Front/Home.blade.php ENDPATH**/ ?>