<?php $__env->startSection('content'); ?>

<div class="col-xl-3 col-md-6">
  <div class="card prod-p-card card-red">
    <div class="card-body">
      <div class="row align-items-center m-b-30">
        <div class="col">
          <h6 class="m-b-5 text-white">Total Profit</h6>
          <h3 class="m-b-0 f-w-700 text-white">$1,783</h3>
        </div>
        <div class="col-auto">
          <i class="fas fa-money-bill-alt text-c-red f-18"></i>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-xl-3 col-md-6">
  <div class="card prod-p-card card-blue">
    <div class="card-body">
      <div class="row align-items-center m-b-30">
        <div class="col">
          <h6 class="m-b-5 text-white">Total Orders</h6>
          <h3 class="m-b-0 f-w-700 text-white">15,830</h3>
        </div>
        <div class="col-auto">
          <i class="fas fa-database text-c-blue f-18"></i>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-xl-3 col-md-6">
  <div class="card prod-p-card card-green">
    <div class="card-body">
      <div class="row align-items-center m-b-30">
        <div class="col">
          <h6 class="m-b-5 text-white">Average Price</h6>
          <h3 class="m-b-0 f-w-700 text-white">$6,780</h3>
        </div>
        <div class="col-auto">
          <i class="fas fa-dollar-sign text-c-green f-18"></i>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-xl-3 col-md-6">
  <div class="card prod-p-card card-yellow">
    <div class="card-body">
      <div class="row align-items-center m-b-30">
        <div class="col">
          <h6 class="m-b-5 text-white">Product Sold</h6>
          <h3 class="m-b-0 f-w-700 text-white">6,784</h3>
        </div>
        <div class="col-auto">
          <i class="fas fa-tags text-c-yellow f-18"></i>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/nettech2010/public_html/Project/BlazerUSA/resources/views/Admin/Dashboard.blade.php ENDPATH**/ ?>