<?php $__env->startSection('content'); ?>

<section>
  <div class="inner-banner">
    <img src="<?php echo e(asset('public/Front/img/inner-banner.png')); ?>">
    <div class="container">
      <div class="row">
        <div class="banner-text">
          <h3>Blog</h3>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
        </div>
      </div>
    </div>
  </div>
</section>
<section>
  <div class="blog-section">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-sm-8 col-xs-12 blog-left">
          <div class="blog-img">
            <img src="<?php echo e(asset('public/Front/Blog/'.$Blog->image)); ?>">
          </div>
          <div class="blog-text">
            <ul>
              <li><i class="fa fa-user" aria-hidden="true"></i><?php echo e($Blog->author); ?> </li>
              <li><i class="fa fa-calendar" aria-hidden="true"></i><?php echo e(date('d M Y',strtotime($Blog->publish_date))); ?></li>
              <li><i class="fa fa-comments" aria-hidden="true"></i>Comments</li>
            </ul>
            <h3><?php echo e($Blog->title); ?></h3>
            <?=html_entity_decode($Blog->description)?>
          </div>
          <div class="blog-form">
            <h3>Recent Post</h3>
            <form action="javascript:void(0)" method="post" id="BlogComment" name="BlogComment">
              <input type="text" id="c_name" name="c_name" placeholder="Your Name">
              <input type="text" id="c_email" name="c_email" placeholder="Your Email">
              <textarea rows="5" id="c_comment" name="c_comment" placeholder="Type Comment"></textarea>
              <input type="submit" id="BlogUsBtn" value="Submit" class="sch-btn">
            </form>
          </div>


        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="blog-right">
          <div class="search-sec">
            <h3>Category</h3>
            <ul>
              <?php $__currentLoopData = $BlogCategory; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li>
                  <a href="<?php echo e(route('CategoryWiseBlog',['CatSlug'=>$cat['Slug']])); ?>">
                    <?php echo e($cat['Category']); ?><span> (<?php echo e($cat['TotalBlog']); ?>)</span>
                  </a>
                </li>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
          </div>
          <div class="search-sec post">
            <h3>Category</h3>
            <ul>
              <?php $__currentLoopData = $RecentBlogList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <li>
                <a href="<?php echo e(route('BlogDetails',['CatSlug'=>$cat->CatSlug,'BlogUrl'=>$cat->slug])); ?>">
                  <span class="post-img">
                    <img src="<?php echo e(asset('public/Front/Blog/'.$cat->image)); ?>" width="120">
                  </span>
                  <p><?=(new App\Helpers\Common)->limitTextWords(html_entity_decode($cat->description),7, true, true)?></p>Read more... 
                </a>
              </li>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script src="<?php echo e(asset('public/Front/js/BlogDetails.js')); ?>"></script> 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Front.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\Blazer_USA\resources\views/Front/BlogDetails.blade.php ENDPATH**/ ?>