<!doctype html>

<html class="no-js" lang="">

<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Sun-Style | Admin</title>

    <meta name="description" content="Sufee Admin - HTML5 Admin Template">

    <meta name="viewport" content="width=device-width, initial-scale=1">



    <link rel="apple-touch-icon" href="apple-icon.png">

    <link rel="shortcut icon" href="favicon.ico">



    <link rel="stylesheet" href="<?php echo e(asset('public/Admin')); ?>/assets/css/normalize.css">

    <link rel="stylesheet" href="<?php echo e(asset('public/Admin')); ?>/assets/css/bootstrap.min.css">

    <link rel="stylesheet" href="<?php echo e(asset('public/Admin')); ?>/assets/css/font-awesome.min.css">

    <link rel="stylesheet" href="<?php echo e(asset('public/Admin')); ?>/assets/css/themify-icons.css">

    <link rel="stylesheet" href="<?php echo e(asset('public/Admin')); ?>/assets/css/flag-icon.min.css">

    <link rel="stylesheet" href="<?php echo e(asset('public/Admin')); ?>/assets/css/cs-skin-elastic.css">

    <link rel="stylesheet" href="<?php echo e(asset('public/Admin')); ?>/assets/scss/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>



</head>

    <body class="" style="background-color:#f7b32f;">





        <div class="sufee-login d-flex align-content-center flex-wrap">

            <div class="container">

                <div class="login-content">

                    <div class="login-logo">

                        <a href="index.html">

                            <img class="align-content" src="<?php echo e(asset('public/SiteLogo')); ?>/logo.png" alt="">

                        </a>

                    </div>
                    
                    <div class="col-md-12">

                        <?php if(Session::has('message')): ?>

                            <div class="alert <?php echo e(Session::get('alert-class', 'alert-info')); ?>">

                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

                                <strong><?php echo e(Session::get('message')); ?></strong>

                            </div>

                        <?php endif; ?>

                    </div>

                    <div class="login-form">

                        <form action="<?php echo e(route('AdminLoginValidate')); ?>" name="LoginForm" id="LoginForm" method="POST">



                            <input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>"> 

                            <div class="form-group">

                                <label>Email address</label>

                                <input type="email" id="email" name="email" onkeypress="HideErr('emailErr');" 

                                        class="form-control" placeholder="Enter Email.">

                                <span id="emailErr"></span>

                            </div>

                            <div class="form-group">

                                <label>Password</label>

                                <input type="password" id="pass" name="pass" onkeypress="HideErr('passErr');" 

                                        class="form-control" placeholder="Enter Password.">

                                <span id="passErr"></span>

                            </div>

                            <button type="button" id="LoginBtn" 

                                    class="btn btn-success btn-flat m-b-30 m-t-30" 

                                    style="background-color:#f7b32f;">

                                Sign in

                            </button>

                            

                        </form>

                    </div>

                </div>

            </div>

        </div>



        <input type="hidden" name="url" id="url" value='<?php echo e(URL::to("/")); ?>'>

        <input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>"> 

        <script src="<?php echo e(asset('public/Admin')); ?>/assets/js/vendor/jquery-2.1.4.min.js"></script>

        <script src="<?php echo e(asset('public/Admin')); ?>/assets/js/popper.min.js"></script>

        <script src="<?php echo e(asset('public/Admin')); ?>/assets/js/plugins.js"></script>

        <script src="<?php echo e(asset('public/Admin')); ?>/assets/js/main.js"></script>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <script src="<?php echo e(asset('public/Admin')); ?>/custom-js/login.js"></script>





    </body>

</html>

<?php /**PATH C:\xampp\htdocs\Blazer_USA\resources\views/Admin/Pages/Login.blade.php ENDPATH**/ ?>