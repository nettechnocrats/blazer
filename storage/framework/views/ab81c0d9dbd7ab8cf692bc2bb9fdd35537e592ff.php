<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Blazer USA</title>
  <link rel="stylesheet" href="<?php echo e(asset('public/Front/css/fontawesome.min.css')); ?>"/>
  <link rel="stylesheet" href="<?php echo e(asset('public/Front/css/bootstrap.min.css')); ?>"/>
  <link rel="stylesheet" href="<?php echo e(asset('public/Front/css/slick.css')); ?>"/>
  <link rel="stylesheet" href="<?php echo e(asset('public/Front/css/custom.css')); ?>"/>
  <?php echo $__env->yieldContent('css'); ?>
</head>
<body>

  <input type="hidden" id="url" value='<?php echo e(URL::to("/")); ?>'>
  <input type="hidden" id="csrf_token" value='<?php echo e(csrf_token()); ?>'>

  <section class="top_line">
    <div class="left_side">
      <ul>
        <li>(+1)866-540-3229<span>|</span></li>
        <li>FREE Shipping on orders over $150</li>
      </ul>
    </div>
    <div class="right_side">
      <nav class="navbar navbar-expand">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item"><a class="nav-link" href="<?php echo e(route('Dashboard')); ?>">My Account</a></li>
            <!-- <li class="nav-item"><a class="nav-link" href="<?php echo e(route('Wishlist')); ?>">My Wish List</a></li> -->
            <?php if(Session::get('UserLogin')==1): ?>
              <li class="nav-item"><a class="nav-link" href="<?php echo e(route('Dashboard')); ?>">Dashboard</a></li>
            <?php else: ?>  
              <li class="nav-item"><a class="nav-link" href="<?php echo e(route('SignUp')); ?>">Sign Up</a></li>
              <li class="nav-item"><a class="nav-link" href="<?php echo e(route('Login')); ?>">Sign In</a></li>
            <?php endif; ?>
          </ul>
        </div>
      </nav>
    </div>
  </section>
  <header class="header">
    <div class="logo">
      <a href="<?php echo e(url('/')); ?>"><img src="<?php echo e(asset('public/Front')); ?>/img/logo.png" alt="" /></a>
    </div>
    <nav class="navbar navbar-expand-lg custom_nav">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent2" aria-controls="navbarSupportedContent2" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent2">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item <?php if(@$Menu=='Home'): ?> active <?php endif; ?> "><a class="nav-link" href="<?php echo e(route('HomePage')); ?>">Home</a></li>
          <?=(new App\Helpers\Common)->HeaderMenu()?>
          <li class="nav-item <?php if(@$Menu=='Blog'): ?> active <?php endif; ?> "><a class="nav-link" href="<?php echo e(route('Blog')); ?>">Blog</a></li>
          <li class="nav-item <?php if(@$Menu=='ContactUs'): ?> active <?php endif; ?> "><a class="nav-link" href="<?php echo e(route('ContactUs')); ?>">Contact Us</a></li>
        </ul>
      </div>
    </nav>
    <div class="head_right">
      <div class="head_search">
          <a href="javascript:void(0)">
            <img src="<?php echo e(asset('public/Front')); ?>/img/search.png" alt="" />
          </a>
          <input type="text" />
      </div>
      <div class="head_cart">
        <a href="<?php echo e(route('Cart')); ?>">
          <img src="<?php echo e(asset('public/Front')); ?>/img/cart.png" alt="" />
          <span><?=(new App\Helpers\Common)->TotalCartCount()?></span>
        </a>
      </div>
    </div>
  </header>

  <?php echo $__env->yieldContent('content'); ?>

  <section class="newsletter">
    <div class="container">
      <h3>Join Our News Letter</h3>
      <p>Be the first to hear about our special offers and promotions!</p>
      <form method="post" name="OurNewsLetterForm" id="OurNewsLetterForm">
        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
        <div class="join_input">
          <input type="text" name="news_email" id="news_email" placeholder="Type your email here" />
          <button type="submit" class="send">
            <img src="<?php echo e(asset('public/Front')); ?>/img/send.png" alt="Send" />
          </button>
        </div>
      </form>  
    </div>
  </section>
  <footer class="footer">
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-md-6 left_side">
          <div class="foot_logo">
            <a href="<?php echo e(url('/')); ?>">
              <img src="<?php echo e(asset('public/Front')); ?>/img/logo_footer.png" />
            </a>
          </div>
          <p class="foot_info">We are a team of designers and developers that create high quality Magento, Prestashop, Opencart themes and provide premium and dedicated support to our products.</p>
          <ul>
            <li>205 Arapahoe St, Schoenchen, KS 69696</li>
            <li>Email: <a href="mailto:Support@Plazathemes.com">Support@Plazathemes.com</a></li>
            <li>Phone: <a href="tel:+1123-456-6789">+1123-456-6789</a></li>
          </ul>
        </div>
        <div class="col-lg-4 col-md-6 mid_side">
          <div class="row">
            <div class="col-md-6">
              <h5>Useful Links</h5>
              <ul>
                <li><a href="<?php echo e(route('PrivacyPolicy')); ?>">Privacy Policy</a></li>
                <li><a href="#">Order History</a></li>
                <li><a href="<?php echo e(route('ContactUs')); ?>">Contact Us</a></li>
                <li><a href="<?php echo e(route('Returns')); ?>">Returns</a></li>
                <li><a href="<?php echo e(route('ReturnPolicy')); ?>">Return Policy</a></li>
                <li><a href="<?php echo e(route('GiftVouchers')); ?>">Gift Vouchers </a></li>
              </ul>
            </div>
            <div class="col-md-6">
              <h5>Find us on</h5>
              <ul>
                <?=(new App\Helpers\Common)->SocialMediaLink()?>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 right_side">
          <h5>Product Tags</h5>
          <ul class="pro_tag">
            <?=(new App\Helpers\Common)->ProductTag()?>
          </ul>
        </div>
      </div>
    </div>
  </footer>
  <section class="copyright">
    <div class="container">
      <p>Copyright &copy; 2019 BlazerUSA. All right reserved.</p>
      <img src="<?php echo e(asset('public/Front')); ?>/img/pay_opt.png" alt="" />
    </div>
  </section>

  <script src="<?php echo e(asset('public/Front/js/jquery-3.4.1.min.js')); ?>"></script>
  <script src="<?php echo e(asset('public/Front/js/bootstrap.min.js')); ?>"></script>
  <script src="<?php echo e(asset('public/Front/js/slick.min.js')); ?>"></script>

  <script src="<?php echo e(asset('public/Front/js/jquery.validate.min.js')); ?>"></script>
  <script src="<?php echo e(asset('public/Front/js/additional-methods.min.js')); ?>"></script> 

  <script src="<?php echo e(asset('public/Front/js/custom.js')); ?>"></script>

  <?php echo $__env->yieldContent('javascript'); ?>

</body>
</html><?php /**PATH C:\xampp\htdocs\Blazer_USA\resources\views/Front/default.blade.php ENDPATH**/ ?>