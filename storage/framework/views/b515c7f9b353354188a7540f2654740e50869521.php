<?php $__env->startSection('content'); ?>
<?php
use Anam\Phpcart\Cart;
$this->cart = new Cart();
?>
<section class="breadcrumb">
  <div class="container">
    <ul>
      <li><a href="<?php echo e(route('Home')); ?>">Home</a><i class="fa fa-angle-right" aria-hidden="true"></i></li>
      <li>Cart</li>
    </ul>
  </div>
</section>
<section class="comm_Sec cart_page">
  <div class="container">
    <div class="head">
      <div class="left_side"><h2 class="heading1">DON'T FORGET!</h2></div>
    </div>
    <div class="dont_forgot">
      <div class="left_side">
        <ul class="product_box mt-0">
          <li>
            <div class="img_wrap"><img src="<?php echo e(asset('public/Front')); ?>/img/product1.jpg" alt=""></div>
            <div class="cont_wrap">
              <h5>Product Name</h5>
              <a class="btn1 sm" href="#">Shop Now</a>
            </div>
          </li>
          <li>
            <div class="img_wrap"><img src="<?php echo e(asset('public/Front')); ?>/img/product2.jpg" alt=""></div>
            <div class="cont_wrap">
              <h5>Product Name</h5>
              <a class="btn1 sm" href="#">Shop Now</a>
            </div>
          </li>
          <li>
            <div class="img_wrap"><img src="<?php echo e(asset('public/Front')); ?>/img/product3.jpg" alt=""></div>
            <div class="cont_wrap">
              <h5>Product Name</h5>
              <a class="btn1 sm" href="#">Shop Now</a>
            </div>
          </li>
        </ul>
        <div class="est_mnt_fee">
          <div class="est_fee_box">
            <div class="cont_box">
              <ul>
                <li>
                  <img src="<?php echo e(asset('public/Front')); ?>/img/satisfaction1.png" alt=""/>
                  <span>100% Satisfaction Guarantee</span>
                </li>
                <li>
                  <img src="<?php echo e(asset('public/Front')); ?>/img/satisfaction2.png" alt=""/>
                  <span>Shipping and Return Policy</span>
                </li>
                <li>
                  <img src="<?php echo e(asset('public/Front')); ?>/img/satisfaction3.png" alt=""/>
                  <span>Privacy & Security</span>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="right_side">
        <div class="cart_detail">
          <?php if($TotalCart > 0): ?>
          <ul class="cart_box">
            <?php
            $SubTotal = 0;
            ?>
            <?php $__currentLoopData = $CartList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cart): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php
              $SubTotal+=$cart->quantity*$cart->price;
            ?>
            <li>
              <div class="cont_wrap">
                <h4><a href="<?php echo e($cart->pro_url); ?>"><?php echo e($cart->name); ?></a><span class="pr">$<?php echo e($cart->price); ?><a class="del" href="javascript:void(0)" onclick="RemoveCart(<?=$cart->pro_id?>);"><img src="<?php echo e(asset('public/Front')); ?>/img/del.png" alt=""/></a></span></h4>
                <ul>
                  <?php $__currentLoopData = $cart->attributes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $attributes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <li><?php echo e($attributes['type']); ?>: <?php echo e($attributes['ValueName']); ?></li>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  <li>QTY: <?php echo e($cart->quantity); ?></li>
                </ul>
              </div>
              <div class="img_wrap"><img src="<?php echo e($cart->pro_img); ?>" alt=""/></div>
            </li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </ul>
          <?php endif; ?>
          <div class="go_checkout">
            <p>Items in cart : <strong><?php echo e($this->cart->count()); ?></strong></p>
            <p>Subtotal : <strong>$<?=$SubTotal?></strong></p>
            <p class="free_ship">YOU QUALIFY FOR FREE SHIPPING!</p>
            <p>Shipping : <strong class="cl_red">FREE!</strong></p>
            <p>Tax : <strong>Calculated at Checkout</strong></p>
            <p>Estimated Total: <strong>$<?=$SubTotal?></strong></p>
            <a class="add_promo" href="javascript:void(0)">Add Promotional Code</a>
            <div class="btn_check"><a class="btn1 red" href="<?php echo e(route('Checkout')); ?>">CHECKOUT</a></div>
            <div class="conti_shop"><a href="<?php echo e(route('Home')); ?>">Continue Shopping</a></div>
          </div>
        </div>
        <div class="cart_btns">
          <a class="btn1" href="<?php echo e(route('Home')); ?>">Continue Shopping</a>
          <a class="btn1 red" href="<?php echo e(route('Checkout')); ?>">CHECKOUT</a>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
<script src="<?php echo e(asset('public/Front/js/Cart.js')); ?>"></script> 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Front.Default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\Blazer\resources\views/Front/Cart.blade.php ENDPATH**/ ?>