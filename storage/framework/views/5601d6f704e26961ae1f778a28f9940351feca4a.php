<!doctype html>
<html class="no-js" lang="">
    <?php echo $__env->make('Admin/Layouts/Head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <body>
        <?php echo $__env->make('Admin/Layouts/Menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>      

        <div id="right-panel" class="right-panel">
            <?php echo $__env->make('Admin/Layouts/Header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <div class="breadcrumbs">
	            <div class="col-sm-4">
	                <div class="page-header float-left">
	                    <div class="page-title">
	                        <h1>Product-Edit</h1>
	                    </div>
	                </div>
	            </div>
	            <div class="col-sm-8">
	                <div class="page-header float-right">
	                    <div class="page-title">
	                        <ol class="breadcrumb text-right">
	                            <li><a href="<?php echo e(route('AdminDashboard')); ?>">Dashboard</a></li>
	                            <li><a href="<?php echo e(route('ProductList')); ?>">Product-List</a></li>
	                            <li class="active">Product-Edit</li>
	                        </ol>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div class="col-md-12">
		        <?php if(Session::has('message')): ?>           
		            <div class="alert <?php echo e(Session::get('alert-class', 'alert-info')); ?>">
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <strong><?php echo e(Session::get('message')); ?></strong>
		            </div>
		        <?php endif; ?>
		    </div>
			<div class="content mt-3">
				<div class="animated fadeIn">	
					<form action="<?php echo e(route('ProductEditDetails')); ?>" name="EditForm" id="EditForm" method="POST" enctype="multipart/form-data">
						<input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">
						<input type="hidden" name="PID" id="PID" value="<?php echo e($PID); ?>">
						<div class="row">
							<div class="col-lg-4">
								<div class="card">
									<div class="card-header">
										<i class="fa fa-plus"></i> <strong>Product General Info</strong>
									</div>
									<div class="card-body card-block">
										<input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>"> 
										<div class="form-group">
											<strong for="country" class=" form-control-label">Name</strong>
											<input onkeypress="HideErr('nameErr'),HideErr('slugErr');"  type="text" id="name" name="name" 
													placeholder="Name" class="form-control" onkeyup="Slug();" value="<?php echo e($ProductDetail->name); ?>">
				                            <span id="nameErr"></span>
										</div>

										<div class="form-group">
											<strong for="country" class=" form-control-label">Slug</strong>
											<input onkeypress="HideErr('slugErr');"  type="text" id="slug" name="slug" 
													placeholder="Slug" class="form-control" value="<?php echo e($ProductDetail->slug); ?>">
				                            <span id="slugErr"></span>
										</div>

										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<strong for="country" class=" form-control-label">Status</strong>
													<select onchange="HideErr('statusErr');" name="status" id="status"
															 class="form-control">
						                                <option value="1" <?php if($ProductDetail->status==1){ echo "selected"; }?>>Active</option>
						                                <option value="0" <?php if($ProductDetail->status==0){ echo "selected"; }?>>De-active</option>
						                            </select>
						                            <span id="statusErr"></span>
												</div>
											</div>
											<div class="col-md-6">										
												<div class="form-group">
													<strong for="country" class=" form-control-label">Sort</strong>
													<select onchange="HideErr('sortErr');" name="sort" id="sort" class="form-control">
						                                <?php for($i=1;$i<=$Sort;$i++): ?>
						                                <option value="<?php echo e($i); ?>"
						                                	<?php if($ProductDetail->sort==$i){ echo "selected"; }?>><?php echo e($i); ?></option>
						                                <?php endfor; ?>
						                            </select>
						                            <span id="sortErr"></span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="card">
									<div class="card-header">
										<i class="fa fa-plus"></i> <strong>Product Price Info</strong>
									</div>
									<div class="card-body card-block">										
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<strong for="country" class=" form-control-label">Price</strong>
													<input onkeypress="HideErr('priceErr');"  type="text" id="price" name="price" 
															placeholder="Price" class="form-control prc" value="<?php echo e($ProductDetail->price); ?>">
						                            <span id="priceErr"></span>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<strong for="country" class=" form-control-label">Compare Price</strong>
													<input onkeypress="HideErr('c_priceErr');"  type="text" id="c_price" name="c_price" 
															placeholder="Compare Price" class="form-control prc" value="<?php echo e($ProductDetail->compare_price); ?>">
						                            <span id="c_priceErr"></span>
												</div>
											</div>
										</div>
										<?php 
											$Warranty='';
											$WarrantyIn='';
											if($ProductDetail->warranty!='')
											{ 
												$Warranty=$ProductDetail->warranty;
											}
											if($ProductDetail->warranty_in!='')
											{ 
												$WarrantyIn=$ProductDetail->warranty_in;
											}
										?> 
										<strong for="country" class=" form-control-label">Warranty</strong>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<select onchange="HideErr('warrantyErr');" name="warranty" id="warranty" 
													class="form-control">
														<option value="">Select</option>
						                                <?php for($w=1;$w<=12;$w++): ?>
						                                <option value="<?php echo e($w); ?>"
						                                	<?php if($Warranty==$w){ echo "selected"; }?>><?php echo e($w); ?></option>
						                                <?php endfor; ?>
						                            </select>
						                            <span id="warrantyErr"></span>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<select onchange="HideErr('warranty_inErr');" name="warranty_in" id="warranty_in" 
													class="form-control">
														<option value=""  <?php if($WarrantyIn==''){ echo "selected"; }?>>Select</option>					                                
						                                <option value="M" <?php if($WarrantyIn=='M'){ echo "selected"; }?>>Month(s)</option>
						                                <option value="Y" <?php if($WarrantyIn=='Y'){ echo "selected"; }?>>Year(s)</option>
						                            </select>
						                            <span id="warranty_inErr"></span>
												</div>
											</div>
										</div>
										<?php 
											$offer='';
											$offer_p='';
											if($ProductDetail->offer!='')
											{ 
												$offer=$ProductDetail->offer;
											}
											if($ProductDetail->offer_p!='')
											{ 
												$offer_p=$ProductDetail->offer_p;
											}
										?> 
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<strong for="country" class=" form-control-label">Any Offer?</strong>
													<select onchange="HideErr('offerErr');" name="offer" id="offer" 
														class="form-control">					                                
						                                <option value="0" <?php if($offer=='0'){ echo "selected"; }?>>No</option>
						                                <option value="1" <?php if($offer=='1'){ echo "selected"; }?>>Yes</option>
						                            </select>
						                            <span id="offerErr"></span>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<strong for="country" class=" form-control-label">Offer %</strong>
													<input onkeypress="HideErr('offer_pErr');"  type="text" id="offer_p" name="offer_p" 
															placeholder="Offer %" class="form-control prc" value='<?php echo e($offer_p); ?>'>
						                            <span id="offer_pErr"></span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="card">
									<div class="card-header">
										<i class="fa fa-plus"></i> <strong>Product Collection Info</strong>
									</div>
									<div class="card-body card-block">
										<input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>"> 
										<div class="form-group">
											<strong for="country" class=" form-control-label">Collection</strong>
											<select onchange="HideErr('collectionErr');" name="collection" id="collection" 
													class="form-control">
				                                <option value="">Select Collection</option>
				                                <?php $__currentLoopData = $Collection; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				                                <option value="<?php echo e($c->id); ?>" 
				                                	<?php if($ProductDetail->collection==$c->id){ echo "selected"; }?>><?php echo e($c->name); ?></option>
				                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				                            </select>
				                            <span id="collectionErr"></span>
										</div>
	 
										<div class="form-group"> 
											<strong for="country" class=" form-control-label">Category</strong>
											<select onchange="HideErr('categoryErr');" name="category" id="category" 
													class="form-control">
				                                <option value="">Select Category</option>
				                                <?php $__currentLoopData = $Category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				                                <option value="<?php echo e($c->id); ?>"
				                                	<?php if($ProductDetail->category==$c->id){ echo "selected"; }?>><?php echo e($c->category); ?></option>
				                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				                            </select>
				                            <span id="categoryErr"></span>
										</div>

										<div class="form-group">
											<strong for="country" class=" form-control-label">Sub-Category</strong>
											<select onchange="HideErr('sub_categoryErr');" name="sub_category" id="sub_category" 
													class="form-control">
				                                <option value="">Select Sub-Category</option>
				                               	<?php $__currentLoopData = $SubCategory; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				                                <option value="<?php echo e($c->id); ?>"
				                                	<?php if($ProductDetail->sub_category==$c->id){ echo "selected"; }?>><?php echo e($c->category); ?></option>
				                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				                            </select>
				                            <span id="sub_categoryErr"></span>
										</div>
										
									</div>
								</div>
							</div>
							
						</div>
						<div class="row">
							<div class="col-lg-6">
								<div class="card">
									<div class="card-header">
										<i class="fa fa-plus"></i> <strong>Product Variant</strong>
									</div>
									<div class="card-body card-block">
										<?php $__currentLoopData = $Variant; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<label class="container1"><strong><?php echo e($v->variant); ?></strong>
											<input type="checkbox" name="variant[]" value="<?php echo e($v->id); ?>" 
												<?php if(in_array($v->id, $ProductVariant)){ echo "checked"; }?>>
											<span class="checkmark"></span>									
											<?php $__currentLoopData = $VariantAttributes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $va): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<?php if($v->id==$va->variant_id): ?>
												<label class="container1 cont"><?php echo e($va->value); ?>

													<input type="checkbox" name="attribute[]" value="<?php echo e($va->id); ?>"
														<?php if(in_array($va->id, $ProductAttribute)){ echo "checked"; }?>>
													<span class="checkmark"></span>
												</label>
												<?php endif; ?>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>									
										</label>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									</div>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="card">
									<div class="card-header">
										<i class="fa fa-plus"></i> <strong>Product Images</strong>
									</div>
									<div class="card-body card-block">
										<h1>Images</h1>
										<?php $__currentLoopData = $ProductImages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$i): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<div class="row" id="<?php echo e($key); ?>">
												<div class="col-md-2">
												</div>
												<div class="col-md-4">
													<div class="form-group">
														<center><img src="<?php echo e(asset('public/Front/Product').'/'.$i->image); ?>" height="50" width="50"></center>
													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group">
														<a href="javascript:void(0);" onclick="RemoveImage('<?php echo $PID; ?>','<?php echo $i->id; ?>','<?php echo $key; ?>');" class="btn btn-danger">Remove</a>
													</div>
												</div>
												<div class="col-md-2">
												</div>
											</div>
											<?php if($key==0): ?>
												<span id="imageErr"></span>
											<?php endif; ?>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										<div id="ImagesDiv">
											<div class="row">
												<div class="col-md-4">
													<div class="form-group">
														<input type="file" onchange="HideErr('imageErr');" name="image[]" id="image" 
																class="form-control-sm form-control upld" alt="ImagePrev" >
							                            <span id="imageErr"></span>
													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group">
														<center><img src="<?php echo e(asset('public/Front/Product/DefaultImageIcon.png')); ?>" height="50" width="50" id="ImagePrev"></center>
													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group">
														<a href="javascript:void(0);" onclick="AddMoreSection();" class="btn btn-success">Add More</a>
													</div>
												</div>
											</div>
											<span id="imageErr"></span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
	                            <div class="card">
	                                <div class="card-header">
	                                    <h4>Product Other Info.</h4>
	                                </div>
	                                <div class="card-body">
                                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" id="home-tab" 
                                                data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><strong>Product Description</strong></a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="profile-tab" 
                                                data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false"><strong>Size Details</strong></a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="contact-tab" 
                                                data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false"><strong>Technical Information</strong></a>
                                            </li>
                                        </ul>
                                        <div class="tab-content pl-3 p-1" id="myTabContent">
                                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                                <div class="form-group">
													<textarea onchange="HideErr('descriptionErr');" name="description" id="product_description" 
															class="form-control-sm form-control" rows="5"><?php echo e($ProductDetail->description); ?></textarea>
						                            <span id="descriptionErr"></span>
												</div>
                                            </div>
                                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                                <div class="form-group">
													<textarea onchange="HideErr('size_detailsErr');" name="size_details" id="size_details" 
															class="form-control-sm form-control" rows="5"><?php echo e($ProductDetail->size_details); ?></textarea>
						                            <span id="size_detailsErr"></span>
												</div>
                                            </div>
                                            <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                                <div class="form-group">
													<textarea onchange="HideErr('technical_infoErr');" name="technical_info" id="technical_info" 
															class="form-control-sm form-control" rows="5"><?php echo e($ProductDetail->technical_info); ?></textarea>
						                            <span id="technical_infoErr"></span>
												</div>
                                            </div>
                                        </div>
	                                </div>
	                            </div>
	                        </div>
                        </div>
						<div class="card">
							<div class="card-footer">
								<button type="button" class="btn btn-success" id="EditBtn">
									<i class="fa fa-save"></i> Save
								</button>
							</div>
						</div>
					</form>
				</div><!-- .animated -->
			</div><!-- .content -->
        </div>
        <?php echo $__env->make('Admin/Layouts/Footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <input type="hidden" name="url" id="url" value='<?php echo e(URL::to("/")); ?>'>
	    <input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">
	    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
	    <script src="<?php echo e(asset('public/Admin/custom-js/Product.js')); ?>" type="text/javascript"></script>
	    <script src="//cdn.ckeditor.com/4.9.2/full-all/ckeditor.js"></script>
		<script>
	   		CKEDITOR.replace( 'product_description' );
	   		CKEDITOR.replace( 'size_details' );
	   		CKEDITOR.replace( 'technical_info' );
	    </script>
    </body>
</html><?php /**PATH C:\xampp\htdocs\Blazer_USA\resources\views/Admin/Pages/Product/ProductEdit.blade.php ENDPATH**/ ?>