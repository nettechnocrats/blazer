<!doctype html>
<html class="no-js" lang="">
    <?php echo $__env->make('Admin/Layouts/Head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <body>
        <?php echo $__env->make('Admin/Layouts/Menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>      

        <div id="right-panel" class="right-panel">
            <?php echo $__env->make('Admin/Layouts/Header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <div class="breadcrumbs">
	            <div class="col-sm-4">
	                <div class="page-header float-left">
	                    <div class="page-title">
	                        <h1>Variants-Edit</h1>
	                    </div>
	                </div>
	            </div>
	            <div class="col-sm-8">
	                <div class="page-header float-right">
	                    <div class="page-title">
	                        <ol class="breadcrumb text-right">
	                            <li><a href="<?php echo e(route('AdminDashboard')); ?>">Dashboard</a></li>
	                            <li><a href="<?php echo e(route('VariantList')); ?>">Variant-List</a></li>
	                            <li class="active">Variants-Edit</li>
	                        </ol>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div class="col-md-12">
		        <?php if(Session::has('message')): ?>           
		            <div class="alert <?php echo e(Session::get('alert-class', 'alert-info')); ?>">
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <strong><?php echo e(Session::get('message')); ?></strong>
		            </div>
		        <?php endif; ?>
		    </div>
			<div class="content mt-3">
				<div class="animated fadeIn">
					<div class="col-lg-8">
						<div class="card">
							<div class="card-header">
								<i class="fa fa-plus"></i> <strong>Edit Variants Details</strong>
							</div>
							<div class="card-body card-block">
								<form action="<?php echo e(route('VariantEditDetails')); ?>" method="post" enctype="multipart/form-data" 
									class="form-horizontal" id='SaveForm'>
									<input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">
									<input type="hidden" name="VID" id="VID" value="<?php echo e($VariantDetail->id); ?>">
									<div class="col-lg-6">
										<div class="form-group">
											<strong for="country" class=" form-control-label">Variant Name</strong>
											<input value="<?php echo e($VariantDetail->variant); ?>" onkeypress="HideErr('variantErr');"  type="text" id="variant" name="variant" placeholder="Variant" class="form-control"  onkeyup="Slug();">
											<span id="variantErr"></span>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<strong for="country" class=" form-control-label">Status</strong>
													<select onchange="HideErr('statusErr');" name="status" id="status"
															 class="form-control-sm form-control">
						                               	<option value="1" <?php  if($VariantDetail->status==1) { echo "Selected"; }?>>Active</option>
					                                	<option value="0" <?php  if($VariantDetail->status==0) { echo "Selected"; }?>>De-active</option>
						                            </select>
						                            <span id="statusErr"></span>
												</div>
											</div>
											<div class="col-md-6">										
												<div class="form-group">
													<strong for="country" class=" form-control-label">Sort</strong>
													<select onchange="HideErr('sortErr');" name="sort" id="sort" class="form-control-sm form-control">
						                                <?php for($i=1;$i<=$Sort;$i++): ?>
						                                <option value="<?php echo e($i); ?>"
						                                <?php 
					                                		if($VariantDetail->sort==$i) 
					                                		{
					                                			echo "Selected";
					                                		}
					                                	?>
						                                ><?php echo e($i); ?></option>
						                                <?php endfor; ?>
						                            </select>
						                            <span id="sortErr"></span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<strong for="country" class="form-control-label">Variants Attributes</strong>
											<div  id="VariantsAttributes">
												<?php $__currentLoopData = $VariantAttributeDetail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<div class="row" id='<?php echo e($key); ?>'>
													<div class="col-lg-8">
														<input value="<?php echo e($val->value); ?>" type="text" class="form-control" name="attributes[]" id="attribute" onkeypress="HideErr('attributeErr');">
													</div>
													<?php if($key==0): ?>
													<div class="col-lg-4">
														<button type="button" name="AddMore" class="btn btn-success" onclick="AddMoreSection();">
															<i class="fa fa-plus"></i> Add More
														</button>
													</div>													
													<?php else: ?>
													<div class="col-lg-4">
															<button name="AddMore" class="btn btn-danger" onclick="DeleteSection('<?php echo e($key); ?>');">
																<i class="fa fa-remove"></i> Remove
															</button>
														</div>
													<?php endif; ?>
												</div>
												<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												<span id="attributeErr"></span>
											</div>
										</div>
									</div>	
								</form>
							</div>
							<div class="card-footer">
								<button type="button" class="btn btn-success" id="SaveBtn">
									<i class="fa fa-save"></i> Save
								</button>
							</div>
						</div>
					</div>					
				</div><!-- .animated -->
			</div><!-- .content -->
        </div>
        <?php echo $__env->make('Admin/Layouts/Footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <input type="hidden" name="url" id="url" value='<?php echo e(URL::to("/")); ?>'>
	    <input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">
	    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
	    <script src="<?php echo e(asset('public/Admin/custom-js/Variants.js')); ?>" type="text/javascript"></script>
    </body>
</html><?php /**PATH C:\xampp\htdocs\Blazer_USA\resources\views/Admin/Pages/Variants/VariantsEdit.blade.php ENDPATH**/ ?>