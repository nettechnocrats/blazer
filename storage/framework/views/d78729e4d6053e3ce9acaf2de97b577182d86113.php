<aside id="left-panel" class="left-panel">
  <nav class="navbar navbar-expand-sm navbar-default">
    <div class="navbar-header">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
        <i class="fa fa-bars"></i>
      </button>
      <a class="navbar-brand" href="<?php echo route('AdminDashboard') ?>"><img src="<?php echo e(asset('public/Front/img/register-logo.png')); ?>" alt="Logo"></a>
      <a class="navbar-brand hidden" href="<?php echo route('AdminDashboard') ?>"><img src="<?php echo e(asset('public/Front/img/register-logo.png')); ?>" alt="Logo"></a>
    </div>
    <div id="main-menu" class="main-menu collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li class="active">
          <a href="<?php echo e(route('AdminDashboard')); ?>"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
        </li>
        <h3 class="menu-title">Product</h3>
        <li class="menu-item-has-children dropdown">
          <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
            <i class="menu-icon fa fa-laptop"></i>Category
          </a>
          <ul class="sub-menu children dropdown-menu">
            <li><i class="fa fa-table"></i><a href="<?php echo e(route('CategoryList')); ?>">List</a></li>
            <li><i class="fa fa-table"></i><a href="<?php echo e(route('CategoryAdd')); ?>">Add New</a></li>
          </ul>
        </li>
        <li class="menu-item-has-children dropdown">
          <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
            <i class="menu-icon fa fa-laptop"></i>Variants
          </a>
          <ul class="sub-menu children dropdown-menu">
            <li><i class="fa fa-table"></i><a href="<?php echo e(route('VariantList')); ?>">List</a></li>
            <li><i class="fa fa-table"></i><a href="<?php echo e(route('VariantAdd')); ?>">Add New</a></li>
          </ul>
        </li>
        <li class="menu-item-has-children dropdown">
          <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="menu-icon fa fa-table"></i>Product
          </a>
          <ul class="sub-menu children dropdown-menu">
            <li><i class="fa fa-table"></i><a href="<?php echo e(route('ProductList')); ?>">List</a></li>
            <li><i class="fa fa-table"></i><a href="<?php echo e(route('ProductAdd')); ?>">Add New</a></li>
          </ul>
        </li>
        <li class="menu-item-has-children dropdown">
          <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="menu-icon fa fa-map-marker"></i>Delivery Location
          </a>
          <ul class="sub-menu children dropdown-menu">
            <li><i class="menu-icon fa fa-th"></i><a href="<?php echo e(route('DeliveryLocationList')); ?>">List</a></li>
            <li><i class="menu-icon fa fa-th"></i><a href="<?php echo e(route('DeliveryLocationAdd')); ?>">Add New</a></li>
          </ul>
        </li>
        <li class="menu-item-has-children dropdown">
          <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="menu-icon fa fa-dollar"></i>Tax master
          </a>
          <ul class="sub-menu children dropdown-menu">
            <li><i class="menu-icon fa fa-th"></i><a href="<?php echo e(route('TaxList')); ?>">List</a></li>
            <li><i class="menu-icon fa fa-th"></i><a href="<?php echo e(route('TaxAdd')); ?>">Add New</a></li>
          </ul>
        </li>
        <li class="menu-item-has-children dropdown">
          <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="menu-icon fa fa-dollar"></i>Coupon
          </a>
          <ul class="sub-menu children dropdown-menu">
            <li><i class="menu-icon fa fa-th"></i><a href="<?php echo e(route('CouponList')); ?>">List</a></li>
            <li><i class="menu-icon fa fa-th"></i><a href="<?php echo e(route('CouponAdd')); ?>">Add New</a></li>
          </ul>
        </li>
        <li class="">
          <a href="<?php echo e(route('ContentList')); ?>"> <i class="menu-icon fa fa-file"></i>Content Management </a>
        </li>
        <li class="">
          <a href="<?php echo e(route('BannerList')); ?>"> <i class="menu-icon fa fa-image"></i>Banner Management </a>
        </li>
        <h3 class="menu-title">Management</h3>
        <li class="menu-item-has-children dropdown">
          <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
            <i class="menu-icon fa fa-laptop"></i>Orders
          </a>
          <ul class="sub-menu children dropdown-menu">
            <li><i class="fa fa-table"></i><a href="<?php echo e(route('OrderList')); ?>">List</a></li>
          </ul>
        </li>
        <li class="menu-item-has-children dropdown">
          <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="menu-icon fa fa-user"></i>Users</a>
          <ul class="sub-menu children dropdown-menu">
            <li><i class="fa fa-table"></i><a href="<?php echo e(route('UsersList')); ?>">List</a></li>
          </ul>
        </li>
        <li class="">
          <a href="<?php echo e(route('Report')); ?>"> <i class="menu-icon fa fa-file"></i>Report</a>
        </li>               
        <h3 class="menu-title">Others</h3>
        <li class="menu-item-has-children dropdown">
          <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="menu-icon fa fa-laptop"></i>Blog</a>
          <ul class="sub-menu children dropdown-menu">
            <li><i class="menu-icon fa fa-th"></i><a href="<?php echo e(route('BlogCategory')); ?>">Category</a></li>
            <li><i class="menu-icon fa fa-th"></i><a href="<?php echo e(route('BlogPost')); ?>">Blog</a></li>
          </ul>
        </li>
        <li class="menu-item-has-children dropdown">
          <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="menu-icon fa fa-envelope"></i>Send Mail</a>
          <ul class="sub-menu children dropdown-menu">
            <li><i class="menu-icon fa fa-th"></i><a href="<?php echo e(route('EmailHistory')); ?>">History</a></li>
            <li><i class="menu-icon fa fa-th"></i><a href="<?php echo e(route('SendNew')); ?>">Send New Mail</a></li>
          </ul>
        </li>
        <li class="active">
          <a href="<?php echo e(route('SocialMedia')); ?>"> <i class="menu-icon fa fa-dashboard"></i>Social Media Link</a>
        </li>
        <li class="active">
          <a href="<?php echo e(route('SubscriberList')); ?>"> <i class="menu-icon fa fa-dashboard"></i>Subscribers</a>
        </li>
      </ul>
    </div>
  </nav>
</aside><?php /**PATH C:\xampp\htdocs\Blazer_USA\resources\views/Admin/Layouts/Menu.blade.php ENDPATH**/ ?>