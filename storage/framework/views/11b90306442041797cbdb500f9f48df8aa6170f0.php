<?php $__env->startSection('content'); ?>

<section>
  <div class="inner-banner">
    <img src="<?php echo e(asset('public/Front/img/inner-banner.png')); ?>">
    <div class="container">
      <div class="row">
        <div class="banner-text">
          <h3>Signin</h3>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
        </div>
      </div>
    </div>
  </div>
</section>
<section>
  <div class="register-section">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-xs-12 col-sm-6">
          <div class="register-left">
            <div class="register-head">
              <img src="<?php echo e(asset('public/Front/img/register-logo.png')); ?>">
              <p>Paulo detraxit in eos, id mel facilisi scribentur. Ea alii delicata philosophia eos.</p>
            </div>
            <div class="register-list">
              <ul>
                <li>
                  <div class="row">
                    <div class="col-md-12-col-sm-10 col-xs-10">
                      <div class="register-text">
                        <h3>Lorem Ipsum</h3>
                        <p>Paulo detraxit in eos, id mel facilisi scribentur. Ea alii delicata philosophia eos.</p>
                      </div>
                    </div>
                  </div>
                </li>
              <li>
              <div class="row">
                <div class="col-md-12-col-sm-10 col-xs-10">
                  <div class="register-text">
                    <h3>Lorem Ipsum</h3>
                    <p>Paulo detraxit in eos, id mel facilisi scribentur. Ea alii delicata philosophia eos.</p>
                  </div>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
      <div class="register-right" style="margin-top: 70px;">
        <div class="register-form">
          <div class="form-head">
            <h3>Login here</h3>
          </div>
          <div class="register-tab">
            <div class="tab-pane">
              <span id="Return_msg"></span>
              <form action="" method="post" id="LoginForm" name="LoginForm">
                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                <div class="form-group">
                  <label for="email"></label>
                  <input type="email" class="form-control" id="email" name="email" placeholder="Email Address" required="">
                  <span class="icon fa fa-envelope fa-lg"></span>
                </div>
                <div class="form-group">
                  <label for="sub"></label>
                  <input type="password" class="form-control" id="password" name="password" placeholder="Enter Password" required="">
                  <span class="icon fa fa-key"></span>
                </div>
                <div class="checkbox">
                  <input id="login-remember" type="checkbox" name="remember" value="1">
                  <label for="login-remember">Remember me</label>
                </div>
                <p><a href="<?php echo e(route('ForgetPassword')); ?>">Forget Password</a></p>
                <div class="text-center">
                  <button type="submit" id="LoginBtn" class="btn btn-default reg-btn">Login</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
  
</section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script src="<?php echo e(asset('public/Front/js/Login.js')); ?>"></script> 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Front.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\Blazer_USA\resources\views/Front/Login.blade.php ENDPATH**/ ?>