<!doctype html>
<html class="no-js" lang="">
    <?php echo $__env->make('Admin/Layouts/Head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <body>
        <?php echo $__env->make('Admin/Layouts/Menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>      

        <div id="right-panel" class="right-panel">
            <?php echo $__env->make('Admin/Layouts/Header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <div class="breadcrumbs">
	            <div class="col-sm-4">
	                <div class="page-header float-left">
	                    <div class="page-title">
	                        <h1>Product-List</h1>
	                    </div>
	                </div>
	            </div>
	            <div class="col-sm-8">
	                <div class="page-header float-right">
	                    <div class="page-title">
	                        <ol class="breadcrumb text-right">
	                            <li><a href="<?php echo e(route('AdminDashboard')); ?>">Dashboard</a></li>
	                            <li class="active">Product-List</li>
	                        </ol>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div class="col-md-12">
		        <?php if(Session::has('message')): ?>           
		            <div class="alert <?php echo e(Session::get('alert-class', 'alert-info')); ?>">
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <strong><?php echo e(Session::get('message')); ?></strong>
		            </div>
		        <?php endif; ?>
		    </div>
	        <div class="content mt-3">
	            <div class="animated fadeIn">
	                <div class="col-lg-12">
	                    <div class="card">
	                        <div class="card-header">
	                           <div class="pull-left" ><strong class="card-title">Product-List</strong></div>
	                           <div class="pull-right">
	                           	<a href="<?php echo e(route('ProductAdd')); ?>" class="btn btn-danger"><i class="fa fa-plus"></i> Add Product</a>
	                           </div>
	                        </div>
	                        <div class="card-body">
	                            <table class="table">
			                        <thead>
										<tr>
											<th scope="col">Collection</th>
											<th scope="col">Category</th>
											<th scope="col">Product Name</th>
											<th scope="col">Price</th>
											<th scope="col">Status</th>
											<th scope="col">Action</th>
										</tr>
			                        </thead>
			                         <thead>
										<tr>
											<th scope="col">
												<select  name="search_collection" id="search_collection" 
														class="form-control">
					                                <option value="">All</option>
					                                <?php $__currentLoopData = $Collection; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					                                <option value="<?php echo e($c->id); ?>"><?php echo e($c->name); ?></option>
					                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					                            </select>
											</th>
											<th scope="col">
												<select  name="search_category" id="search_category" 
														class="form-control">
					                                <option value="">All</option>
					                            </select>
											</th>
											<th scope="col">
												<input type="text"  name="search_name" id="search_name" 
														class="form-control" placeholder="Product Name">
											</th>
											<th scope="col">
												<select name="search_price" id="search_price"
														 class="form-control">
					                                <option value="ASC">ASC</option>
					                                <option value="DESC">DESC</option>
					                            </select>
											</th>
											<th scope="col">
												<select name="search_status" id="search_status"
														 class="form-control">
					                                <option value="">All</option>
					                                <option value="1">Active</option>
					                                <option value="0">De-active</option>
					                            </select>
											</th>
											<th scope="col">
												<button class="btn btn-danger" onclick="Search();"><i class="fa fa-search"></i> Serach</button>
											</th>
										</tr>
			                        </thead>
			                        <tbody id="ProductListDiv">
			                            
			                      	</tbody>
			                  </table>
	                        </div>
	                    </div>
	                </div>
	            </div><!-- .animated -->
	        </div><!-- .content -->
        </div>
        <?php echo $__env->make('Admin/Layouts/Footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <input type="hidden" name="url" id="url" value='<?php echo e(URL::to("/")); ?>'>
	    <input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">
	    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
	    <script src="<?php echo e(asset('public/Admin/custom-js/Product.js')); ?>" type="text/javascript"></script>
	    <script>
	    $("document").ready(function(){
	    	GetAllProduct(1);
	    });
	    </script>
    </body>
</html><?php /**PATH C:\xampp\htdocs\Blazer_USA\resources\views/Admin/Pages/Product/ProductList.blade.php ENDPATH**/ ?>