<?php $__env->startSection('content'); ?>
<section class="breadcrumb">
	<div class="container">
		<ul>
			<li><a href="<?php echo e(route('Home')); ?>">Home</a><i class="fa fa-angle-right" aria-hidden="true"></i></li>
			<li><?php echo e($Category->title); ?></li>
		</ul>
	</div>
</section>

<section class="comm_Sec">
	<div class="container">
		<h2 class="heading1 text-center"><?php echo e($Category->title); ?><span class="sub_h1"><?php echo html_entity_decode($Category->description) ?></span></h2>
		<div class="shop_by">
			<span>Shop by:</span>
			<div class="shop_by_input">
				<select class="inputfield">
					<option>Color</option>
					<option>Option 1</option>
					<option>Option 2</option>
					<option>Option 3</option>
				</select>
				<select class="inputfield">
					<option>Style</option>
					<option>Option 1</option>
					<option>Option 2</option>
					<option>Option 3</option>
				</select>
				<select class="inputfield">
					<option>Fit</option>
					<option>Option 1</option>
					<option>Option 2</option>
					<option>Option 3</option>
				</select>
				<select class="inputfield">
					<option>Fabric</option>
					<option>Option 1</option>
					<option>Option 2</option>
					<option>Option 3</option>
				</select>
				<select class="inputfield">
					<option>Pattern</option>
					<option>Option 1</option>
					<option>Option 2</option>
					<option>Option 3</option>
				</select>
			</div>
		</div>
		<div class="cat_bn">
			<div class="left_side">
				<div class="cont_wrap">
					<h2>Lorem Ipsum</h2>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</p>
					<a class="btn1" href="#">Shop Now</a>
				</div>
			</div>
			<div class="right_side"></div>
		</div>
	</div>
</section>

<section class="cat_product">
	<div class="container">
		<ul class="product_box">
			<?php $__currentLoopData = $ProductList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<li>
				<a href="<?php echo e(url('ProductDetail')); ?>/<?php echo e($p['pro_id']); ?>"></a>
				<div class="img_wrap"><img src="<?php echo e($p['pro_image']); ?>" alt=""></div>
				<div class="cont_wrap">
					<h5><?php echo e($p['pro_name']); ?></h5>
					<div class="color_price">
						<div class="pro_color">
							<?php $__currentLoopData = $p['pro_color']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $color): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <span style="background:<?php echo e($color->value); ?>;"></span>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
							<p><?php echo e(count($p['pro_color'])); ?> Colors</p>
						</div>
						<div class="pro_price">$<?php echo e($p['pro_price']); ?></div>
					</div>
				</div>
			</li>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</ul>
		<!-- <div class="pro_with_cat">
			<div class="left_side">
				<ul class="product_box">
					<li>
						<a href="#"></a>
						<div class="img_wrap"><img src="img/cat_product1.jpg" alt=""></div>
						<div class="cont_wrap">
							<h5>Product Name</h5>
							<div class="color_price">
								<div class="pro_color">
									<span style="background:#131313;"></span>
									<span style="background:#6c6c6c;"></span>
									<p>2 Colors</p>
								</div>
								<div class="pro_price">$175</div>
							</div>
						</div>
					</li>
					<li>
						<a href="#"></a>
						<div class="img_wrap"><img src="img/cat_product2.jpg" alt=""></div>
						<div class="cont_wrap">
							<h5>Product Name</h5>
							<div class="color_price">
								<div class="pro_color">
									<span style="background:#131313;"></span>
									<span style="background:#6c6c6c;"></span>
									<p>2 Colors</p>
								</div>
								<div class="pro_price">$175</div>
							</div>
						</div>
					</li>
					<li>
						<a href="#"></a>
						<div class="img_wrap"><img src="img/cat_product3.jpg" alt=""></div>
						<div class="cont_wrap">
							<h5>Product Name</h5>
							<div class="color_price">
								<div class="pro_color">
									<span style="background:#131313;"></span>
									<span style="background:#6c6c6c;"></span>
									<p>2 Colors</p>
								</div>
								<div class="pro_price">$175</div>
							</div>
						</div>
					</li>
					<li>
						<a href="#"></a>
						<div class="img_wrap"><img src="img/cat_product4.jpg" alt=""></div>
						<div class="cont_wrap">
							<h5>Product Name</h5>
							<div class="color_price">
								<div class="pro_color">
									<span style="background:#131313;"></span>
									<span style="background:#6c6c6c;"></span>
									<p>2 Colors</p>
								</div>
								<div class="pro_price">$175</div>
							</div>
						</div>
					</li>
					<li>
						<a href="#"></a>
						<div class="img_wrap"><img src="img/cat_product5.jpg" alt=""></div>
						<div class="cont_wrap">
							<h5>Product Name</h5>
							<div class="color_price">
								<div class="pro_color">
									<span style="background:#131313;"></span>
									<span style="background:#6c6c6c;"></span>
									<p>2 Colors</p>
								</div>
								<div class="pro_price">$175</div>
							</div>
						</div>
					</li>
					<li>
						<a href="#"></a>
						<div class="img_wrap"><img src="img/cat_product6.jpg" alt=""></div>
						<div class="cont_wrap">
							<h5>Product Name</h5>
							<div class="color_price">
								<div class="pro_color">
									<span style="background:#131313;"></span>
									<span style="background:#6c6c6c;"></span>
									<p>2 Colors</p>
								</div>
								<div class="pro_price">$175</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<div class="right_side">
				<div class="big_Cat">
					<div class="img_wrap" style="background-image:url(img/pro_with_cat.jpg);"></div>
					<div class="cont_wrap">
						<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
						<a class="btn1 black sm" href="#">Shop Now</a>
					</div>
				</div>
			</div>
		</div> -->
		<!-- <ul class="product_box mt-0">
			<li>
				<a href="#"></a>
				<div class="img_wrap"><img src="img/cat_product1.jpg" alt=""></div>
				<div class="cont_wrap">
					<h5>Product Name</h5>
					<div class="color_price">
						<div class="pro_color">
							<span style="background:#131313;"></span>
							<span style="background:#6c6c6c;"></span>
							<p>2 Colors</p>
						</div>
						<div class="pro_price">$175</div>
					</div>
				</div>
			</li>
			<li>
				<a href="#"></a>
				<div class="img_wrap"><img src="img/cat_product2.jpg" alt=""></div>
				<div class="cont_wrap">
					<h5>Product Name</h5>
					<div class="color_price">
						<div class="pro_color">
							<span style="background:#131313;"></span>
							<span style="background:#6c6c6c;"></span>
							<p>2 Colors</p>
						</div>
						<div class="pro_price">$175</div>
					</div>
				</div>
			</li>
			<li>
				<a href="#"></a>
				<div class="img_wrap"><img src="img/cat_product3.jpg" alt=""></div>
				<div class="cont_wrap">
					<h5>Product Name</h5>
					<div class="color_price">
						<div class="pro_color">
							<span style="background:#131313;"></span>
							<span style="background:#6c6c6c;"></span>
							<p>2 Colors</p>
						</div>
						<div class="pro_price">$175</div>
					</div>
				</div>
			</li>
			<li>
				<a href="#"></a>
				<div class="img_wrap"><img src="img/cat_product4.jpg" alt=""></div>
				<div class="cont_wrap">
					<h5>Product Name</h5>
					<div class="color_price">
						<div class="pro_color">
							<span style="background:#131313;"></span>
							<span style="background:#6c6c6c;"></span>
							<p>2 Colors</p>
						</div>
						<div class="pro_price">$175</div>
					</div>
				</div>
			</li>
			<li>
				<a href="#"></a>
				<div class="img_wrap"><img src="img/cat_product5.jpg" alt=""></div>
				<div class="cont_wrap">
					<h5>Product Name</h5>
					<div class="color_price">
						<div class="pro_color">
							<span style="background:#131313;"></span>
							<span style="background:#6c6c6c;"></span>
							<p>2 Colors</p>
						</div>
						<div class="pro_price">$175</div>
					</div>
				</div>
			</li>
			<li>
				<a href="#"></a>
				<div class="img_wrap"><img src="img/cat_product1.jpg" alt=""></div>
				<div class="cont_wrap">
					<h5>Product Name</h5>
					<div class="color_price">
						<div class="pro_color">
							<span style="background:#131313;"></span>
							<span style="background:#6c6c6c;"></span>
							<p>2 Colors</p>
						</div>
						<div class="pro_price">$175</div>
					</div>
				</div>
			</li>
			<li>
				<a href="#"></a>
				<div class="img_wrap"><img src="img/cat_product2.jpg" alt=""></div>
				<div class="cont_wrap">
					<h5>Product Name</h5>
					<div class="color_price">
						<div class="pro_color">
							<span style="background:#131313;"></span>
							<span style="background:#6c6c6c;"></span>
							<p>2 Colors</p>
						</div>
						<div class="pro_price">$175</div>
					</div>
				</div>
			</li>
			<li>
				<a href="#"></a>
				<div class="img_wrap"><img src="img/cat_product3.jpg" alt=""></div>
				<div class="cont_wrap">
					<h5>Product Name</h5>
					<div class="color_price">
						<div class="pro_color">
							<span style="background:#131313;"></span>
							<span style="background:#6c6c6c;"></span>
							<p>2 Colors</p>
						</div>
						<div class="pro_price">$175</div>
					</div>
				</div>
			</li>
			<li>
				<a href="#"></a>
				<div class="img_wrap"><img src="img/cat_product4.jpg" alt=""></div>
				<div class="cont_wrap">
					<h5>Product Name</h5>
					<div class="color_price">
						<div class="pro_color">
							<span style="background:#131313;"></span>
							<span style="background:#6c6c6c;"></span>
							<p>2 Colors</p>
						</div>
						<div class="pro_price">$175</div>
					</div>
				</div>
			</li>
			<li>
				<a href="#"></a>
				<div class="img_wrap"><img src="img/cat_product5.jpg" alt=""></div>
				<div class="cont_wrap">
					<h5>Product Name</h5>
					<div class="color_price">
						<div class="pro_color">
							<span style="background:#131313;"></span>
							<span style="background:#6c6c6c;"></span>
							<p>2 Colors</p>
						</div>
						<div class="pro_price">$175</div>
					</div>
				</div>
			</li>
			<li>
				<a href="#"></a>
				<div class="img_wrap"><img src="img/cat_product1.jpg" alt=""></div>
				<div class="cont_wrap">
					<h5>Product Name</h5>
					<div class="color_price">
						<div class="pro_color">
							<span style="background:#131313;"></span>
							<span style="background:#6c6c6c;"></span>
							<p>2 Colors</p>
						</div>
						<div class="pro_price">$175</div>
					</div>
				</div>
			</li>
			<li>
				<a href="#"></a>
				<div class="img_wrap"><img src="img/cat_product2.jpg" alt=""></div>
				<div class="cont_wrap">
					<h5>Product Name</h5>
					<div class="color_price">
						<div class="pro_color">
							<span style="background:#131313;"></span>
							<span style="background:#6c6c6c;"></span>
							<p>2 Colors</p>
						</div>
						<div class="pro_price">$175</div>
					</div>
				</div>
			</li>
			<li>
				<a href="#"></a>
				<div class="img_wrap"><img src="img/cat_product3.jpg" alt=""></div>
				<div class="cont_wrap">
					<h5>Product Name</h5>
					<div class="color_price">
						<div class="pro_color">
							<span style="background:#131313;"></span>
							<span style="background:#6c6c6c;"></span>
							<p>2 Colors</p>
						</div>
						<div class="pro_price">$175</div>
					</div>
				</div>
			</li>
			<li>
				<a href="#"></a>
				<div class="img_wrap"><img src="img/cat_product4.jpg" alt=""></div>
				<div class="cont_wrap">
					<h5>Product Name</h5>
					<div class="color_price">
						<div class="pro_color">
							<span style="background:#131313;"></span>
							<span style="background:#6c6c6c;"></span>
							<p>2 Colors</p>
						</div>
						<div class="pro_price">$175</div>
					</div>
				</div>
			</li>
			<li>
				<a href="#"></a>
				<div class="img_wrap"><img src="img/cat_product5.jpg" alt=""></div>
				<div class="cont_wrap">
					<h5>Product Name</h5>
					<div class="color_price">
						<div class="pro_color">
							<span style="background:#131313;"></span>
							<span style="background:#6c6c6c;"></span>
							<p>2 Colors</p>
						</div>
						<div class="pro_price">$175</div>
					</div>
				</div>
			</li>
			<li>
				<a href="#"></a>
				<div class="img_wrap"><img src="img/cat_product1.jpg" alt=""></div>
				<div class="cont_wrap">
					<h5>Product Name</h5>
					<div class="color_price">
						<div class="pro_color">
							<span style="background:#131313;"></span>
							<span style="background:#6c6c6c;"></span>
							<p>2 Colors</p>
						</div>
						<div class="pro_price">$175</div>
					</div>
				</div>
			</li>
			<li>
				<a href="#"></a>
				<div class="img_wrap"><img src="img/cat_product2.jpg" alt=""></div>
				<div class="cont_wrap">
					<h5>Product Name</h5>
					<div class="color_price">
						<div class="pro_color">
							<span style="background:#131313;"></span>
							<span style="background:#6c6c6c;"></span>
							<p>2 Colors</p>
						</div>
						<div class="pro_price">$175</div>
					</div>
				</div>
			</li>
			<li>
				<a href="#"></a>
				<div class="img_wrap"><img src="img/cat_product3.jpg" alt=""></div>
				<div class="cont_wrap">
					<h5>Product Name</h5>
					<div class="color_price">
						<div class="pro_color">
							<span style="background:#131313;"></span>
							<span style="background:#6c6c6c;"></span>
							<p>2 Colors</p>
						</div>
						<div class="pro_price">$175</div>
					</div>
				</div>
			</li>
			<li>
				<a href="#"></a>
				<div class="img_wrap"><img src="img/cat_product4.jpg" alt=""></div>
				<div class="cont_wrap">
					<h5>Product Name</h5>
					<div class="color_price">
						<div class="pro_color">
							<span style="background:#131313;"></span>
							<span style="background:#6c6c6c;"></span>
							<p>2 Colors</p>
						</div>
						<div class="pro_price">$175</div>
					</div>
				</div>
			</li>
			<li>
				<a href="#"></a>
				<div class="img_wrap"><img src="img/cat_product5.jpg" alt=""></div>
				<div class="cont_wrap">
					<h5>Product Name</h5>
					<div class="color_price">
						<div class="pro_color">
							<span style="background:#131313;"></span>
							<span style="background:#6c6c6c;"></span>
							<p>2 Colors</p>
						</div>
						<div class="pro_price">$175</div>
					</div>
				</div>
			</li>
		</ul> -->
		<div class="cus_pagi">
			<ul class="pagination">
				<li class="page-item prev"><a class="page-link" href="#"><i class="fa fa-caret-left" aria-hidden="true"></i></a></li>
				<li class="page-item active"><a class="page-link" href="#">1</a></li>
				<li class="page-item"><a class="page-link" href="#">2</a></li>
				<li class="page-item"><a class="page-link" href="#">3</a></li>
				<li class="page-item"><a class="page-link" href="#">4</a></li>
				<li class="page-item next"><a class="page-link" href="#"><i class="fa fa-caret-right" aria-hidden="true"></i></a></li>
			</ul>
			<div class="view_all_pro"><a href="#">View All Products</a></div>
		</div>
	</div>
</section>
<section class="comm_Sec">
  <div class="container">
    <h2 class="heading1 text-center">Shopping Categories<span class="sub_h1">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</span></h2>
    <ul class="category_box">
      <?php $__currentLoopData = $Categoryl; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <li style="background-image:url(<?php echo e(asset('public/Front/Category')); ?>/<?php echo e($c->image); ?>">
        <div class="cont_wrap">
          <h5><?php echo e($c->title); ?></h5>
          <div class="cat_box_link"><a href="<?php echo e(url('Category')); ?>/<?php echo e($c->id); ?>">Click Here</a></div>
        </div>
      </li>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
  </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Front.Default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/nettech2010/public_html/Project/BlazerUSA/resources/views/Front/Category.blade.php ENDPATH**/ ?>