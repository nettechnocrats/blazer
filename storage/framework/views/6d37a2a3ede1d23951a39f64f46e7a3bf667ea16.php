<h4>Account Settings</h4>
<ul>
  <li><a href="<?php echo e(route('PersonalInformation')); ?>">Personal Information</a></li>
  <li><a href="<?php echo e(route('Order')); ?>">Myorder and Order Details</a></li>
  <li><a href="<?php echo e(route('Wishlist')); ?>">Manage Wishlist</a></li>
  <li><a href="<?php echo e(route('ChangePassword')); ?>">Change Password</a></li>
  <li><a href="<?php echo e(route('Logout')); ?>">Logout</a></li>
</ul><?php /**PATH C:\xampp\htdocs\Blazer_USA\resources\views/Front/User/LeftMenu.blade.php ENDPATH**/ ?>