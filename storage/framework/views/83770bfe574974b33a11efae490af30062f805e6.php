<?php $__env->startSection('content'); ?>
<section>
  <div class="inner-banner">
    <img src="<?php echo e(asset('public/Front/img/inner-banner.png')); ?>">
    <div class="container">
      <div class="row">
        <div class="banner-text">
          <h3>Contact Us</h3>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
        </div>
      </div>
    </div>
  </div>
</section>
<section>
  <div class="contact-section">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="contact-outer">
            <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              <div class="icon-outer">
                <i class="fa fa-home" aria-hidden="true"></i>
              </div>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <div class="contact-text">
                <h3>Address</h3>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
              </div>
            </div>
          </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="contact-outer">
            <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              <div class="icon-outer" style="padding:5px 26px;">
                <i class="fa fa-mobile" aria-hidden="true"></i>
              </div>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <div class="contact-text">
                <h3>Phone</h3>
                <a href="tel:+1.6308024302">+1.6308024302</a>
              </div>
            </div>
          </div>
        </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="contact-outer">
            <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              <div class="icon-outer">
                <i class="fa fa-envelope" aria-hidden="true"></i>
              </div>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <div class="contact-text">
                <h3>Email</h3>
                <a href="mailto:info@lorumipsum.com">info@lorumipsum.com</a><p></p>
              </div>
            </div>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section>
  <div class="map-section">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d387190.279909073!2d-74.25987368715491!3d40.69767006458873!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew+York%2C+NY%2C+USA!5e0!3m2!1sen!2sin!4v1550726673873" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen=""></iframe>
          </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="contact-form">
            <span id="Return_msg"></span>
            <form action="" method="post" id="ContactUsForm" name="ContactUsForm">
              <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
              <input type="text" name="c_name" id="c_name" placeholder="Your Name">
              <input type="text" name="c_email" id="c_email" placeholder="Your email">
              <input type="text" name="c_subject" id="c_subject" placeholder="Subject">
              <textarea row="5" name="c_description" id="c_description" placeholder="Brief description"></textarea>
              <input type="submit" value="Send" id="ContactUsBtn" class="contact-btn">
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script src="<?php echo e(asset('public/Front/js/ContactUs.js')); ?>"></script> 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Front.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\Blazer_USA\resources\views/Front/ContactUs.blade.php ENDPATH**/ ?>