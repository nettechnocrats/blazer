<?php $__env->startSection('css'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/Admin')); ?>/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/Admin')); ?>/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('BreadCrumb'); ?>
<div class="page-header card">
  <div class="row align-items-end">
    <div class="col-lg-8">
      <div class="page-header-title">
        <div class="d-inline">
          <h5>Blog</h5>
        </div>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="page-header-breadcrumb">
        <ul class=" breadcrumb breadcrumb-title">
          <li class="breadcrumb-item">
            <a href="<?php echo e(route('Dashboard')); ?>"><i class="feather icon-home"></i></a>
          </li>
          <li class="breadcrumb-item"><a><?php echo e($title); ?></a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="col-lg-12">
      <?php if(\Session::has('success')): ?>
      <div class="alert alert-success background-success">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i class="icofont icofont-close-line-circled text-white"></i>
      </button>
      <p><?php echo e(\Session::get('success')); ?></p>
    </div>
    <?php endif; ?>
    </div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="col-sm-12">
  <div class="card">
    <div class="card-header">
      <h5>Blog List</h5>
      <a href="<?php echo e(route('AddBlog')); ?>" class="btn btn-primary" style="float: right;" title="Add category"><i class="fa fa-plus" aria-hidden="true"></i>Add Blog</a>
    </div>
    <div class="card-block">
      <div class="dt-responsive table-responsive">
        <table id="footer-search" class="table table-striped table-bordered nowrap">
          <thead>
            <tr>
              <th>Category</th>
              <th>Author</th>
              <th>Name</th>
              <th>Publish date</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php $__currentLoopData = $Blog; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $b): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr id="Row_<?php echo $b->id; ?>">
              <input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">
              <td><?php echo e($b->cat_id); ?></td>
              <td><?php echo e($b->author); ?></td>
              <td><?php echo e($b->title); ?></td>
              <td><?php echo e($b->publish_date); ?></td>
              <td id="StatusDiv_<?php echo $b->id; ?>" style="text-align:center;">
            <?php 
            if($b->status=='0')
            {
              ?>
                <a href="javascript:void(0);" id="StatusHref_<?php echo e($b->id); ?>" class="btn btn-danger"
                  onclick="ChangeBlogStatus('<?php echo $b->id; ?>',1);">
                  Deactive
                </a>                
              <?php
            }
            else if($b->status=='1')
            {
              ?>
                <a href="javascript:void(0);" id="StatusHref_<?php echo e($b->id); ?>" class="btn btn-success"
                   onclick="ChangeBlogStatus('<?php echo $b->id; ?>',0);">
                  Active
                </a>
              <?php
            }             
            ?>
          </td>
              <td style="text-align:center;">
                <a href="<?php echo e(url('Admin/EditBlog')); ?>/<?php echo e($b->id); ?>" class="btn btn-success btn-xs" title="Edit"><i class="fa fa-edit" aria-hidden="true" ></i></a>
              </td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </tbody>
          <tfoot>
            <tr>
              <th>Category</th>
              <th>Author</th>
              <th>Name</th>
              <th>Publish date</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script src="<?php echo e(asset('public/Admin')); ?>/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo e(asset('public/Admin')); ?>/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo e(asset('public/Admin')); ?>/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo e(asset('public/Admin')); ?>/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>

<script type="text/javascript">
$('#footer-search tfoot th').each(function(){
  var title=$(this).text();$(this).html('<input type="text" class="form-control" placeholder="Search '+title+'" />');
});
var table=$('#footer-search').DataTable();
table.columns().every(function(){
  var that=this;
  $('input',this.footer()).on('keyup change',function(){
    if(that.search()!==this.value){
      that.search(this.value).draw();
    }
  });
});
</script>
<script src="<?php echo e(asset('public/Admin/validation/blog.js')); ?>"></script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/nettech2010/public_html/Project/BlazerUSA/resources/views/Admin/Blog/Blog.blade.php ENDPATH**/ ?>