<?php $__env->startSection('content'); ?>

<section>
  <div class="inner-banner">
    <img src="<?php echo e(asset('public/Front/img/inner-banner.png')); ?>">
    <div class="container">
      <div class="row">
        <div class="banner-text">
          <h3>Change Password</h3>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="dashboard">
  <div class="container">
    <div class="dash_menu">
      <?php echo $__env->make('Front.User.LeftMenu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <div class="dash_cont">
      <h2>Change Password</h2>
      <span id="Return_msg"></span>
      <form action="" method="post" name="ChangePasswordForm" id="ChangePasswordForm">
        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
        <div class="row">
          <div class="form-group col-md-6">
            <label>Current Password</label>
            <input type="password" class="form-control inputfiled" id="old_pass" name="old_pass" placeholder="Current Password">
          </div>
          <div class="form-group col-md-6">
            <label>New Password</label>
            <input type="password" class="form-control inputfiled" id="new_pass" name="new_pass" placeholder="Enter Password">
          </div>
          <div class="form-group col-md-6">
            <label>Re-enter New Password</label>
            <input type="password" class="form-control inputfiled" id="conf_pass" name="conf_pass" placeholder="Enter Password">
          </div>
        </div>
        <input type="submit" class="btn cus_btn1 sm red" id="SubmitBtn" value="Save">
      </form>
    </div>
  </div>
</section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script src="<?php echo e(asset('public/Front/js/ChangePassword.js')); ?>"></script> 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Front.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\Blazer_USA\resources\views/Front/User/ChangePassword.blade.php ENDPATH**/ ?>