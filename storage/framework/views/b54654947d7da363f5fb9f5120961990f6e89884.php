<?php $__env->startSection('content'); ?>

<section>
  <div class="inner-banner">
    <img src="<?php echo e(asset('public/Front/img/inner-banner.png')); ?>">
    <div class="container">
      <div class="row">
        <div class="banner-text">
          <h3>Product Details</h3>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="product_inner">
  <div class="container">
    <div class="row">
      <div class="col-sm-5 col-md-5">
        <div class="pro_img_left">
          <img src="<?php echo e(asset('public/Front/Product')); ?>/<?php echo e($ProductImage); ?>" alt="<?php echo e($ProductDetails->name); ?>">
        </div>
      </div>
      <div class="col-sm-7 col-md-7">
        <form action="<?=url('AddToCart')?>" method="post" name="AddToCartForm" id="AddToCartForm">
          <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
          <input type="hidden" name="pro_id" value="<?=$ProductDetails->id?>">
          <input type="hidden" name="pro_url" value="<?=url()->full()?>">
          <div class="pro_right">
            <h2><?php echo e($ProductDetails->name); ?></h2>
            <div class="price">$<?php echo e($ProductDetails->price); ?></div>
            <div class="quantity_sec">
              <h3>Quantity</h3>
              <div class="input-group">
                <span class="input-group-btn">
                  <button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
                    <i class="fa fa-minus" aria-hidden="true"></i>
                  </button>
                </span>
                <input type="text" name="quant[1]" id="Quantity" class="form-control input-number" value="1" min="1" max="10">
                <span class="input-group-btn">
                  <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[1]">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                  </button>
                </span>
              </div>
            </div>
            <div class="blazer-select">
              <div class="row">
                <?php
                $variant = array();
                foreach ($ProductVariant as $Variant) {
                  $variant[] = $Variant['variant_name'];
                ?>
                <div class="col-md-4">
                  <p>Select <?=$Variant['variant_name']?></p>
                  <select id="<?=$Variant['variant_name']?>" onchange="HideMsg('Err_<?=$Variant['variant_name']?>');" name="<?=$Variant['variant_name']?>" class="form-control">
                    <option value="0" selected>Select <?=$Variant['variant_name']?></option>
                    <?php
                    foreach ($Variant['AttributeList'] as $Attribute) {
                      echo '<option value="'.$Attribute->id.'">'.$Attribute->value.'</option>';
                    }
                    ?>
                  </select>
                  <span id="Err_<?=$Variant['variant_name']?>" style="color:red"></span>
                </div>
                <?php
                }
                ?>
              </div>
              <input type="hidden" id="variant_type" name="variant_type" value="<?php echo implode(',', $variant);?>">
            </div>
            <div class="addto_cart">
              <a href="javascript:void(0)" onclick="AddToCart()">Add to Cart</a>
            </div>
            <!-- <div class="about_product">
              <h3>About The Product</h3>
              <ul>
                <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
                <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
                <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
              </ul>
            </div> -->
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<section class="detail_content_sec">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h3>Description</h3>
        <?php echo utf8_decode($ProductDetails->description);?>
      </div>
    </div>
  </div>
</section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script src="<?php echo e(asset('public/Front/js/ProductDetails.js')); ?>"></script> 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Front.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\Blazer_USA\resources\views/Front/ProductDetails.blade.php ENDPATH**/ ?>