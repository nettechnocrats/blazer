<?php $__env->startSection('css'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/Admin')); ?>/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/Admin')); ?>/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('BreadCrumb'); ?>
<div class="page-header card">
  <div class="row align-items-end">
    <div class="col-lg-8">
      <div class="page-header-title">
        <div class="d-inline">
          <h5>Edit Variant</h5>
        </div>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="page-header-breadcrumb">
        <ul class=" breadcrumb breadcrumb-title">
          <li class="breadcrumb-item">
            <a href="<?php echo e(route('Dashboard')); ?>"><i class="feather icon-home"></i></a>
          </li>
          <li class="breadcrumb-item"><a href="<?php echo e(route('Variant')); ?>">Variant List</a>
          </li>
          <li class="breadcrumb-item"><a>Edit Variant</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="col-sm-8">
  <div class="card">
    <div class="card-header">
      <h5>Edit Variant</h5>
    </div>
    <div class="card-block">
      <input type="hidden" id="url" value='<?php echo e(url("Admin/")); ?>'>
      <form id="updateform" action="<?php echo e(route('UpdateVariant')); ?>" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">
        <div class="form-group row">
          <div class="col-sm-4">
            <label>Name</label>
            <input type="hidden" value="<?php echo e($Variant->id); ?>" name="id">
            <input type="text" class="form-control" name="name" id="name" value="<?php echo e($Variant->variant); ?>">
          </div>
          <div class="col-sm-4">
            <label>Sort</label>
            <input type="number" class="form-control" name="sort" value="<?php echo e($Variant->sort); ?>">
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <strong for="country" class=" form-control-label">Status</strong>
              <select  name="status" id="status"
              class="form-control">
                <option value="1">Active</option>
                <option value="0">De-active</option>
              </select>
            </div>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-lg-6">
            <label>Variant Attributes</label>
            <div  id="VariantsAttributes">
              <div class="form-group row">
                <div class="col-sm-8">
                  <input type="text" value="" class="form-control" id="attribute">
                </div>
                <div class="col-sm-4">
                  <button type="button" name="AddMore" class="btn btn-success" onclick="AddMoreSection();">
                    <i class="fa fa-plus"></i> Add More
                  </button>
                </div>
              </div>
            </div>
            <?php $__currentLoopData = $VariantAttribute; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $Val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="form-group row" id="Row_<?php echo $Val->id; ?>">
                <div class="col-sm-8">
                  <input type="hidden" value="<?php echo e($Val->id); ?>" name="atributeid[]">
                  <input type="text" value="<?php echo e($Val->value); ?>" class="form-control" name="attributes[]" id="attribute">
                </div>
                <div class="col-sm-4">
                  <a href="javascript:void(0);" onclick="DeleteVariant('<?php echo $Val->id; ?>',0);">
                  <button type="button" name="delete" class="btn btn-danger">
                    <i class="fa fa-trash"></i>
                  </button></a>
                  <br>
                </div>
              </div>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </div>
        </div>
        <input type="submit" class="btn btn-success" value="Update" id="UpdateBtn">
      </form>
    </div>
  </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script src="<?php echo e(asset('public/Admin/validation/variant.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Admin.Default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/nettech2010/public_html/Project/BlazerUSA/resources/views/Admin/Variant/EditVariant.blade.php ENDPATH**/ ?>